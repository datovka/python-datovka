# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for finding Data Boxes
"""

import pygtk
import gtk
pygtk.require("2.0")
from dslib import models
from ds_search import DsSearchDialog
from contacts_dialog import DsContactsDialog
import os
import mimetypes
import sys
import os_support


class DsSendDialog(object):

  _last_dir = None



  def __init__(self, parent, reply_to=None):
    """
    client = dslib client instance
    recipient will be the default recipient,
    subject will be the default subject
    """
    self.parent = parent
    self.reply_to = reply_to
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("ds_send.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("send_message_dialog")
    #self.dialog.connect("close", self.on_send_message_dialog_close)
    # actions
    self.add_recipient = self.builder.get_object("add_recipient")
    self.remove_recipient = self.builder.get_object("remove_recipient")
    self.add_attachment = self.builder.get_object("add_attachment")
    self.remove_attachment = self.builder.get_object("remove_attachment")
    self.open_attachment = self.builder.get_object("open_attachment")
    # other widgets
    self.recipient_store = self.builder.get_object("recipient_store")
    self.recipient_treeview = self.builder.get_object("recipient_treeview")
    self.recipient_treeview.get_selection().connect("changed",
            self.on_recipient_treeview_selection_changed,
            self.recipient_treeview)
    self.attachment_store = self.builder.get_object("attachment_store")
    self.attachment_treeview = self.builder.get_object("attachment_treeview")
    self.attachment_treeview.get_selection().connect("changed",
            self.on_attachment_treeview_selection_changed,
            self.attachment_treeview)
    self.metatype_store = self.builder.get_object("metatype_store")
    self.subject_entry = self.builder.get_object("subject_entry")
    self.send_button = self.builder.get_object("send_button")
    self.pay_for_reply_checkbox = self.builder\
                                    .get_object("pay_for_reply_checkbox")
    self.use_paid_reply_checkbutton = self.builder\
                                    .get_object("use_paid_reply_checkbutton")
    self.update_widget_visibility()
    self.dialog.set_transient_for(self.parent.window1)
    self.run_number = 0 # used to detect reusal of the same dialog

  def update_widget_visibility(self):
    # adjust the pay_for_reply_checkbox
    account_info = self.parent.account.account_info
    if account_info.dbType == "OVM" or account_info.dbEffectiveOVM:
      # this is OVM, paying for reply does not have any meaning
      self.pay_for_reply_checkbox.hide() # adjust the use_paid_reply_checkbutton
    if self.reply_to and self.reply_to._dmType == "I":
      # this is a reply to an initialization message with paid reply
      self.use_paid_reply_checkbutton.show()
      self.use_paid_reply_checkbutton.set_active(True)
      self.pay_for_reply_checkbox.hide() # it would not make sense to combine it
    else:
      self.use_paid_reply_checkbutton.hide()
      
  def run(self):
    self.run_number += 1
    self.update_widget_visibility()
    self.dialog.show()
    if self.reply_to and self.run_number == 1:
      # we are replying to an existing message,
      # so we pre-fill some of the entries
      self.recipient_store.append([self.reply_to.dbIDSender,
                                   self.reply_to.dmSender,
                                   self.reply_to.dmSenderAddress])
      # subject
      annot = self.reply_to.dmAnnotation or ""
      self.subject_entry.set_text(_("Re:") + annot)
      # optional data
      expand = False
      for frm, to in (("dmRecipientRefNumber","dmSenderRefNumber"),
                      ("dmSenderRefNumber","dmRecipientRefNumber"),
                      ("dmRecipientIdent","dmSenderIdent"),
                      ("dmSenderIdent","dmRecipientIdent")):
        if getattr(self.reply_to, frm):
          self.builder.get_object(to).set_text(getattr(self.reply_to, frm))
          expand = True
      if expand:
        self.builder.get_object("expander1").set_expanded(True)
      
    acc_name = self.parent.account.name
    user_name = self.parent.account.credentials.username
    if acc_name != user_name:
      self.builder.get_object("from_label").set_markup(
                        "<b>%s</b> (%s)" % (acc_name, user_name))
    else:                  
      self.builder.get_object("from_label").set_markup("<b>%s</b>" % acc_name)
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()

  # ========== Callbacks ==========

  def on_add_recipient_activate(self, w):
    d = DsSearchDialog(self.parent, trans_window=self.dialog)
    result = d.run()
    boxes = d.get_selected_box_names()
    d.destroy()
    if result == 1:
      for box in boxes:
        addr_join = ", ".join(  filter(None, (box[2], box[3])))
        self._add_recipient_row([box[0],box[1],addr_join])
    self._check_requisities()
    
  def on_add_recipient_from_contacts_activate(self, w):
    contacts = self._get_contacts()
    d = DsContactsDialog(self.parent, contacts, trans_window=self.dialog)
    result = d.run()
    boxes = d.get_selected_box_names()
    d.destroy()
    if result == 1:
      for box in boxes:
        self._add_recipient_row([box[0],box[1],box[2]])
    self._check_requisities()   

  def _add_recipient_row(self, new_row):
    for row in self.recipient_store:
      if row[0] == new_row[0]:
        break
    else:
      self.recipient_store.append(new_row)
      

  def on_add_attachment_activate(self, w):
    d = gtk.FileChooserDialog(title="Select an attachment file",
                              parent=self.dialog,
                              action=gtk.FILE_CHOOSER_ACTION_OPEN,
                              buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                       gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                              )
    if DsSendDialog._last_dir:
      open_dir = DsSendDialog._last_dir
    else:
      open_dir = os_support.get_home_dir("./") 
    d.set_current_folder(open_dir)
    d.set_select_multiple(True)
    ok = d.run()
    not_added = []
    if ok == gtk.RESPONSE_ACCEPT:
      for fullname in d.get_filenames():
        fullname = os.path.abspath(fullname.decode('utf-8'))
        if not os.path.isfile(fullname):
          not_added.append(fullname)
          continue
        dir, name = os.path.split(fullname)
        # save the directory for the next run
        DsSendDialog._last_dir = dir
        mimetype, encoding = mimetypes.guess_type(fullname)
        if mimetype == "application/xml":
          mimetype = "text/xml"
        mime_list = {
          '.fo': "application/vnd.software602.filler.form+xml",
          '.zfo': "application/vnd.software602.filler.form-xml-zip",
          '.odt': "application/vnd.oasis.opendocument.text",
          '.ods': "application/vnd.oasis.opendocument.spreadsheet",
          '.odp': "application/vnd.oasis.opendocument.presentation",
          '.docx': "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
          '.xlsx': "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          '.pptx': "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        }
        if not mimetype:
          _name, ext = os.path.splitext(fullname)
          if ext and ext in mime_list:
            mimetype = mime_list[ext]
          else:
            mimetype = "text/plain" # or maybe consider "application/octet-stream"
        
        normname = os.path.normcase(fullname)
        for row in self.attachment_store:
          if row[1] == normname:
            break
        else:
          # it seems that GTK always returns utf-8 encoded filenames
          # despite what i says in the manual - we need not decode
          x = self.attachment_store.append([name, normname, 0, mimetype])
    d.destroy()
    if not_added:
      d = gtk.MessageDialog(self.dialog,
                            buttons=gtk.BUTTONS_OK,
                            type=gtk.MESSAGE_WARNING)
      text = ngettext("It was not possible to add the following file:",
                      "It was not possible to add the following files:",
                      len(not_added))
      d.set_markup((text + "\n\n") + "\n".join(not_added))
      d.run()
      d.destroy()
    self._check_requisities()


  def on_subject_entry_changed(self, w):
    self._check_requisities()

  def on_recipients_treeview_key_release_event(self, w, event):
    if event.keyval == gtk.keysyms.Delete:
      self._remove_selected_recipient()

  def on_remove_recipient_activate(self, w):
    self._remove_selected_recipient()

  def _remove_selected_recipient(self):
    treeview = self.recipient_treeview
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      model.remove(model.get_iter(paths[0]))
      self._check_requisities()

  def on_attachment_treeview_key_release_event(self, w, event):
    if event.keyval == gtk.keysyms.Delete:
      self._remove_selected_attachment()
        
  def on_remove_attachment_activate(self, w):
    self._remove_selected_attachment()
    
  def _remove_selected_attachment(self):
    treeview = self.attachment_treeview
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      model.remove(model.get_iter(paths[0]))
      self._check_requisities()
    
  def on_mimetype_cellrenderer_edited(self, w, row, new_value):
    self.attachment_store[row][3] = new_value

  def on_recipient_treeview_selection_changed(self, w, tree):
    model, paths = w.get_selected_rows()
    if len(paths) == 1:
      sensitive = True
    else:
      sensitive = False
    self.remove_recipient.set_sensitive(sensitive)
    
  def on_attachment_treeview_selection_changed(self, w, tree):
    model, paths = w.get_selected_rows()
    if len(paths) == 1:
      sensitive = True
    else:
      sensitive = False
    self.open_attachment.set_sensitive(sensitive)
    self.remove_attachment.set_sensitive(sensitive)

  def on_open_attachment_activate(self, w):
    treeview = self.attachment_treeview
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      fname = model[paths[0]][1]
      fname = fname.decode('utf-8')
      ok = os_support.open_file_in_associated_app(fname)
      if not ok:
        self.parent._show_error(_("I could not find an application to open file."))
      
  def on_attachment_treeview_row_activated(self, tree, path, col):
      self.on_open_attachment_activate(None)
      
  def on_send_message_dialog_close(self, w):
    dial = gtk.MessageDialog(w,
                             message_format=_("Do you really want to close the\
 dialog?\nAll the data you have entered would be lost."),
                             buttons=gtk.BUTTONS_YES_NO,
                             type=gtk.MESSAGE_QUESTION)
    res2 = dial.run()
    dial.destroy()
    if res2 == gtk.RESPONSE_NO:
      w.emit_stop_by_name('close')
      return True
  
  # ======== end of Callbacks =========


  def _check_requisities(self):
    """check if all mandatory fields are filled. If yes, activate the OK button"""
    text_ok = False
    recipients_ok = False
    attachments_ok = False
    if self.subject_entry.get_text():
      text_ok = True
    if len(self.recipient_store) > 0:
      recipients_ok = True
    if len(self.attachment_store) > 0:
      attachments_ok = True
    all_ok = text_ok and recipients_ok and attachments_ok
    if all_ok:
      self.send_button.set_sensitive(True)
    else:
      self.send_button.set_sensitive(False)
      
  def gen_prepared_messages(self):
    import base64
    for row in self.recipient_store:
      recipient = row[0]
      rec_name = row[1]
      envelope = models.dmEnvelope()
      envelope.dbIDRecipient = recipient
      envelope.dmRecipient = rec_name
      envelope.dmAnnotation = self.builder.get_object("subject_entry").get_text()
      for name in ("dmLegalTitleLaw","dmLegalTitleYear","dmLegalTitleSect",
                   "dmLegalTitlePar","dmLegalTitlePoint","dmRecipientRefNumber",
                   "dmRecipientIdent","dmSenderRefNumber","dmSenderIdent",
                   "dmToHands"):
        text = self.builder.get_object(name).get_text()
        if text:
          setattr(envelope, name, text)
      envelope.dmPersonalDelivery = self.builder.\
                                get_object("dmPersonalDelivery").get_active()
      # reply message when reply was paid for
      if self.reply_to and self.reply_to._dmType == "I" and \
        self.use_paid_reply_checkbutton.get_active():
        envelope._dmType = "O"
      # initialization message for paid message
      if self.pay_for_reply_checkbox.get_active():
        envelope._dmType = "I"
      # sender indentification
      if self.builder.get_object("add_sender_ident_checkbutton").get_active():
        envelope.dmPublishOwnID = True
      dmfiles = []
      for row in self.attachment_store:
        dmfile = models.dmFile()
        dmfile._dmMimeType = row[3]      
        dmfile._dmFileMetaType = "main"
        dmfile._dmFileDescr = row[0].decode('utf-8')
        f = file(row[1].decode('utf-8'), "rb")
        dmfile.dmEncodedContent = base64.standard_b64encode(f.read())
        f.close()
        dmfiles.append(dmfile)
      yield rec_name, recipient, envelope, dmfiles

  def _get_contacts(self):    
    """some IDs might be duplicated either by missing address in case of
    incomplete messages or different address (or possibly name) if it was
    changed during time"""
    contacts = set(self.parent.account.messages.database.get_unique_senders())
    contacts |= set(self.parent.account.messages.database.get_unique_recipients())
    clean = {}
    for id,name,address in contacts:
      if (not id in clean) or (address and not clean[id][1]):
          clean[id] = (name, address)
    to_sort = [(name, address, id) for id, (name, address) in clean.iteritems()]
    to_sort.sort()
    return [(id, name, address, id) for (name, address, id) in to_sort]
