# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""This file contains code that is needed to update different parts of Datovka
between different versions.
It is in a separate module in order not to clutter the normal namespace"""

import os.path

def add_dmtype_column_to_dsdb(dbfilename):
  import sqlalchemy as sal
  if not os.path.isfile(dbfilename) or os.path.getsize(dbfilename) == 0:
    # not a file or an empty one - we do not try to fix it
    return
  engine = sal.create_engine('sqlite:///%s'%dbfilename, echo=False)
  try:
    res = engine.execute("SELECT _dmType FROM messages LIMIT 1")
  except sal.exc.OperationalError, e:
    if "_dmType" in str(e):
      # dmType column does not exist - we create it
      # print "adding column _dmType to", dbfilename
      engine.execute("ALTER TABLE messages ADD COLUMN _dmType")
    else:
      pass # we ignore all other operational errors in here
  engine.dispose()
