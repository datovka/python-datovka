# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for finding Data Boxes
"""

import pygtk
import gtk
pygtk.require("2.0")
import string

class DsChangePasswordDialog(object):
  
  PASSWORD_CHARS = string.ascii_letters + string.digits + "_-"

  def __init__(self, parent, boxname):
    """
    client = dslib client instance
    """
    self.parent = parent
    self._requires_otp = False
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("change_password.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("change_password_dialog")
    self.builder.get_object("boxid").set_text(boxname)
    self.current_password_entry = self.builder.get_object("current_password_entry")
    self.new_password_entry = self.builder.get_object("new_password_entry")
    self.new_password_entry2 = self.builder.get_object("new_password_entry2")
    self.ok_button = self.builder.get_object("ok_button")
    self.warning_label = self.builder.get_object("warning_label")
    self.dialog.set_transient_for(self.parent.window1)


  def run(self):
    if self._requires_otp:
      self.builder.get_object("otp_entry").show()
      self.builder.get_object("otp_label").show()
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()
    
  def set_requires_otp(self, req):
    self._requires_otp = req

  def check_new_passwords_matching(self):
    return self.new_password_entry.get_text() == self.new_password_entry2.get_text()

  def get_passwords(self):
    if not self.check_new_passwords_matching():
      raise Exception("the two versions of new password do not match")
    old_pass = self.current_password_entry.get_text()
    if self._requires_otp:
      old_pass += self.builder.get_object("otp_entry").get_text()
    return old_pass, self.new_password_entry.get_text()

  def on_some_new_password_entry_changed(self, w):
    if not self.check_new_passwords_matching():
      self.ok_button.set_sensitive(False)
      self.warning_label.set_text(_("New passwords do not match."))
    else:
      if self._is_otp_ok():
        self.ok_button.set_sensitive(True)
      self.warning_label.set_text("")

  def on_passgenbutton_clicked(self, w):
    self._show_hide_passwords(True)
    self.builder.get_object("passshowbutton").set_label(_("Hide"))
    newpass = self.gen_password()
    self.new_password_entry.set_text(newpass)
    self.new_password_entry2.set_text(newpass)
  
  def on_passshowbutton_clicked(self, w):
    if self.new_password_entry.get_visibility():
      show = False
      w.set_label(_("Show"))
    else:
      show = True
      w.set_label(_("Hide"))
    self._show_hide_passwords(show)
    
  def _show_hide_passwords(self, show):
    self.new_password_entry.set_visibility(show)
    self.new_password_entry2.set_visibility(show)
    
  def on_otp_entry_insert_text(self, w, text, length, *args):
    if not text.isdigit():
      w.emit_stop_by_name("insert_text")
      self.warning_label.set_text(_("Only numbers are allowed!"))
    else:
      self.warning_label.set_text("")
      
  def _is_otp_ok(self):
    if not self._requires_otp:
      return True
    return len(self.builder.get_object("otp_entry").get_text()) >= 6
  
  def on_otp_entry_changed(self, w):
    self.builder.get_object("ok_button").set_sensitive(self._is_otp_ok())
    
  @classmethod
  def gen_password(cls):
    import random
    while True:
      # two copies of characters to allow repeating
      password = "".join(random.sample(cls.PASSWORD_CHARS+cls.PASSWORD_CHARS, 8))
      if set(password) & set(string.digits) and \
         set(password) & set(string.ascii_lowercase) and \
         set(password) & set(string.ascii_uppercase):
        return password
      
