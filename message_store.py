#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

class MessageStore(object):
  """
  Represents a store for messages, capable of adding, retrieving
  and persisting message objects
  This class is an abstract model, derived classes should implement
  specific types of storage 
  """
  
  def __init__(self):
    pass
  
  def __iter__(self):
    return self._iterate_messages()
  
  def open(self):
    pass
  
  def close(self):
    pass
  
  def add_message(self, message):
    pass
  
  def has_message(self, mid):
    pass
  
  def get_message_by_id(self, mid):
    pass

  def add_message_supplementary_data(self, mid, key, value):
    pass
  
  def get_message_supplementary_data(self, mid, key):
    pass

  def has_message_supplementary_data(self, mid):
    pass
  
  def _iterate_messages(self):
    """generator or iterator"""
    pass



class DictMessageStore(MessageStore):
  """
  Represents a store for messages, capable of adding, retrieving
  and persisting message objects
  """
  
  def __init__(self):
    self.messages = {}
    self.message_supplementary_data = {}
  
  def add_message(self, message):
    print message
    self.messages[int(message.dmID)] = message
  
  def has_message(self, mid):
    return int(mid) in self.messages
  
  def get_message_by_id(self, mid):
    return self.messages.get(int(mid), None)
  
  def set_message_supplementary_data(self, mid, key, value):
    if not mid in self.message_supplementary_data:
      self.message_supplementary_data[mid] = {}
    self.message_supplementary_data[mid][key] = value
  
  def get_message_supplementary_data(self, mid, key):
    data = self.message_supplementary_data.get(mid)
    if data:
      return data.get(key, None)
    else:
      return None
    
  def has_message_supplementary_data(self, mid):
    return self.message_supplementary_data.has_key(mid)
  
  def _iterate_messages(self):
    """generator or iterator"""
    for id, message in self.messages.iteritems():
      yield message


from dslib.dsdb import alchemy
from updates import add_dmtype_column_to_dsdb
import os


class DsdbMessageStore(MessageStore):
  """
  Represents a store for messages, capable of adding, retrieving
  and persisting message objects
  """
  
  MESSAGE_TYPE_RECEIVED = alchemy.MESSAGE_TYPE_RECEIVED
  MESSAGE_TYPE_SENT = alchemy.MESSAGE_TYPE_SENT
  
  def __init__(self, dbfilename=None):
    """dbfile == None means in-memory database"""
    self.database = None
    self.dbfilename = dbfilename
    self.message_supplementary_data = {}
  
  def open(self):
    # test if the database schema does not need an update
    if self.dbfilename and os.path.exists(self.dbfilename):
      add_dmtype_column_to_dsdb(self.dbfilename)
    # proceed with opening the db
    self.database = alchemy.DSDatabase()
    self.database.open_database(self.dbfilename)
  
  def close(self):
    if self.database:
      self.database.close_database()
      self.database = None
  
  def add_message(self, message, raw_data=None, supp_data=None):
    self.database.store_message(message, raw_data=raw_data)
    if supp_data:
      self.set_message_supplementary_data_many(message.dmID, supp_data)
  
  def add_delivery_info_data(self, mid, delivery_info, raw_data=None):
    self.database.add_delivery_info_data(mid, delivery_info, raw_data=raw_data)
  
  def has_message(self, mid):
    return self.database.has_message(mid)
  
  def get_message_by_id(self, mid, client=None):
    return self.database.get_message(int(mid))
  
  def get_message_from_raw_data(self, mid, client):
    return self.database.get_message_from_raw_data(mid, client)
  
  def get_delivery_info_from_raw_data(self, mid, client):
    return self.database.get_delivery_info_from_raw_data(mid, client)
  
  def set_message_supplementary_data_many(self, mid, data):
    sd = self.database.get_supplementary_data(mid)
    if not sd:
      sd = alchemy.SupplementaryMessageData(mid)
    for key, value in data.iteritems():
      if key == 'custom_data':
        sd.set_custom_data(value)
      else:
        setattr(sd, key, value)
    self.database.store_supplementary_data(sd)
  
  def set_message_supplementary_data(self, mid, key, value):
    sd = self.database.get_supplementary_data(mid)
    if not sd:
      sd = alchemy.SupplementaryMessageData(mid)
    setattr(sd, key, value)
    self.database.store_supplementary_data(sd)
  
  def get_message_supplementary_data(self, mid, key):
    sd = self.database.get_supplementary_data(mid)
    if not sd:
      return None
    if key == 'custom_data':
      return sd.get_custom_data()
    else:
      return getattr(sd, key)

  def has_message_supplementary_data(self, mid):
    return self.database.has_supplementary_data(mid)
  
  def is_sent(self, mid):
    """convenience method - boolean, true if message has message_type 'sent'"""
    typ = self.get_message_supplementary_data(mid, "message_type")
    if typ:
      return typ == self.MESSAGE_TYPE_SENT

  def is_received(self, mid):
    """convenience method - boolean, true if message has message_type 'sent'"""
    typ = self.get_message_supplementary_data(mid, "message_type")
    if typ:
      return typ == self.MESSAGE_TYPE_RECEIVED
    
  def has_raw_data(self, mid):
    return self.database.has_raw_data(mid)
  
  def get_raw_data(self, mid):
    return self.database.get_raw_data(mid)

  def has_raw_delivery_info_data(self, mid):
    return self.database.has_raw_delivery_info_data(mid)

  def get_raw_delivery_info_data(self, mid):
    return self.database.get_raw_delivery_info_data(mid)
  
  def get_messages_between_dates(self, from_date, to_date, message_type=None,
                                 add_pkcs7_data=False):
    return self.database.get_messages_between_dates(from_date, to_date,
                                                    message_type=message_type,
                                                    add_pkcs7_data=add_pkcs7_data)
    
  def get_messages_without_date(self, message_type=None, add_pkcs7_data=False):
    return self.database.get_messages_without_date(message_type=message_type,
                                                   add_pkcs7_data=add_pkcs7_data)
    
  def _iterate_messages(self):
    """generator or iterator"""
    import datetime
    threshold = datetime.datetime.now()-datetime.timedelta(days=90)
    return self.database.messages_between_dates(threshold, None)
    #return self.database.all_messages()
  
  def remove_message(self, mid):
    self.database.remove_message(mid)
  
  @classmethod
  def message_type_str_to_code(self, typ):
    if typ == 'sent':
      return self.MESSAGE_TYPE_SENT
    elif typ == "received":
      return self.MESSAGE_TYPE_RECEIVED
    else:
      raise ValueError("unknown message type")
