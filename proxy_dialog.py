# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for proxy setting
"""

import pygtk
import gtk
pygtk.require("2.0")
import os
import urllib2


class ProxyDialog(object):

  def __init__(self, parent):
    """
    client = dslib client instance
    """
    self.parent = parent
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("proxy_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("proxy_dialog")
    self.https_manual_config_box = self.builder.get_object("https_manual_config_box")
    self.https_auto_proxy_label = self.builder.get_object("https_auto_proxy_label")
    self.https_auth_data_box = self.builder.get_object("https_proxy_auth_data_box")
    self.http_manual_config_box = self.builder.get_object("http_manual_config_box")
    self.http_auto_proxy_label = self.builder.get_object("http_auto_proxy_label")
    self.http_auth_data_box = self.builder.get_object("http_proxy_auth_data_box")
    self._show_auto_proxies()
    self.set_proxies(parent)
    self.dialog.set_transient_for(self.parent.window1)

  def update_dslib_proxy_settings(self):
    """takes data entered in the dialog and updates the settings in dslib"""
    from dslib.network import ProxyManager
    get_object = self.builder.get_object
    for proto in ('http','https'):
      proxy_obj = getattr(ProxyManager, proto.upper()+'_PROXY')
      proxy_setting = self.get_proxy_setting(proto)
      proxy_obj.set_uri(proxy_setting)
      if proxy_setting is not None:
        proxy_obj.username = get_object(proto+"_proxy_auth_username_entry")\
                                .get_text()
        proxy_obj.password = get_object(proto+"_proxy_auth_password_entry")\
                                .get_text()
      else:
        proxy_obj.username = None
        proxy_obj.password = None
    
  def get_proxy_setting(self, proto):
    if self.builder.get_object(proto+"_radiobutton_no_proxy").get_active():
      return None
    elif self.builder.get_object(proto+"_radiobutton_auto_proxy").get_active():
      return -1
    else:
      hostname = self.builder.get_object(proto+"_proxy_hostname").get_text()
      port = self.builder.get_object(proto+"_proxy_port").get_text()
      return hostname+":"+port
    
  def set_proxies(self, parent):
    from dslib.network import ProxyManager
    get_object = self.builder.get_object
    for proto in ('http','https'):
      proxy_obj = getattr(ProxyManager, proto.upper()+'_PROXY')
      proxy_setting = getattr(parent, proto+"_proxy")
      # https proxy
      if proxy_setting is None:
        get_object(proto+"_radiobutton_no_proxy").set_active(True)
      elif proxy_setting == -1:
        get_object(proto+"_radiobutton_auto_proxy").set_active(True)
      else:
        pparts = proxy_obj.hostname.rsplit(":", 1)
        if len(pparts) == 2:
          hostname, port = pparts
          get_object(proto+"_proxy_hostname").set_text(hostname)
          get_object(proto+"_proxy_port").set_text(port)
          get_object(proto+"_radiobutton_manual_proxy").set_active(True)
      # authentication
      get_object(proto+"_proxy_auth_username_entry")\
                  .set_text(proxy_obj.username or "")
      get_object(proto+"_proxy_auth_password_entry")\
                  .set_text(proxy_obj.password or "")
                
    
  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()
   
  # -- signal handlers
    
  def on_https_radiobutton_manual_proxy_toggled(self, w):
    if w.get_active():
      self.https_manual_config_box.set_sensitive(True)
      self.https_auto_proxy_label.set_sensitive(False)
      self.https_auth_data_box.set_sensitive(True)
    
  def on_https_radiobutton_auto_proxy_toggled(self, w):
    if w.get_active():
      self.https_manual_config_box.set_sensitive(False)
      self.https_auto_proxy_label.set_sensitive(True)
      self.https_auth_data_box.set_sensitive(True)
      
  def on_https_radiobutton_no_proxy_toggled(self, w):
    if w.get_active():
      self.https_manual_config_box.set_sensitive(False)
      self.https_auto_proxy_label.set_sensitive(False)
      self.https_auth_data_box.set_sensitive(False)

  def on_http_radiobutton_manual_proxy_toggled(self, w):
    if w.get_active():
      self.http_manual_config_box.set_sensitive(True)
      self.http_auto_proxy_label.set_sensitive(False)
      self.http_auth_data_box.set_sensitive(True)
    
  def on_http_radiobutton_auto_proxy_toggled(self, w):
    if w.get_active():
      self.http_manual_config_box.set_sensitive(False)
      self.http_auto_proxy_label.set_sensitive(True)
      self.http_auth_data_box.set_sensitive(True)
      
  def on_http_radiobutton_no_proxy_toggled(self, w):
    if w.get_active():
      self.http_manual_config_box.set_sensitive(False)
      self.http_auto_proxy_label.set_sensitive(False) 
      self.http_auth_data_box.set_sensitive(False)

  # -- private functions
  
  def _show_auto_proxies(self):
    from dslib.client import Client
    for proto in ('http','https'):
      proxy = urllib2.getproxies().get(proto, None)
      if not proxy:
        proxy = _("<i>No proxy detected, direct connection will be used.</i>")
      else:
        proxy = _("<i>Proxy detected</i>: %s" % proxy)
      self.builder.get_object(proto+"_auto_proxy_label").set_markup(proxy)