========
Datovka
========

Datovka (dříve dsgui) je svobodné grafické uživatelské rozhraní k datovým
schránkám.


Závislosti
===========

Následující seznam obsahuje balíčky, které je třeba mít v systému pro správný
chod Datovky.

* python
* python-dev (python-devel)
* pygtk
* pyOpenSSL (python-openssl, python-ssl)
* SQLAlchemy (python-sqlalchemy)
* dslib
* ReportLab (python-reportlab) - nepovinná závislost


Verze Datovky
===============

* verze 3.1 -
    Update dslib - podpora nejnovější SQLAlchemy (0.9), přidané indexy do
    databáze.
    Pokud systém samotný správně nerozezná media type, tak u souborů .docx,
    .xlsx, .pptx, .odt, .ods, .odp, .fo a .zfo jsou správně určeny.
    Opravena chyba v zobrazování chybové hlášky pokud systém zprávu předává v
    jiném kódování než utf-8.

* verze 3.0.3 -
    Bylo přidáno zapamatování pozice posuvníků mezi jednotlivými prvky
    grafického rozhraní. Opravena byla chyba zapamatování řadícího sloupce
    v případě řazení podle ID zprávy a chyba zobrazení zprávy v případě
    specifických parametrů zprávy.

* verze 3.0.2 -
    Bylo opraveno několik chyb v kompatibilitě s Windows a byl ošetřen případ
    kdy jména příloh obsahují znaky, které nejsou kompatibilní s daným systémem.

* verze 3.0.1 -
    Byla opravena chyba zobrazení informací o složce v případě prázdné databáze
    zpráv.

* verze 3.0 -
    Byl kompletně přepracován způsob zobrazování seznamů zpráv - přijaté a
    odeslané zprávy se nezobrazují v tabech, ale jako pod-položky seznamu účtů.
    Zprávy jsou rozděleny na aktuální a všechny, které jsou rozděleny po
    jednotlivých letech. Bylo výrazně urychleno načítání zpráv z databáze a tím
    i přepínání mezi účty a start aplikace. Byla přidána možnost nastavení
    výběru zprávy při otevření účtu (nejnovější, poslední otevřená, žádná).
    Byla vylepšena detekce existence souboru přílohy a jejího duplicitního
    přídání. Byla implemenována možnost mazat zprávy starší než 90 dní z
    databáze (zatím pouze po jednotlivých zprávách). Byla přidána možnost
    deaktivovat účet pro synchronizaci při použití funkce "Sync all" vhodná
    např. pro zrušené schránky. Byla přidána podpora pro odesílání a příjem
    informací o identifikaci odesilatele zprávy.
    U exportu korespondence byla změněna výchozí přípona pro soubory ve formátu
    CSV na .txt, což zjednodušuje import do MS Excelu. Byla přidána možnost
    skrolování do seznamu účtů. Při otevírání příloh v externích aplikacích jsou
    soubory ukládány v temp adresáři pod původním jménem a nikoli náhodně
    vygenerovaným.
    Byly opraveny chyby při rychlém přepnutí zobrazeného účtu, vyhledávání účtů
    v seznamu kontaktů, stahování nových zpráv u schránek s více než 1000
    aktivních zpráv. Bylo opraveno pojmenování doručenky jako dodejky.  

* verze 2.1.2 -
    Byla opravena chyba nastavení jazyka z operačního systému v případě
    defaultního nastavení. Byla odstraněna možnost změny testovacího účtu na
    normální a opačně, která vedla k pádu aplikace. Česká lokalizace byla
    doplněna a upravena tak, aby odpovídala oficiálnímu rozhraní u výrazů
    "dodáno" a "doručeno".


* verze 2.1.1 -
	V této verzi byly přidány následující funkce - možnost zvolit jazyk aplikace
	přímo v preferencích a možnost označit všechny zprávy jako přečtené.
	Opraveny byly následující chyby - chyba ve filtrování seznamu kontaktů,
	chyba zpracování konfiguračního souboru	na českých Windows při zpracování
	cest končících na specifické znaky s diakritikou, chyba vytváření nové
	databáze v adresáři s diakritikou v názvu, nastavení zobrazení toolbaru nově
	reflektuje nastavení systému, byly odlišeny ikony pro preference a nastavení
	jednotlivých účtů, portable verze neukládá absolutní cestu k databázovému
	souboru. Ikonky pro synchronizaci jednoho účtu, synchronizaci všech účtů 
	a stažení kompletní zprávy byly změněny.
	Ve verzi pro Mac OS X byla vylepšena integrace s prostředím, v portable
	verzi byla deaktivována možnost přesunu databáze.


* verze 2.0.2 -
	Bylo opraveno několik chyb v práci s více účty v případě jejich abecedního
	řazení (odstranění účtu a hromadná synchronizace). Byla opravena chyba
	zobrazení nových zpráv v případě časového posunu.
	

* verze 2.0.1 -
	Byly opraveny následující problémy - import adresáře s existujícími
	databázemi zpráv vytvořil nekorektní strukturu účtů, po smazání posledního
	účtu nebyly správně ošetřeny některé akce, při upozornění na vypršení hesla
	u více účtů bylo možno úspěšně změnit heslo jen u prvního z nich.
	

* verze 2.0 -
    Tato verze vylepšuje stávající funkcionalitu v několika oblastech.
    První z nich je kompatibilita s novými funkcemi DS. Byla přidána podpora pro
    placené odpovědní zprávy - z Datovky je možné poslat jak inicializační
    zprávu, tak odpověď na ní placenou příjemcem. Opravena byla také nedokonalá
    synchronizace zrušení devadesátidenní platnosti hesla.
    Druhou oblastí změn je uživatelská přívětivost, zejména v oblasti podpory
    více datových schránek. Pořadí schránek v seznamu je nově možné měnit,
    přibylo tlačítko pro synchronizaci všech schránek najednou a u každé
    schránky je nově zobrazen počet nepřečtených přijatých zpráv. Po přepnutí
    účtu je vždy automaticky vybrána nejnovější zpráva a aktuální zpráva je v
    seznamu vždy viditelná i po změně řazení zpráv. Při vyhledávání schránek
    jsou nově zobrazeny informace o omezení vyhledávání schránek v závislosti na
    statutu schránky uživatele. Vylepšena byla také rychlost synchronizace se
    serverem DS a některé běžné chybové zprávy byly doplněny o popis
    pravděpodobného důvodu chyby a možnosti řešení. Datovka si nově pamatuje
    poslední použité adresáře i mezi spuštěními. 
    Poslední oblastí změn jsou opravy chyb. Ty se týkají zobrazení zpráv při
    přepínání účtů, výběru lokalizace pod Mac OS X, zobrazení jmen příloh pod
    Mac OS X, funkcionality hypertextových odkazů v dialozích a zpracování
    argumentů příkazové řádky.
    V oblasti portable verze bylo odstraněno omezení zapamatování hesla a tato
    verze byla graficky odlišena, aby nemohla být uživatelem zaměněna za
    běžně nainstalovanou verzi. 
     

* verze 1.7 -
    První verze vydaná pod novým jménem Datovka.
    Přidává možnost běhu v portable módu a v debug módu. Umožňuje vytvoření účtu
    nahráním dříve zkopírované databáze zpráv. Zlepšuje kompatibilitu CSV
    výstupu přehledu korespondence s MS Excelem. 
    Opravuje chyby ve vytváření nového databázového souboru v případě, že byl
    původní smazán, v odstraňování účtu, v přepínání účtu v okamžiku, kdy je
    vybrána přijatá i odeslaná zpráva a ve zpracování časových údajů událostí.
    

Verze dsgui
=============

* verze 1.6.3 -
    V této verzi byl přidán export seznamu korespondence ve formátu CSV a
    upraveno otevírání příloh v externích aplikacích. Bylo také opraveno
    několik drobných chyb, např. při přepínání mezi zprávami v případě, že byla
    vybrána jiná než první příloha, či zobrazování nápovědy v prohlížeči.

* verze 1.6.2 -
    Tato verze opravuje závažnou chybu, která znemožňuje přidání účtu do dsgui.
    Opraveno bylo také několik drobnějších problémů.

* verze 1.6.1 -
    V této verzi bylo opraveno několik chyb, zejména v práci s diakritikou
    v názvech příloh na systémech Windows a u autentizace jednorázovým heslem.

* verze 1.6 -
    Hlavní novinkou v této verzi je podpora pro autentizaci jednorázovým kódem,
    která byla přidána do systému datových schránek 19.6.2011.
    Byly také mírně vylepšeny a opraveny dialogy pro vytvoření přehledu
    korespondence a pro vytváření zprávy. Případné neošetřené chyby v aplikaci
    jsou nově zobrazovány také prostřednictvím dialogového okna, což umožňuje
    jejich debugování i v případě, že aplikace nebyla spuštěna z konzole.  

* verze 1.5.1 -
    Tato verze obsahuje přepracovanou podporu pro HTTP(S) proxy. Nově je možné
    HTTP a HTTPS proxy konfigurovat nezávisle na sobě a použít autentizaci.
    Aplikace také ukládá konfiguraci nejen při ukončení, ale také při
    důležitých změnách, jako je přidání účtu, apod.
    Byla odstraněna možnost vybrat víckrát stejného adresáta k jedné zprávě.
    Byla opravena chyba zobrazení seznamu kontaktů v případě změny řazení
    záznamů.  

* verze 1.5 -
	V této verzi byla především implementována podpora pro komerční datové
    zprávy (KDZ), které vyžadují explicitní potvrzení přijetí.
    Bylo přidáno také několik dalších novinek - seznam použitých kontaktů
    přístupný při vytváření nové zprávy, ukládání ručně nastavené šířky sloupců
    v seznamech zpráv, přímé otevírání dodejek v externí asociované aplikaci a
    počet nových zpráv zobrazený po synchronizaci ve stavovém řádku.
    Opraveno bylo také několik chyb, zejména v české lokalizaci a při vytváření
    nových účtů.
    Upozornění: ve výchozím nastavení nově dsgui při zapnuté kontrole nových
    verzí odesílá při přístupu na server CZ.NIC Labs krátkou informaci o verzi
    dsgui, verzi Pythonu, platformě a ID aplikace. Tato informace slouží pro
    účely statistiky a neobsahuje žádná osobní data. Pokud si nebudete přát
    tuto informaci odesílat, můžete změnit příslušné nastavení v dialogu
    Soubor/Nastavení. 
	  

* verze 1.4.1 -
    Bylo opraveno několik chyb - zobrazení dialogu po odeslání zprávy a její
    správné zařazení mezi odeslané zprávy, umístění okna mimo viditelnou plochu
    na Windows, aktualizace údajů o schránce bezprostředně po jejich stažení a
    problém s kompatibilitou s PyGTK 2.22.

* verze 1.4 -
	Byla přidána možnost změnit adresář pro uložení databáze zpráv pro jednotlivé
	účty a možnost otevřít zprávu v externí asociované aplikaci. Dialog pro
	odesílání zpráv byl upraven, aby zobrazoval identifikátor odesílající
	schránky a bylo možno snadno prohlížet a odebírat přílohy. Při odpovědi na
	zprávu se nově kopírují čísla jednací, apod.
	Byla opravena chyba při otevírání příloh v externích aplikacích na některých
	platformách.

* verze 1.3 -
    V této verzi byly zprávy rozděleny na "Současné", tedy ty, které jsou ještě
    k disposici na serveru, a na "Archív", který obsahuje starší zprávy. To by
    mělo přispět ke zrychlení startu aplikace, přepínání účtů a také ke zlepšení
    přehlednosti seznamu zpráv. Informace o účtu byly přesunuty z tabu na
    samostatnou stránku.
    Byla také přidána možnost exportu obálky zprávy do PDF a u obou PDF exportů
    byla přidána možnost automatického otevření vytvořeného souboru v asociované
    aplikaci. Bylo přidáno tlačítko pro vymazání vyhledávacího políčka.
    Bylo opraveno několik chyb - export dodejek v PDF do adresáře s diakritikou
    na WinXP, dotaz na přepsání souboru u všech ukládacích dialogů, hlášení chyb
    při selhání proxy a ztráta údajů o událostech při autentizaci zprávy.

* verze 1.2 -
	V této verzi byla přidána možnost vyhledávání ve zprávách a možnost
	exportovat ZFO zpráv zároveň s přehledem korespondence.
	Opravena byla chyba při zpracování časových údajů v událostech a
	při zpracování časových údajů závislých na letním a zimním čase. V případě,
	že zaznamenáte hodinový posun v časech doručení a dodání, lze tento údaj
	opravit pomocí tlačítka "Ověřit zprávu".

* verze 1.1.2 -
    V této verzi je opravena drobná chyba zpracování časového údaje v dslib.
    
* verze 1.1.1 -
    Tato verze opravuje několik drobných chyb. Jedinou novou funkcí je
    zapamatování posledního použitého adresáře závislé na kontextu.
    
    Knihovna dslib byla upravena pro plnou podporu Pythonu 2.5.

* verze 1.1 -
	Tato verze obsahuje podporu pro autentizaci pomocí kombinace uživatelského
    certifikátu a hesla a možnost ovlivňovat ukládání hesla pro každý účet
    zvlášť. Byla přidána funkce pro export dodejek v PDF a přehledu korespondence
    za určité časové období v HTML. Formát pro zobrazení data byl změněn na
    podobu nastavenou v aktuálním locale.
    
    Knihovna dslib byla optimalizována pro podporu velkých podepsaných zpráv a
    ZFO souborů (u zpráv o velikosti 10 MB došlo ke zrychlení zpracování o dva
    řády).

* verze 1.0 -
    První ostrá verze programu. Od vydání rc1 bylo opraveno několik chyb a
    přidáno několik vylepšení - upozornění na novou verzi dsgui při startu
    programu, možnost přiřadit schránce vlastní označení, možnost rozesílat
    zprávy na více adres najednou, možnost posílat programu jména zfo souborů
    pro prohlížení jako argumentů z příkazové řádky, byla odstraněna závislost
    na externích knihovnách v modulu anydb a několik dalších menších změn.
    
    Nejdůležitější změnou je updatovaná knihovna dslib, která používá nové
    schéma URL pro SOAP služby. Stará URL nebudou ze strany datových schránek
    dostupná po 20.9.2010. 
	  

* verze 1.0-rc1 -
    První release candidate verze programu. Od vydání beta1 byla přidána možnost
    exportu zpráv a dodejek do formátu ZFO, authentizace lokálních i 
    exportovaných zpráv proti serveru Datových schránek, prohlížení příloh u
    odeslaných schránek, prohlížení zpráv a dodejek ze ZFO souborů.
    Byly také vylepšeny některé ikonky. Opraveno bylo několik chyb, zejména
    práce s unikódovými názvy souborů pod Windows XP, update statutu již stažené
    odeslané zprávy. Nově se také při prohlížení zprávy zobrazují informce o
    referenčních číslech a spisových značkách.
    Připraveny byly binární distribuce pro Windows, Mac OS X 10.6 a balíčky pro
    Linuxové distribuce ve formátu deb a rpm.

* verze 1.0-beta1 -
    Beta verze programu. Přidána možnost přihlašování
    certifikátem, přidány další typy schránek kromě OVM, opraveny
    drobné chyby. Pro přihlašování certifikátem přibyla závislost
    v podobě knihovny PyOpenSSL.
    Připravena binární distribuce pro Windows, Mac OS X 10.6 a balíčky pro
    Linuxové distribuce ve formátu deb a rpm.

* verze 0.5.1 -
    Byla opravena chyba při ukládání dat v případě absence
    přihlašovacích údajů k účtu.

* verze 0.5 - 
    Poslední verze ve stavu "technical preview".
    Byl kompletně přepracován systém ukládání zpráv a přibyla nová
    závislost v podobě knihovny SQLAlchemy. Byla přidána kontrola
    certifikátu serveru. Byl přidán konfigurační dialog, který umožňuje
    nastavovat parametry kontroly podpisů zpráv, jejich ukládání,
    ukládání hesel, apod.
    Byla zrušena podpora pro verze Pythonu 2.5 a nižší, zejména vzhledem 
    k absenci modulu ssl pro kontrolu certifikátu serveru. 
    Byla opravena chyba, která znemožňovala spuštění dsgui v adresáři 
    s diakritikou ve jméně pod Windows. Bylo opraveno několik dalších 
    drobných chyb.

* verze 0.4.1 -
    Zpracování podepsaných zpráv bylo výrazně urychleno. 

* verze 0.4 -
    Jedná se stále o "technical preview" :)
    Bylo přidáno kompletní ověřování podpisů zpráv (podpis, certifikát,
    časové razítko). Byla přidána podpora pro změnu hesla přímo z aplikace
    a varování před vypršením hesla. Nativní otevírání příloh funguje
    v Gnome, KDE, Windows a na Mac OS X. Dotazy na server běží ve
    vlastním vlákně a GUI zobrazuje informace o průběhu operace.

* verze 0.3.1 -
    byla opravena chyba při práci s domovským adresářem v české verzi
    Windows XP.

* verze 0.3 -
    Jedná se stále o "technical preview".
    Byla přidána podpora pro GTK 2.14 a tedy některé starší distribuce. 
    Byla přidána podpora pro Maemo 5 (OS např. v Nokia N900), včetně
    nativního menu a instalační dokumentace. 
    Dvojklik na název zprávy v seznamu zpráv otevře zprávu v novém okně
    (vhodné zejména pro malé displeje).
			
* verze 0.2 -
    Jedná se stále o "technical preview".
    Byla přidána možnost správy více uživatelských účtů, podpora pro
    nastavení HTTPS proxy pro připojení k datové schránce (funguje pouze
    v Pythonu 2.6.3 a vyšších) a stránka s informacemi o datové
    schránce. Od této verze program ukládá stažené zprávy a další
    informace o uživateli do domovského adresáře uživatele.
    Na systémech Windows byla opravena chyba při výběru lokalizace.

* verze 0.1 -
    Jedná se o "technical preview", které si klade za cíl především
    demonstrovat možnosti knihovny dslib a umožnit uživatelům OS
    bez oficiální podpory Datových schránek přístup k tomuto
    komunikačnímu kanálu.
             

Podporované platformy
======================

Datovka byla úspěšně testována v následujících operačních systémech a
jejich verzích:

* Linux 
   - testováno na Ubuntu (11.10, 11.04, 10.04), Fedora 12-14, OpenSUSE 11.4,
     Mandriva 2010.1 a Gentoo
   - detaily v INSTALL-LINUX.txt
* Windows
   - testováno na Windows XP, Windows Vista a Windows 7
   - detaily v INSTALL-WIN.txt
* MacOS X
   - testováno na MacOS X 10.6 Snow Leopard a MacOS X 10.7 Lion
   - detaily v INSTALL-MACOSX.txt
* FreeBSD
   - v nových verzích není rutinně testováno
   - detaily v INSTALL-FREEBSD.txt
* Maemo (systém některých telefonů Nokia založený na Debianu)
   - testováno na Maemo 5 (Nokia N900)
   - detaily v INSTALL-MAEMO.txt
   - podporováno od verze 0.3 do verze 0.4.1


Známé problémy a omezení
=========================

* způsob uložení již stažených dat nezaručuje jejich integritu v případě
  ručního zásahu
* ve starších verzích PyGTK než 2.14 GUI nefunguje
