
#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import xml.dom.minidom as dom
import os

# not needed anymore - we bind action by hand now instead of replacing them
def translate_actions_to_events(doc):
  """takes a DOM Document objects of an UI XML.
  It finds all buttons using actions and takes the
  action 'activate' callback and adds it as direct callback
  to the 'clicked' or 'activate' action of the button or menuitem.
  This fixes the UI definition for PyGTK older than 2.16.
  """
  # at first change the required GTK version
  for req in doc.getElementsByTagName("requires"):
    if req.getAttribute("lib") == "gtk+":
      req.setAttribute("version", "2.14")
  button_els = []
  actions = {}
  stocks = {}
  labels = {}
  for object_el in doc.getElementsByTagName("object"):
    if object_el.getAttribute("class") == "GtkAction":
      # this is action
      for child_el in object_el.childNodes:
        if child_el.nodeName == "signal" and child_el.getAttribute("name") == "activate":
          actions[object_el.getAttribute("id")] = child_el.getAttribute("handler")
        elif child_el.nodeName == "property" and child_el.getAttribute("name") == "stock_id":
          stocks[object_el.getAttribute("id")] = child_el
        elif child_el.nodeName == "property" and child_el.getAttribute("name") == "label":
          labels[object_el.getAttribute("id")] = child_el
    else:
      related_action = None
      for property in object_el.childNodes:
        if property.nodeName == "property":
          name = property.getAttribute("name")
          if name == "related_action":
            related_action = property.childNodes[0].nodeValue
            break
      if related_action:
        button_els.append((object_el,related_action))
        object_el.removeChild(property)
  # now process all the buttons and connect them to actions
  for button_el, action_name in button_els:
    # action callbacks
    if action_name in actions:
      # <signal name="clicked" handler="on_gettoolbutton_clicked"/>
      clicked_el = doc.createElement("signal")
      if button_el.getAttribute("class") == "GtkToolButton":
        handler = "clicked"
      else: 
        handler = "activate"
      clicked_el.setAttribute("name", handler)
      clicked_el.setAttribute("handler", actions[action_name])
      button_el.appendChild(clicked_el)
    # stock ids
    if action_name in stocks:
      #if button_el.getAttribute("class") == "GtkToolButton":
      # we simply clone the stock id from the action to the button
      button_el.appendChild(stocks[action_name].cloneNode(deep=True))
    # labels
    if action_name in labels:
      # we simply clone the label from the action to the button
      button_el.appendChild(labels[action_name].cloneNode(deep=True))

def fix_requires(doc):
  # at first change the required GTK version
  for req in doc.getElementsByTagName("requires"):
    if req.getAttribute("lib") == "gtk+":
      req.setAttribute("version", "2.14")

def bind_nonmenu_action_references(doc, builder):
  """manually bind actions to non-menu widgets"""
  for prop_el in doc.getElementsByTagName("property"):
    if prop_el.getAttribute("name") == "related_action":
      if prop_el.parentNode.getAttribute("class") not in ("GtkMenuItem","GtkImageMenuItem"):
        parent_id = prop_el.parentNode.getAttribute('id')
        action_id = prop_el.childNodes[0].nodeValue
        action = builder.get_object(action_id)
        widget = builder.get_object(parent_id)
        if action and widget:
          action.connect_proxy(widget)
        else:
          print "Action or proxy widget not found:", action, widget, parent_id

def remove_requires(doc):
  for req in doc.getElementsByTagName("requires"):
    if req.getAttribute("lib") == "gtk+":
      req.parentNode.removeChild(req)


def manually_create_menu(doc, menuid, bind_class, builder):
  """
  doc = DOM Document or Element instance
  menuid = id of menu element to be read
  bind_class = class where to bind the actions
  builder = gtk.Builder instance that can be used to retreive related objects
  """
  menu_el = None
  for el in doc.getElementsByTagName("object"):
    if el.getAttribute("class") == "GtkMenuBar" and el.getAttribute("id") == menuid:
      menu_el = el
      break
  else:
    raise ValueError("GtkMenuBar with id '%s' was not found" % menuid)
  menubar = _manually_create_widget(menu_el, bind_class, builder)
  return menubar
  
def _manually_create_widget(el, bind_class, builder):
  import gtk
  clsname = el.getAttribute('class').replace("Gtk","")
  cls = getattr(gtk, clsname)
  properties = {}
  signals = {}
  children = []
  for child_el in el.childNodes:
    if isinstance(child_el, dom.Element):
      if child_el.nodeName == "child":
        for subchild_el in child_el.childNodes:
          if isinstance(subchild_el, dom.Element):
            children.append(_manually_create_widget(subchild_el, bind_class, builder))
      elif child_el.nodeName == "property":
        prop_name = child_el.getAttribute("name")
        value = child_el.childNodes[0].nodeValue
        properties[prop_name] = value
      elif child_el.nodeName == "signal":
        sig_name = child_el.getAttribute("name")
        handler = child_el.getAttribute("handler")
        signals[sig_name] = handler
  # instance creation
  if clsname == "ImageMenuItem":
    if properties.get("label","").startswith("gtk-"):
      properties["stock_id"] = properties["label"]
    copied_attrs = ["stock_id"]
  else:
    copied_attrs = ["label"]
  args = dict([(str(k),v) for k,v in properties.iteritems() if k in copied_attrs])
  instance = cls(**args)
  # children
  for child in children:
    if clsname == "MenuItem":
      instance.set_submenu(child)
    else:
      instance.append(child)
  # related actions
  related_action = properties.get("related_action", None)
  if related_action:
    action_obj = builder.get_object(related_action)
    if action_obj:
      action_obj.connect_proxy(instance)
    else:
      print "Unknown action:", related_action
  return instance
  
def remove_incompatible_parts(xml_string, version):
  import xml.sax
  from xml.sax.saxutils import XMLGenerator
  from cStringIO import StringIO

  class RemovingHandler(xml.sax.ContentHandler):
    def __init__(self, writer, version):
      xml.sax.ContentHandler.__init__(self)
      self.writer = writer
      self.version = version
      self._above = []
      self._ignore_until = None
      self._just_removed = False
    def startElement(self, name, attrs):
      #print len(self._above), self._above, name, self._ignore_until, self._ignore()
      self._above.append(name)
      if self._ignore():
        return
      if self.version < (2,16,0):
        if (name == "property" and attrs.get("name") == "orientation") or \
           (name == "object" and attrs.get("class") == "GtkMenuBar") or \
           (name == "packing" and self._just_removed) or \
           (name == "property" and attrs.get("name") == "use_action_appearance") or \
           (name == "property" and attrs.get("name") == "related_action") or \
           (name == "property" and attrs.get("name") == "sort_column_id") or \
           (name == "requires" and attrs.get("lib") == "gtk+"):
          self._ignore_until = len(self._above)
        else:
          self.writer.startElement(name, attrs)
      else:
        self.writer.startElement(name, attrs)
      self._just_removed = False
    def endElement( self, name):
      if not self._ignore():
        self.writer.endElement(name)
      self._above.pop(-1)
    def characters( self, data):
      if not self._ignore():
        self.writer.characters(data)
    def _ignore(self):
      if self._ignore_until == None:
        return False
      if len(self._above) < self._ignore_until:
        self._ignore_until = None   
        self._just_removed = True     
        return False
      return True
  
  out = StringIO()
  writer = XMLGenerator(out, encoding="utf-8")
  handler = RemovingHandler(writer, version)
  xml.sax.parseString(xml_string, handler)
  return out.getvalue()


def compatibilize_xml_string(xml_string, version):
  if version < (2,16,0):
    xml_string = xml_string.replace('<requires lib="gtk+" version="2.16"/>',"")
  return xml_string

def compatibilize_xml_file(filename, version):
  def _read_file(filename):
    f = open(filename, "r")
    text = f.read()
    f.close()
    return text
  # shortcut
  return compatibilize_xml_string(_read_file(filename), version)


