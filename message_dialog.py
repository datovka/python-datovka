# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for display of messages
"""

import pygtk
import gtk
pygtk.require("2.0")

class DsMessageDialog(object):
  
  def __init__(self, parent, message=None, show_attachments=False,
               warn_about_old_messages=True):
    self.parent = parent
    self.message = message
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("message_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.textview = self.builder.get_object("show_message_dialog_textview")
    self.parent._create_text_tags(self.textview.get_buffer())
    self.dialog = self.builder.get_object("message_dialog")
    self.attachment_store = self.builder.get_object("received_attachment_store")
    self.attachment_treeview = self.builder.get_object("received_attachment_treeview")
    if show_attachments:
      self.builder.get_object("attachment_box").show()
    self.parent.show_received_message(message,
                                      textview=self.textview,
                                      attachment_store=self.attachment_store,
                                      warn_about_old_messages=warn_about_old_messages,
                                      show_id=True
                                      )
    self.dialog.set_transient_for(self.parent.window1)
    if self.message:
      self.dialog.set_title(_("Message %s")%message.dmID)
    
  # basic emulation of a Dialog

  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()

  # callback handlers
  
  def on_save_attachment_activate(self, w):
    model, paths = self.attachment_treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      attachment = self.message.dmFiles[model[paths[0]][0]]
      self.parent.on_save_attachment_activate(w, self.message, attachment)

  def on_open_attachment_activate(self, w):
    model, paths = self.attachment_treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      attachment = self.message.dmFiles[model[paths[0]][0]]
      self.parent.on_open_attachment_activate(w, self.message, attachment)
      
  def on_received_attachment_treeview_row_activated(self, tree, path, column):
    self.builder.get_object("open_attachment").activate()

  def on_received_attachment_treeview_button_press_event(self, treeview, event):
    if event.button == 3:
      x = int(event.x)
      y = int(event.y)
      time = event.time
      pthinfo = treeview.get_path_at_pos(x, y)
      if pthinfo is not None:
        path, col, cellx, celly = pthinfo
        treeview.grab_focus()
        treeview.set_cursor( path, col, 0)
        popup = self.builder.get_object("attachment_popup")
        popup.popup(None, None, None, event.button, time)
      return True