# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
#*    USA

"""
Here resides a separate dialog for finding Data Boxes
"""

import pygtk
import gtk
pygtk.require("2.0")
from dslib import models


class DsContactsDialog(object):
  
  def __init__(self, parent, contacts, trans_window=None):
    """
    parent = GUI instance
    if trans_window is given, make the dialog transitional to this window
    """
    self.parent = parent
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("contacts_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("contacts_dialog")
    self.filter_entry = self.builder.get_object("filter_entry")
    # data model with filtering
    self.found_data_boxes_store = self.builder.get_object("found_data_boxes_store")
    self.found_data_boxes_filtered = self.found_data_boxes_store.filter_new()
    self.found_data_boxes_filtered.set_visible_column(4)
    self.found_data_boxes_sorted = gtk.TreeModelSort(self.found_data_boxes_filtered)
    # the treeview for the data model
    self.found_data_boxes_treeview = self.builder\
                                      .get_object("found_data_boxes_treeview")
    self.found_data_boxes_treeview.set_model(self.found_data_boxes_sorted)
    for contact in sorted(contacts):
      self.found_data_boxes_store.append([contact[0],contact[1],
                                          contact[2], False, True])
    if trans_window:
      self.dialog.set_transient_for(trans_window)

  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()

  # ========== Callbacks ==========

  def on_filter_entry_changed(self, w):
    def uni_lower(txt):
        if not type(txt) is unicode:
            return txt.decode('utf-8').lower()
        return txt.lower()
    text = w.get_text().lower()
    for row in self.found_data_boxes_store:
      row[4] = (row[0] and text in uni_lower(row[0])) or \
               (row[1] and text in uni_lower(row[1])) or \
               (row[2] and text in uni_lower(row[2]))


  def on_clear_filter_button_clicked(self, w):
    self.filter_entry.set_text("")

  def select_toggle_toggled(self, w, row):
    column = 3
    filter_row = self.found_data_boxes_sorted.convert_path_to_child_path(row)
    base_row = self.found_data_boxes_filtered.convert_path_to_child_path(filter_row)
    self.found_data_boxes_store[base_row][column] = \
        not self.found_data_boxes_store[base_row][column]
    for row in self.found_data_boxes_store:
      if row[column]:
        self.builder.get_object("ok_button").set_sensitive(True)
        break
    else:
      self.builder.get_object("ok_button").set_sensitive(False)

  # // end of Callbacks
  
  def get_selected_box_names(self): 
    ret = []
    for row in self.found_data_boxes_store:
      if row[3]:
        # this row is selected
        ret.append([row[0],row[1],row[2]])
    return ret
    
