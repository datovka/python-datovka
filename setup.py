#! /usr/bin/python
# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


# py2app code partly taken from partly inspired by setup.py
# from the Tryton project (www.tryton.org)
# Copyright (C) 2004-2014 Tryton team (hg.tryton.org/tryton/file/971f862eb459/COPYRIGHT)


import sys
import glob
import os
from setuptools import setup
import release

# available languages to pack
langs = [l for l in os.listdir('locale')
         if os.path.isdir('locale/'+l) and
            os.path.exists( 'locale/'+l+'/LC_MESSAGES/datovka.mo')]

#print "found languages:", langs
localizations = [('share/locale/'+lang+'/LC_MESSAGES',
                  ['locale/'+lang+'/LC_MESSAGES/datovka.mo']) for lang in langs]
icons = []
for d in os.listdir('icons'):
  dname = os.path.join('icons',d)
  if os.path.isdir(dname):
    dest_dir = os.path.join('share/icons/hicolor/',d,'apps')
    icon_names = []
    for fname in os.listdir(dname): 
      icon_name = os.path.join(dname, fname)
      if os.path.isfile(icon_name):
        icon_names.append(icon_name)
    if icon_names:
      icons.append((dest_dir,icon_names))

data_files = [('share/datovka/doc', ['doc/index.html','doc/style.css']+
               glob.glob('doc/*.png')),
              ('share/datovka/icons',glob.glob('icons/*.png')+glob.glob('icons/*.svg')),
              ('share/datovka/ui',glob.glob('ui/*.ui')),
              ] + localizations
              
if sys.platform != "darwin":
  data_files += [("share/applications", ["datovka.desktop"]),
                 ("share/icons/hicolor/scalable/apps", glob.glob('icons/*.svg'))
                 ] + icons
# the setup itself
data = dict(
  name = 'datovka',
  version = release.DSGUI_VERSION,
  description = "Datovka (formerly dsgui) is a free graphical interface for \
'Datove schranky'",
  author = "CZ.NIC Labs",
  author_email = "datove-schranky@labs.nic.cz",
  url = "http://labs.nic.cz/datove-schranky/",
  license = "GNU LGPL",
  platforms = ["Unix", "Windows","MacOS X"],
  long_description = "Datovka (formerly dsgui) is a GUI application allowing \
access to 'Datova schranka' (in english 'Databox') - an electronic communication\
interface endorsed by the Czech government.",
  
  packages=['datovka'],
  package_dir = {'datovka': './'},
  data_files = data_files,
  scripts = ['datovka'],
  install_requires=['dslib >= 3.0',"SQLAlchemy >= 0.8"],
  zip_safe=False,
  )

if sys.platform == "darwin":
    data.update(
        options ={ 'py2app': {
            'argv_emulation': False,
            'iconfile': "icons/datovka.icns",
            'includes': "cairo, gio, atk, pango, pangocairo, "
                        "sqlalchemy.dialects.sqlite, gtk.keysyms",
            'packages': "reportlab, dslib",
            'plist': {
                'CFBundleIdentifier': "cz.nic.labs.datovka",
                'CFBundleName': "Datovka"
            }
        }},
        app=['datovka.py'],
        setup_requires=['py2app'],
        install_requires=["SQLAlchemy >= 0.8"],
)

dist = setup(**data)

if sys.platform == 'darwin' and 'py2app' in dist.commands:
    def find_gtk_dir():
        for directory in os.environ['PATH'].split(':'):
            if not os.path.isdir(directory):
                continue
            for file in ('gtk-demo',):
                if os.path.isfile(os.path.join(directory, file)):
                    return os.path.dirname(directory)
        return None

    import shutil
    import re
    from subprocess import Popen, PIPE
    from itertools import chain
    
    gtk_dir = find_gtk_dir()
    gtk_binary_version = Popen(['pkg-config',
            '--variable=gtk_binary_version', 'gtk+-2.0'],
        stdout=PIPE).stdout.read().strip()

    dist_dir = dist.command_obj['py2app'].dist_dir
    resources_dir = os.path.join(dist_dir, 'Datovka.app', 'Contents',
        'Resources')
    gtk_2_dist_dir = os.path.join(resources_dir, 'lib', 'gtk-2.0')
    pango_dist_dir = os.path.join(resources_dir, 'lib', 'pango')
    config_dir = os.path.join(resources_dir, 'etc')
    os.mkdir(config_dir)
    os.mkdir(os.path.join(config_dir, 'pango'))
    os.mkdir(os.path.join(config_dir, 'gtk-2.0'))
    gtkrc_file = os.path.join(config_dir, "gtk-2.0", "gtkrc")
    
    shutil.copytree(os.path.join(gtk_dir, 'lib', 'pango'), pango_dist_dir,
                    ignore=shutil.ignore_patterns('*.la', '*.a'))

    query_pango = Popen(os.path.join(gtk_dir, 'bin', 'pango-querymodules'),
            stdout=PIPE).stdout.read()
    query_pango = query_pango.replace(gtk_dir,
        '@executable_path/../Resources')
    pango_modules_path = os.path.join(config_dir, 'pango', 'pango.modules')
    with open(pango_modules_path, 'w') as pango_modules:
        pango_modules.write(query_pango)

    with open(os.path.join(config_dir, 'pango', 'pangorc'), 'w') as pangorc:
        pangorc.write('[Pango]\n')
        pangorc.write('ModuleFiles=etc/pango/pango.modules\n')

    query_loaders = Popen(os.path.join(gtk_dir, 'bin',
            'gdk-pixbuf-query-loaders'), stdout=PIPE).stdout.read()
    loader_dir, = re.findall('# LoaderDir = (.*)', query_loaders)
    loader_pkg = (loader_dir.replace(os.path.join(gtk_dir, 'lib'),
            '').split(os.path.sep)[-3])
    loader_dist_dir = os.path.join(resources_dir, 'lib', loader_pkg,
            gtk_binary_version, 'loaders')
    if os.path.isdir(loader_dist_dir):
        shutil.rmtree(loader_dist_dir)
    if os.path.isdir(loader_dir):
        shutil.copytree(loader_dir, loader_dist_dir,
                        ignore=shutil.ignore_patterns('*.la', '*.a'))
    query_loaders = query_loaders.replace(os.path.join(gtk_dir, ''),
        '')

    loaders_path = os.path.join(config_dir, 'gtk-2.0', 'gdk-pixbuf.loaders')
    with open(loaders_path, 'w') as loaders:
        loaders.write(query_loaders)

    if os.path.isdir(os.path.join(gtk_2_dist_dir, gtk_binary_version,
                'immodules')):
        shutil.rmtree(os.path.join(gtk_2_dist_dir, gtk_binary_version,
                'immodules'))
    shutil.copytree(
            os.path.join(gtk_dir, 'lib', 'gtk-2.0', gtk_binary_version, 'immodules'),
            os.path.join(gtk_2_dist_dir, gtk_binary_version, 'immodules'),
            ignore=shutil.ignore_patterns('*.la', '*.a'))

    query_immodules = Popen(os.path.join(gtk_dir, 'bin',
            'gtk-query-immodules-2.0'), stdout=PIPE).stdout.read()
    query_immodules = query_immodules.replace(gtk_dir,
        '@executable_path/../Resources')

    immodules_path = os.path.join(config_dir, 'gtk-2.0', 'gtk.immodules')
    with open(immodules_path, 'w') as immodules:
        immodules.write(query_immodules)

    shutil.copytree(
            os.path.join(gtk_dir, 'lib', 'gtk-2.0', 'modules'),
            os.path.join(gtk_2_dist_dir, 'modules'),
            ignore=shutil.ignore_patterns('*.la', '*.a'))
    shutil.copytree(
            os.path.join(gtk_dir, 'lib', 'gtk-2.0', gtk_binary_version, 'engines'),
            os.path.join(gtk_2_dist_dir, gtk_binary_version, 'engines'),
            ignore=shutil.ignore_patterns('*.la', '*.a'))

    for lang in langs:
        for mo_file in glob.glob(os.path.join(gtk_dir, 'share', 'locale', lang,
                                              'LC_MESSAGES', '*.mo')):
            shutil.copy(mo_file, os.path.join(resources_dir, 'share', 'locale',
                                              lang, 'LC_MESSAGES'))
    
    # share/mime/mime.cache is important, without it gdk-pixbuf is failing mysteriously
    share_mime_dir = os.path.join(resources_dir, 'share', 'mime')
    os.mkdir(share_mime_dir)
    shutil.copy(os.path.join(gtk_dir, 'share', 'mime', 'mime.cache'),
                share_mime_dir)

    # fix paths within shared libraries
    print "\nfixing paths within shared libraries"

    for library in (os.path.join(root, fname)
            for root, _dir, fnames in chain(
                    os.walk(gtk_2_dist_dir),
                    os.walk(pango_dist_dir),
                    os.walk(loader_dist_dir))
            for fname in fnames
            if fname.endswith(".so")):
        print library
        libs = [lib.split('(')[0].strip()
            for lib in Popen(['otool', '-L', library],
                stdout=PIPE).communicate()[0].splitlines()
            if 'compatibility' in lib]
        libs = dict(((lib, None) for lib in libs if gtk_dir in lib))
        for lib in libs.keys():
            fixed = lib.replace(gtk_dir + '/lib',
                '@executable_path/../Frameworks')
            Popen(['install_name_tool', '-change', lib, fixed,
                    library]).wait()

    # create shell file which sets up all the necessary environmental variables
    macos_dir = os.path.join(dist_dir, 'Datovka.app', 'Contents', 'MacOS')
    launcher_file = os.path.join(macos_dir, 'Datovka')
    os.rename(launcher_file, os.path.join(macos_dir, 'Datovka.bin'))
    with open(launcher_file, 'w') as launcher_script:
        launcher_script.write(
"""#!/bin/bash

# find out where the contents of the bundle is

PWD=`pwd`
DIRNAME=`dirname "$0"`
if [[ "$DIRNAME" = /* ]]; then
    # absolute path
    CONTENTS_DIR=$DIRNAME
else
    if [[ "$DIRNAME" = "." ]]; then
        CONTENTS_DIR=$PWD;
    else
        CONTENTS_DIR=$PWD/$DIRNAME
    fi
fi

CONTENTS_DIR=`dirname "$CONTENTS_DIR"`

# setup some environment variables
export FONTCONFIG_FILE="$CONTENTS_DIR/Resources/etc/fonts/fonts.conf"
export GDK_PIXBUF_MODULE_FILE="$CONTENTS_DIR/Resources/etc/gtk-2.0/gdk-pixbuf.loaders"
export PANGO_RC_FILE="$CONTENTS_DIR/Resources/etc/pango/pangorc"
export GTK2_RC_FILES="$CONTENTS_DIR/Resources/etc/gtk-2.0/gtkrc"
export LOCALEDIR="$CONTENTS_DIR/Resources/share/locale"
export XDG_DATA_DIRS="$CONTENTS_DIR/Resources/share"
export DSGUI_DATA_DIR="$CONTENTS_DIR/Resources"

# run the main executable
exec $CONTENTS_DIR/MacOS/Datovka.bin

"""
)
    os.chmod(launcher_file, 0755)
    
    with open(gtkrc_file, "w") as gtkrc_fh:
        gtkrc_fh.write(
"""gtk-toolbar-style   = GTK_TOOLBAR_ICONS
gtk-icon-sizes = "gtk-menu=13,13:gtk-small-toolbar=16,16:gtk-large-toolbar=24,24:gtk-dnd=32,32"
gtk-toolbar-icon-size = large-toolbar
gtk-button-images = 0
gtk-menu-images = 1
"""
)

