#!/usr/bin/env python
# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


import os, sys
from cStringIO import StringIO 
import shutil


def update_file(fname, license):
  out = StringIO()
  f = file(fname, 'r')
  added = False
  skipped = 0 # 0=not yet, 1=some, 2=finished
  for line in f:
    if not added:
      if not line.startswith("#"):
        out.write(license)
        added = True
    if line.startswith("#*") and skipped in (0,1):
      skipped = 1
      continue
    if not line.startswith("#*") and skipped == 1:
      # we already skipped something and we found something
      # not #* - we will not skip anything else
      skipped = 2
    out.write(line)
  f.close()
  shutil.copy(fname, fname+".bak")
  f = file(fname, 'w')
  f.write(out.getvalue())
  f.close()
  return added, bool(skipped)

#* THIS REMAINS

def get_license(fname):
  out = StringIO()
  out.write('\n')
  f = file(fname, 'r')
  for line in f:
    out.write("#*"+line)
  f.close()
  #out.write('\n')
  return out.getvalue()

if __name__ == "__main__":
  l = get_license("license_header.txt")
  if len(sys.argv) <= 1:
    print "Give me some filenames."
  for fname in sys.argv[1:]:
    print fname,
    added, updated = update_file(fname, l)
    if added and updated:
      print ".. updated"
    elif added:
      print ".. added"
    else:
      print ".. not added!"