
#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import gtk
import os
import sys

class IconManager(object):
  
  name_to_pixbuf = {}
  
  @classmethod
  def get_pixbuf_by_name(cls, name):
    return cls.name_to_pixbuf.get(name, None)
  
  @classmethod
  def add_pixbuf(cls, name, pixbuf):
    cls.name_to_pixbuf[name] = pixbuf
  
  @classmethod
  def add_icon(cls, name, filename):
    if os.path.exists(filename):
      encoding = sys.getfilesystemencoding()
      pixbuf = gtk.gdk.pixbuf_new_from_file(filename.decode(encoding))
    else:
      raise ValueError("Icon with filename '%s' does not exists!" % filename)
    cls.name_to_pixbuf[name] = pixbuf
    
  @classmethod
  def read_directory(cls, dirname):
    for fname in os.listdir(dirname):
      if fname.endswith(".png"):
        fullname = os.path.join(dirname, fname)
        name = os.path.splitext(fname)[0]
        cls.add_icon(name, fullname)
    # if PNG is not available, use SVG (this is done for better backward
    # compatibility with installations without SVG support)
    # !! do not load SVG - temporary (hopefully) fix for windows distribution
    #for fname in os.listdir(dirname):
    #  if fname.endswith(".svg"):
    #    fullname = os.path.join(dirname, fname)
    #    name = os.path.splitext(fname)[0]
    #    if name not in cls.name_to_pixbuf:
    #        cls.add_icon(name, fullname)
        
  @classmethod
  def add_composite(cls, name, big, small):
    pixbuf = big.copy()
    w = pixbuf.get_width()
    h = pixbuf.get_height()
    sx = w / 48.0
    sx = h / 48.0 
    pixbuf2 = small
    rint = lambda x: int(round(x))
    pixbuf2.composite(pixbuf,
                      dest_x=rint(sx*6),
                      dest_y=rint(sx*4),
                      dest_width=rint(sx*42),
                      dest_height=rint(sx*42),
                      offset_x=rint(sx*6),
                      offset_y=rint(sx*4),
                      scale_x=sx*0.875,
                      scale_y=sx*0.875,
                      interp_type=gtk.gdk.INTERP_BILINEAR,
                      overall_alpha=255)
    cls.add_pixbuf(name, pixbuf)
    
      
