#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Provides useful functionality for discovering where data files
are and other things specific for one installation.
"""

import sys
import os
import locale

try:
  local_dir = os.path.dirname(__file__)
except NameError:
  local_dir = os.path.dirname(sys.executable)
while os.path.isfile(local_dir):
  # under cxFreeze this can happen because the path will be inside the exe file
  local_dir = os.path.dirname(local_dir)
    
def find_data_directory(name):
  # at first try path set in environment variable
  env_path = os.environ.get("DSGUI_DATA_DIR", None)
  if env_path:
    path = os.path.join(env_path, name)
    if os.path.isdir(path):
      return path
  # then try path relative to this file
  local_path = os.path.join(local_dir, name)
  if os.path.isdir(local_path):
    return local_path
  # try relative path inside an egg
  if os.path.dirname(local_dir).endswith(".egg"):
    local_path = os.path.join(os.path.dirname(local_dir),'share','datovka',name)
    if os.path.isdir(local_path):
      return local_path
  # then the system path
  system_path = os.path.join(sys.prefix, "share", "datovka", name)
  if os.path.isdir(system_path):
    return system_path
  dirs = [env_path, local_path, system_path]
  # try something special for icons
  if name == "icons":
    for prefix in [local_dir, sys.prefix]:
      icon_path = os.path.join(prefix, "share", "icons", "hicolor",
                               "scalable", "apps")
      if os.path.isdir(icon_path) and "datovka.png" in os.listdir(icon_path):
        return icon_path
      else:
        dirs.append(icon_path) 
  raise ValueError("Could not find data dir '%s'. Tried '%s'" % (name, dirs))


def find_translation(lang=None):
  """search for translation 1/ locally, 2/ in the system, 3/ use nulltranslation"""
  import gettext
  kwargs = {}
  if lang:
    kwargs['languages'] = (lang,)
  try:
    return gettext.translation("datovka", os.path.join(local_dir, "locale"),
                               **kwargs)
  except IOError:
    if os.path.dirname(local_dir).endswith(".egg"):
      local_path = os.path.join(os.path.dirname(local_dir),'share','locale')
      try:
        return gettext.translation("datovka", local_path, **kwargs)
      except IOError:
        pass
    try:
      return gettext.translation("datovka", **kwargs)
    except IOError:
      return gettext.NullTranslations()
