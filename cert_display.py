#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  
import gtk
from xml.sax.saxutils import escape
import os_support
import datetime
from configuration import ConfigManager
  
class CertDisplayDialog(object):
  
  def __init__(self, parent, message, download_date=None):
    self.parent = parent
    self.message = message
    self.download_date = download_date
    builder = gtk.Builder()
    self.builder = builder
    gui_xml = self.parent.fix_ui_xml("cert_display.ui")
    try:
      builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      builder.add_from_string(gui_xml.decode('utf-8'), -1)
    builder.connect_signals(self)
    self.dialog = self.builder.get_object("cert_display_dialog")
    # message signature
    builder.get_object("message_signature_heading").\
                 set_markup(self.get_signature_heading())
    builder.get_object("message_signature_icon").\
                set_from_stock(self.get_signature_icon(),
                               gtk.ICON_SIZE_DIALOG)
    # signing certificate
    builder.get_object("signing_cert_heading").set_markup(self.get_cert_heading())
    builder.get_object("signing_cert_icon").\
                    set_from_stock(self.get_cert_icon(),
                                   gtk.ICON_SIZE_DIALOG)
    self._fill_certificate_details()
    # timestamp
    builder.get_object("timestamp_heading").set_markup(self.get_timestamp_heading())
    builder.get_object("timestamp_text").set_markup(self.get_timestamp_details())
    builder.get_object("timestamp_icon").\
                    set_from_stock(self.get_timestamp_icon(),
                                   gtk.ICON_SIZE_DIALOG)
    self.dialog.set_transient_for(self.parent.window1)

  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def on_some_expander_activate(self, w):
    """this tries (not very successfully for now) to re-shrink the
    window when an expander is collapsed"""
    if w.get_expanded() == False:
      import gobject
      def x():
        self.dialog.resize(*self.dialog.size_request())
        return False
      gobject.timeout_add(200, x)
      #self.dialog.resize(*self.dialog.size_request())


  def get_signature_heading(self):
    text = ""
    if not self.message.message_verification_attempted():
      text += _("Message signature is not present.") + "\n"
    else:
      if self.message.is_message_verified():
        verified = self._color(_("Yes"), 'good')
        warning = ""
      else:
        verified = self._color(_("No"), 'bad')
        warning = "<b>"+_("Message signature and content do not correspond!")+"</b>"
      text += "<b>" + _("Valid:") + " " + verified +"</b>" + "\n"
      if warning:
        text += warning + "\n"
    return text
  
  def get_signature_icon(self):
    if not self.message.message_verification_attempted():
      return gtk.STOCK_DIALOG_QUESTION
    else:
      if self.message.is_message_verified():
        return "datovka-ok"
      else:
        return "datovka-error"

  def get_cert_heading(self):
    text = ""
    if not self.message.pkcs7_data or not self.message.pkcs7_data.certificates:
      return _("Message signature is not present.") + "\n" +\
             _("Cannot check signing certificate.") + "\n"
    if ConfigManager.CERTIFICATE_VALIDATION_DATE == ConfigManager.DOWNLOAD_DATE:
      date = self.download_date
    else:
      date = datetime.datetime.now()
    if self.message.was_signature_valid_at_date(date):
      verified = self._color(_("Yes"), 'good')
    elif not ConfigManager.CHECK_CRL:
      # check is missing CRL check is the only problem
      if self._is_only_crl_missing(date):
        verified = self._color(_("Yes"), 'good') + " (" + \
                    _("Certificate revocation check is turned off!") + ")"
      else:
        verified = self._color(_("No"), 'bad')
    else:
      verified = self._color(_("No"), 'bad')
    text += "<b>" + _("Valid:") + " " + verified + "</b>" + "\n"
    return text
  
  def _fill_certificate_details(self):
    builder = self.builder
    if ConfigManager.CERTIFICATE_VALIDATION_DATE == ConfigManager.DOWNLOAD_DATE:
      date = self.download_date
    else:
      date = datetime.datetime.now()
    if self.message.pkcs7_data and self.message.pkcs7_data.certificates:
      cf = CertFormater(self.message.pkcs7_data.certificates[0])
      builder.get_object("signing_cert_text").set_markup(cf.get_detail_html())
      builder.get_object("signing_cert_verification_details").\
                    set_markup(cf.get_verification_html(date))
      builder.get_object("signing_cert_verification_expander").show()
      builder.get_object("signing_cert_details_expander").show()
    else:
      builder.get_object("signing_cert_verification_expander").hide()
      builder.get_object("signing_cert_details_expander").hide()
    
  def _is_only_crl_missing(self, date):
    details = self.message.pkcs7_data.certificates[0].\
                    verification_results_at_date(date)
    if details['CERT_NOT_REVOKED'] == None:
      for k,v in details.items():
        if k != "CERT_NOT_REVOKED" and not v:
          break
      else:
        return True
    return False
  
  def get_cert_icon(self):
    if not self.message.message_verification_attempted():
      return gtk.STOCK_DIALOG_QUESTION
    else:
      if ConfigManager.CERTIFICATE_VALIDATION_DATE == ConfigManager.DOWNLOAD_DATE:
        date = self.download_date
      else:
        date = datetime.datetime.now()
      if self.message.was_signature_valid_at_date(date):
        return "datovka-ok"
      elif not ConfigManager.CHECK_CRL:
        # check is missing CRL check is the only problem
        if self._is_only_crl_missing(date):
          return gtk.STOCK_DIALOG_WARNING
        return "datovka-error"
      else:
        return "datovka-error"
    
  def get_timestamp_heading(self):
    text = ""
    check = self.message.check_timestamp()
    if check == None:
      return _("Timestamp is not present.")
    elif self.message.check_timestamp():
      timestamp_check = self._color(_("Yes"), 'good')
    else:
      timestamp_check = self._color(_("No"), 'bad')
    text += "<b>" + _("Valid:") + " " + timestamp_check + "</b>" + "\n"
    return text

  def get_timestamp_details(self):
    text = ""
    if self.message.tstamp_token:
      timestamp_time = self.message.tstamp_token.get_genTime_as_datetime()
      tst_text = os_support.format_datetime(timestamp_time)
      text += "<b>%s</b>: %s\n" % (_("Time"), tst_text)
      text += "<b>%s</b>:" % _("Issuer")
      text += CertFormater.convert_Name(self.message.tstamp_token.tsa)
    return text
  
  def get_timestamp_icon(self):
    check = self.message.check_timestamp()
    if check == None:
      return gtk.STOCK_DIALOG_QUESTION
    elif check: 
      return "datovka-ok"
    else:
      return "datovka-error"
  
  def _color(self, text, good_bad):
    if good_bad == "good":
      color = "#008800"
    else:
      color = "#880000"
    return '<span color="%s">%s</span>' % (color, text)
    
    
class CertFormater(object):

  attrs = (("version",_("Version")),
           ("serial_number",_("Serial number")), 
           ("signature_algorithm",_("Signature algorithm")),
           ("issuer",_("Issuer")),
           ("validity",_("Validity")),
           ("subject",_("Subject")),
           #("pub_key_info",_("Public key info")),
           #("issuer_uid",_("Issuer UID")),
           #("subject_uid",_("Subject UID")),
           )
  
  name_parts = {'2.5.4.3': _("Name"),
                '2.5.4.6': _("Country"),
                '2.5.4.10': _("Organization"),
                '2.5.4.11': _("Organizational unit"),
                '2.5.4.5': _("Serial number")}
  
  signature_algs = {'1.2.840.113549.1.1.5': 'SHA-1 with RSA Encryption',
                    '1.2.840.113549.1.1.11': 'SHA256 with RSA Encryption'}
  
  verification_keys = (('TRUSTED_CERTS_EXIST', _("Trusted certificates were found")),
                       ('SIGNING_ALG_OK', _("Signing algorithm supported")),
                       ('TRUSTED_PARENT_FOUND', _("Trusted parent certificate found")),
                       ('CERT_TIME_VALIDITY_OK', _("Certificate time validity is ok")),
                       ('CERT_NOT_REVOKED', _("Certificate was not revoked")),
                       ('CERT_SIGNATURE_OK', _("Certificate signature verified")),
                       )
  
  def __init__(self, cert):
    self.cert = cert
    
  def __unicode__(self):
    return unicode(self.cert)
  
  def get_detail_html(self):
    ret = ""
    for attr,name in self.attrs:
      value = getattr(self.cert.tbsCertificate, attr)
      method_name = "convert_"+value.__class__.__name__
      if hasattr(self, method_name):
        text = getattr(self, method_name)(value)
      elif attr == "signature_algorithm":
        text = escape(unicode(value))
        if value in self.signature_algs:
          text += " (%s)" % self.signature_algs[value]
        text += "\n"
      else:
        text = escape(unicode(value))+"\n"
      ret += "<b>%s</b>: %s" % (name, text)
    return ret
  
  def get_verification_html(self, date):
    ret = ""
    verification_details = self.cert.verification_results_at_date(date)
    for key,name in self.verification_keys:
      ok = verification_details.get(key, None)
      add = ""
      if key == "CERT_NOT_REVOKED":
        if ok == None:
          add = "    <i>%s</i>\n" % \
                _("Certificate revocation check is turned off!")
        else:
          rev_date = self.cert.get_revocation_date()
          if rev_date:
            x = _("Certificate was revoked %s") % rev_date
            y = _("Verification date %s") % date
            add = "    <i>%s</i>\n    <i>%s</i>\n" % (x, y)
      if ok == None:
        text = '<span color="#f7910e">%s</span>' % _("Information not available")
      elif ok:
        text = '<span color="#008800">%s</span>' % _("Yes")
      else:
        text = '<span color="#880000">%s</span>' % _("No")
      ret += "<b>%s</b>: %s\n%s" % (name, text, add) 
    return ret
  
  @classmethod
  def convert_Name(cls, name):
    ret = "\n"
    for key,value in sorted(name.get_attributes().iteritems()):
      name = cls.name_parts.get(key, key)
      ret += "   <i>%s</i>: %s\n" % (name, value)
    return ret

  @classmethod
  def convert_ValidityInterval(cls, interval):
    ret = "\n"
    from_date = os_support.format_datetime(interval.get_valid_from_as_datetime())
    to_date = os_support.format_datetime(interval.get_valid_to_as_datetime())
    ret += "   <i>%s</i>: %s\n" % (_("Valid from"), from_date)
    ret += "   <i>%s</i>: %s\n" % (_("Valid to"), to_date)
    return ret 

  @classmethod
  def convert_PublicKeyInfo(cls, info):
    import base64
    ret = "\n"
    ret += "   <i>%s</i>: %s\n" % (_("Algorithm"), info.alg)
    modulus = base64.b16encode(info.key['mod'])
    mod = ""
    while modulus:
      mod += modulus[:64] + "\n"
      modulus = modulus[64:]
    ret += "   <i>%s</i>:\n%s\n" % (_("Modulus"), mod)
    ret += "   <i>%s</i>: %s\n" % (_("Exponent"), info.key['exp'])
    return ret 
      
      