#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import gtk

def ask_password(parent,
                 title="Enter password",
                 text="Please enter your password"):
  """pop-up a simple dialog for password"""
  d = gtk.Dialog(title=title,
                 parent=parent,
                 flags=gtk.DIALOG_DESTROY_WITH_PARENT,
                 buttons=(gtk.STOCK_OK, gtk.RESPONSE_OK,
                          gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL))
  d.set_default_response(gtk.RESPONSE_OK)
  box = gtk.HBox()
  d.vbox.pack_start(box)
  box.show()
  l = gtk.Label(text)
  box.pack_start(l, padding=5)
  l.show()
  entry = gtk.Entry()
  entry.set_activates_default(True)
  entry.set_visibility(False)
  box.pack_start(entry, padding=5)
  entry.show()
  d.set_transient_for(parent)
  # run the dialog
  result = d.run()
  if result == gtk.RESPONSE_OK:
    password = entry.get_text()
    ret = True
  else:
    password = None
    ret = False
  d.destroy()
  return (ret, password)