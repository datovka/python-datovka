#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# built-in modules
import tempfile
import os
import shlex
import sys
from threading import Thread
import time
import locale
import datetime

# local modules
from configuration import ConfigManager

DEFAULT_DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DEFAULT_DATE_FORMAT = "%Y-%m-%d"
DEFAULT_TIME_FORMAT = "%H:%M:%S"

FORBIDDEN_CHARS_NT = "\\/:*?\"<>|\0"
FORBIDDEN_CHARS_UNIX = "\/\0"


def create_temp_file(ext, content):
    temp = tempfile.mkstemp(suffix=ext)
    os.write(temp[0], content)
    os.close(temp[0])
    return temp[1]

def create_named_temp_file(fname, content):
  temp_dir = tempfile.gettempdir()
  base, ext = os.path.splitext(fname)
  add = ""
  idx = 1
  while True:
    filename = base + add + ext
    fullname = os.path.join(temp_dir, filename)
    if not os.path.exists(fullname):
      with file(fullname, "wb") as outf:
        outf.write(content)
      return fullname
    else:
      add = "-%d" % idx
      idx += 1
       

def open_file_content_in_associated_app(fname, content):
  ext = os.path.splitext(fname)[1]
  temp_file = create_named_temp_file(fname, content)
  return open_file_in_associated_app(temp_file)
  

def open_file_in_associated_app(fname):
  done = False
  if os.name == 'nt':
    # WINDOWS
    try:
      os.startfile(fname)
      done = True
    except WindowsError:
      done = False
  else:
    commands = []
    if os.environ.get('DESKTOP_SESSION') == 'gnome':
      # GNOME
      commands.append('gnome-open')
    elif os.environ.get('KDE_FULL_SESSION') == 'true':
      # KDE
      commands.append('kde-open')
    elif sys.platform == 'darwin':
      # Mac OS X
      commands.append('open')
    commands.append('xdg-open')
    # did we find anything?
    import subprocess
    for command in commands:
      if not done:
        try:
          p = subprocess.Popen(shlex.split(command)+[fname], close_fds=True)
          t = MonitorThread(p)
          t.start()
        except Exception, e:
          pass
        else:
          done = True
          break
  return done
   
def get_home_dir(topdir):
  dir = None
  if os.name == "nt":
    try:
      import _winreg as reg
      dir = reg.QueryValueEx(reg.OpenKey(reg.HKEY_CURRENT_USER,
                                         "Volatile Environment"),
                                         "USERPROFILE")[0]
    except EnvironmentError:
      pass
  if not dir:
    dir = os.getenv('HOME') or topdir
  if type(dir) == str:
    dir = dir.decode(sys.getfilesystemencoding())
  return dir

def get_home_app_dir(topdir):
  dir = None
  if os.name == "nt":
    try:
      import _winreg as reg
      dir = reg.QueryValueEx(reg.OpenKey(reg.HKEY_CURRENT_USER,
                                         "Volatile Environment"),
                                         "APPDATA")[0]
    except EnvironmentError:
      pass
  if not dir:
    dir = os.getenv('HOME') or topdir
  if type(dir) == str:
    dir = dir.decode(sys.getfilesystemencoding())
  return dir


def format_datetime(d):
  """formats a datetime according to application wide settings"""
  if not d:
    return ""
  if ConfigManager.DATE_FORMAT == ConfigManager.DATE_FORMAT_DEFAULT:
    if type(d) == datetime.date:
      return d.strftime(DEFAULT_DATE_FORMAT)
    elif type(d) == datetime.time:
      return d.strftime(DEFAULT_TIME_FORMAT)
    else:
      return d.strftime(DEFAULT_DATE_TIME_FORMAT)
  elif ConfigManager.DATE_FORMAT == ConfigManager.DATE_FORMAT_ISO:
    return d.isoformat()
  elif ConfigManager.DATE_FORMAT == ConfigManager.DATE_FORMAT_LOCALE:
    if type(d) == datetime.date:
      try:
        return d.strftime(locale.nl_langinfo(locale.D_FMT))
      except AttributeError:
        # sometimes (most notably on windows) nl_langinfo is not available
        return d.strftime(DEFAULT_DATE_FORMAT)
    elif type(d) == datetime.time:
      try:
        return d.strftime(locale.nl_langinfo(locale.T_FMT))
      except AttributeError:
        # sometimes (most notably on windows) nl_langinfo is not available
        return d.strftime(DEFAULT_TIME_FORMAT)
    else:
      # we do not use locale.D_T_FMT because it is too verbose and thus long
      try:
        tm = d.strftime(locale.nl_langinfo(locale.T_FMT))
        dt = d.strftime(locale.nl_langinfo(locale.D_FMT))
        return "%s %s" % (dt, tm)
      except AttributeError:
        # sometimes (most notably on windows) nl_langinfo is not available
        return d.strftime(DEFAULT_DATE_TIME_FORMAT)
  else:
    # no valid setting matches
    return d.isoformat()


def get_default_text_encoding():
  """Try to find what the OS uses by default to encode text - it is used
  for example to decode localized error messages into unicode strings
  """
  if os.name == 'nt':
    try:
      import _winreg as reg
      cp = reg.QueryValueEx(reg.OpenKey(reg.HKEY_LOCAL_MACHINE,
                        "SYSTEM\CurrentControlSet\Control\Nls\CodePage"),
                        "ACP")[0]
    except:
      cp = "cp1250" # lets use the czech default as the best estimate
  else:
    cp = "utf-8"

def sanitize_file_name(name):
  if os.name == "nt":
    forbidden = FORBIDDEN_CHARS_NT
  else:
    forbidden = FORBIDDEN_CHARS_UNIX
  for char in forbidden:
    name = name.replace(char, "_")
  return name
    

class MonitorThread(Thread):
  """this thread monitors a child process until it dies, so that it does not
  become a zombie"""
  
  def __init__(self, process):
    Thread.__init__(self)
    self.process = process

  def run(self):
    while self.process.poll() == None:
      time.sleep(2)

