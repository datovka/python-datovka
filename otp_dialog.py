# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for OTP authentication
"""

import pygtk
import gtk
pygtk.require("2.0")
import os


class OTPDialog(object):

  def __init__(self, parent, last_problem=None):
    """
    client = dslib client instance
    """
    self.parent = parent
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("otp_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("dialog1")
    self.otp_entry = self.builder.get_object("otp_entry")
    self.dialog.set_transient_for(self.parent.window1)
    # show error
    if last_problem:
      text = _("The code you entered was not valid!")
      self.builder.get_object("problem_label").set_text(text)
      self.builder.get_object("problem_label").show()
    # change text according to login_method and error condition
    main_text = ""
    if self.parent.account.credentials.login_method == "hotp":
      if last_problem:
        main_text = _("Please enter the generated one time code for\n"
                      "account <b>%(account_name)s</b> "
                      "(username <i>%(username)s</i>)\n"
                      "once again:")
      else:
        main_text = _("Please enter the generated one time code for\n"
                      "account <b>%(account_name)s</b> "
                      "(username <i>%(username)s</i>):")
      main_text = main_text % \
                      {'account_name': self.parent.account.name,
                       'username': self.parent.account.credentials.username}
    elif self.parent.account.credentials.login_method == "totp":
      if last_problem:
        main_text = _("Please enter the code from SMS once again:")      
      else:
        main_text = _("An SMS with one time code was sent to you.\nPlease enter \
the obtained value:")
    if main_text:
      self.builder.get_object("main_label").set_markup(main_text)
    self.dialog.set_title(_("Server login for '%s'") % self.parent.account.name)
      

  
  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()
   
  def get_otp(self):
    return self.otp_entry.get_text()
  
  # -- signal handlers
    
  def on_otp_entry_insert_text(self, w, text, length, *args):
    if not text.isdigit():
      w.emit_stop_by_name("insert_text")
      self.builder.get_object("info_label").set_text(
                                                _("Only numbers are allowed!"))
    else:
      self.builder.get_object("info_label").set_text("")
      
  def on_otp_entry_changed(self, w):
    sensitive = (len(w.get_text()) >= 6)
    self.builder.get_object("ok_button").set_sensitive(sensitive)
