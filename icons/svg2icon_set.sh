BASE=`basename $1 .svg`
COMMAND="inkscape -z -e "
CONVERT="convert -colorspace gray "
# 48
$COMMAND $BASE-dialog.png -d 90 $BASE.svg
$CONVERT $BASE-dialog.png $BASE-dialog-inactive.png
# 32
$COMMAND $BASE-dnd.png -d 60 $BASE.svg
$CONVERT $BASE-dnd.png $BASE-dnd-inactive.png
# 24
$COMMAND $BASE-largetoolbar.png -d 45 $BASE.svg
$CONVERT $BASE-largetoolbar.png $BASE-largetoolbar-inactive.png
# 18
$COMMAND $BASE-smalltoolbar.png -d 33.75 $BASE.svg
$CONVERT $BASE-smalltoolbar.png $BASE-smalltoolbar-inactive.png
# 16
$COMMAND $BASE-menu.png -d 30 $BASE.svg
$CONVERT $BASE-menu.png $BASE-menu-inactive.png
cp $BASE-menu.png $BASE-button.png
cp $BASE-menu-inactive.png $BASE-button-inactive.png

