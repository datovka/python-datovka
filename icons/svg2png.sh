BASE=`basename $1 .svg`
COMMAND="inkscape -z -e "
# 512
$COMMAND $BASE-512.png -d 960 $BASE.svg
# 256
$COMMAND $BASE-256.png -d 480 $BASE.svg
# 128
$COMMAND $BASE-128.png -d 240 $BASE.svg
# 64
$COMMAND $BASE-64.png -d 120 $BASE.svg
# 48
$COMMAND $BASE-48.png -d 90 $BASE.svg
# 32
$COMMAND $BASE-32.png -d 60 $BASE.svg
# 24
$COMMAND $BASE-24.png -d 45 $BASE.svg
# 16
$COMMAND $BASE-16.png -d 30 $BASE.svg
