BASE=`basename $1 .svg`
COMMAND="inkscape -z -e "
# 256
mkdir -p 256x256
$COMMAND 256x256/$BASE.png -d 480 $BASE.svg
# 128
mkdir -p 128x128
$COMMAND 128x128/$BASE.png -d 240 $BASE.svg
# 64
mkdir -p 64x64
$COMMAND 64x64/$BASE.png -d 120 $BASE.svg
# 48
mkdir -p 48x48
$COMMAND 48x48/$BASE.png -d 90 $BASE.svg
# 32
mkdir -p 32x32
$COMMAND 32x32/$BASE.png -d 60 $BASE.svg
# 24
mkdir -p 24x24
$COMMAND 24x24/$BASE.png -d 45 $BASE.svg
# 16
mkdir -p 16x16
$COMMAND 16x16/$BASE.png -d 30 $BASE.svg
