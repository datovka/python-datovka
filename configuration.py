#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


class ConfigManager(object):
  """singleton style class for management of configuration"""
  
  # the following are used in setup of CERTIFICATE_VALIDATION_DATE
  DOWNLOAD_DATE = 1
  CURRENT_DATE = 2
  
  DATE_FORMAT_DEFAULT = 3
  DATE_FORMAT_ISO = 2
  DATE_FORMAT_LOCALE = 1
  #DATE_FORMAT_CUSTOM = 4
  
  SELECT_NEWEST = 1
  SELECT_LAST_VISITED = 2
  SELECT_NOTHING = 3
  
  allowed_attributes = ('AUTO_DOWNLOAD_WHOLE_MESSAGES',
                        'DEFAULT_DOWNLOAD_SIGNED',
                        #'STORE_PASSWORDS_ON_DISK',
                        'STORE_MESSAGES_ON_DISK',
                        'STORE_ADDITIONAL_DATA_ON_DISK',
                        'CERTIFICATE_VALIDATION_DATE',
                        'CHECK_CRL',
                        'CHECK_NEW_VERSIONS',
                        'SEND_STATS_WITH_VERSION_CHECKS',
                        'DATE_FORMAT',
                        'LANGUAGE',
                        'AFTER_START_SELECT')
  
  AUTO_DOWNLOAD_WHOLE_MESSAGES = False #
  DEFAULT_DOWNLOAD_SIGNED = True # default downloading method
  STORE_PASSWORDS_ON_DISK = False # should password be saved or always asked
                                  # this is the default value only
                                  # as it is adjustable per-account
  STORE_MESSAGES_ON_DISK = True # when False memory is used - non persistent
  STORE_ADDITIONAL_DATA_ON_DISK = True # account info and last check
  CERTIFICATE_VALIDATION_DATE = DOWNLOAD_DATE
  CHECK_CRL = True
  CHECK_NEW_VERSIONS = True
  SEND_STATS_WITH_VERSION_CHECKS = True
  DATE_FORMAT = DATE_FORMAT_LOCALE
  LANGUAGE = "" # "" means use locale settings
  AFTER_START_SELECT = SELECT_NEWEST

  @classmethod
  def set_config(cls, attr, value):
    assert attr in cls.allowed_attributes
    setattr(cls, attr, value)
    
  @classmethod
  def save_to_config(cls, config):
    section = "preferences"
    config.add_section(section)
    for attr in cls.allowed_attributes:
      config.set(section, attr, getattr(cls, attr))

  @classmethod
  def load_from_config(cls, config):
    section = "preferences"
    if config.has_section(section):
      for attr in cls.allowed_attributes:
        if config.has_option(section, attr):
          try:
            if attr in ("LANGUAGE",):
              cls.set_config(attr, config.get(section, attr))
            elif attr in ("CERTIFICATE_VALIDATION_DATE","DATE_FORMAT",
                          "AFTER_START_SELECT"):
              cls.set_config(attr, config.getint(section, attr))
            else:
              cls.set_config(attr, config.getboolean(section, attr))
          except ValueError:
            # ignore invalid values
            pass
    # always use signed method
    cls.set_config("DEFAULT_DOWNLOAD_SIGNED", True)
  
import pygtk
import gtk
pygtk.require("2.0")
import os


class ConfigDialog(object):

  attr_to_oid = (('AUTO_DOWNLOAD_WHOLE_MESSAGES','download_whole_messages'),
                 ('DEFAULT_DOWNLOAD_SIGNED','download_signed'),
                 ('STORE_PASSWORDS_ON_DISK','store_password'),
                 ('STORE_MESSAGES_ON_DISK','store_messages'),
                 ('STORE_ADDITIONAL_DATA_ON_DISK','store_additional_data'),
                 ('CHECK_CRL','check_crl'),
                 ('CHECK_NEW_VERSIONS','check_new_versions'),
                 ('SEND_STATS_WITH_VERSION_CHECKS', 'send_stats'),
                 ('LANGUAGE', 'lang_combo')
                 )
  
  def __init__(self, parent):
    """
    client = dslib client instance
    """
    self.parent = parent
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("config_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("config_dialog")
    self.lang_model = self.builder.get_object("language_model")
    self._fill_data_from_manager()
    self.builder.get_object("download_whole_messages").toggled()
    self.changed = []
    self.dialog.set_transient_for(self.parent.window1)

  def run(self):
    result = self.dialog.run()
    if result == 1:
      self._set_data_to_manager()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()
    
  # Callbacks
  
  def on_download_whole_messages_toggled(self, w):
    self.builder.get_object("download_signed").set_sensitive(w.get_active())
    
  def on_check_new_versions_toggled(self, w):
    self.builder.get_object("send_stats").set_sensitive(w.get_active())

  # // end of Callbacks
  
  def _fill_data_from_manager(self):
    for attr, oid in self.attr_to_oid:
      widget = self.builder.get_object(oid)
      value = getattr(ConfigManager, attr)
      if oid == "lang_combo":
        # find index
        for i, row in enumerate(self.lang_model):
          if row[1] == value:
            value = i
            break
        else:
          value = 0
      widget.set_active(value)
    if ConfigManager.CERTIFICATE_VALIDATION_DATE == ConfigManager.DOWNLOAD_DATE:
      self.builder.get_object("check_against_download_date").set_active(True)
    else:
      self.builder.get_object("check_against_current_date").set_active(True)
    # after start select
    if ConfigManager.AFTER_START_SELECT == ConfigManager.SELECT_NEWEST:
      oid = "after_start_newest"
    elif ConfigManager.AFTER_START_SELECT == ConfigManager.SELECT_LAST_VISITED:
      oid = "after_start_last_visited"
    elif ConfigManager.AFTER_START_SELECT == ConfigManager.SELECT_NOTHING:
      oid = "after_start_nothing" 
    else:
      oid = "after_start_newest"
    self.builder.get_object(oid).set_active(True)
      
  
  def _set_data_to_manager(self):
    for attr, oid in self.attr_to_oid:
      widget = self.builder.get_object(oid)
      old_value = getattr(ConfigManager, attr)
      new_value = widget.get_active()
      if oid == "lang_combo":
          new_value = self.builder.get_object('language_model')[new_value][1]
      if old_value != new_value:
        # value of this attribute has changed
        self.changed.append(attr)
        ConfigManager.set_config(attr, new_value)
    if self.builder.get_object("check_against_download_date").get_active():
      new_value = ConfigManager.DOWNLOAD_DATE
    else:
      new_value = ConfigManager.CURRENT_DATE
    if new_value != ConfigManager.CERTIFICATE_VALIDATION_DATE:
      self.changed.append("CERTIFICATE_VALIDATION_DATE")
      ConfigManager.set_config("CERTIFICATE_VALIDATION_DATE", new_value)
    # after start select
    if self.builder.get_object("after_start_newest").get_active():
      ConfigManager.AFTER_START_SELECT = ConfigManager.SELECT_NEWEST
    elif self.builder.get_object("after_start_last_visited").get_active():
      ConfigManager.AFTER_START_SELECT = ConfigManager.SELECT_LAST_VISITED
    else:
      ConfigManager.AFTER_START_SELECT = ConfigManager.SELECT_NOTHING
 
