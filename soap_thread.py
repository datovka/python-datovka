
#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from threading import Event, Thread
from dslib.ds_exceptions import DSOTPException

class SoapThread(Thread):
  """thread designed to run a command from the dslib soap interface"""

  def __init__(self, method, args, kwargs=None):
    Thread.__init__(self)
    self.method = method
    self.args = args
    self.kwargs = kwargs or dict()
    self.exception = None
    self.reply = None
    self.canceled = False

  def run(self):
    #print "START", self.method.__name__, self.args
    try:
      self.reply = self.method(*self.args, **self.kwargs)
    except DSOTPException, e:
      self.exception = e
      if e.code == DSOTPException.OTP_CANCELED_BY_USER:
        self.canceled = True
    except Exception, e:
      self.exception = e
    else:
      pass
    #print "END"
    