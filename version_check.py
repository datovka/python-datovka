#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import urllib2
import re
import sys

VERSION_URL = "http://labs.nic.cz/page/708/"
USER_AGENT = "Datovka - %s; %s; Python: %s; %s"

def _strip_html_tags(text):
  text = " ".join(text.split())
  text = text.replace(" <p>","").replace("<p>","").replace("</p>","\n\n")
  return text

def check_version(current, inst_id, send_data=False):
  """
  based on current version returns the name of the newest version or None
  if there is no newer version available.
  As a second return value returns the description of the release
  """
  opener = urllib2.build_opener()
  from dslib.network import ProxyManager
  proxy_handler = ProxyManager.HTTP_PROXY.create_proxy_handler()
  proxy_auth_handler = ProxyManager.HTTP_PROXY.create_proxy_auth_handler()
  if proxy_handler:
    opener.add_handler(proxy_handler)
  if proxy_auth_handler:
    opener.add_handler(proxy_auth_handler)
    
  if send_data:
    pyversion = ".".join([str(x) for x in sys.version_info])
    agent_string = USER_AGENT % (current, sys.platform, pyversion, inst_id)
  else:
    agent_string = USER_AGENT % ("", "", "", "")
  opener.addheaders = [('User-agent', agent_string)]
  try:
    d = opener.open(VERSION_URL, timeout=5)
  except:
    return None, None
  else:
    try:
      data = d.read()
      d.close()
    except:
      return None, None
    versions = re.findall("<h2>verze (.*?)</h2>\s*((?:<p>.*?</p>\s*)+)",
                          data, re.DOTALL)
    numbers = [v[0] for v in versions]
    if current in numbers:
      # current is an official release, we continue
      if numbers.index(current) > 0:
        return versions[0][0], _strip_html_tags(versions[0][1])
  return None, None
