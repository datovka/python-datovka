# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for changing the directory
where data for one databox are stored
"""

import pygtk
import gtk
import sys
import os
pygtk.require("2.0")
import os_support


class DataDirDialog(object):
  
  def __init__(self, parent, original_dir):
    self.parent = parent
    self.original_dir = original_dir
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("change_datadir_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("change_datadir_dialog")
    self.ok_button = self.builder.get_object("ok_button")
    self.file_chooser = self.builder.get_object("datadir_filechooser")
    self.dialog.set_transient_for(self.parent.window1)
    self.file_chooser.set_current_folder(self.original_dir)
    self.builder.get_object("curdir_label").set_markup(
                                          "    <i>%s</i>" % self.original_dir)
    self._update_sensitivity()
    self.new_dir = None

  def run(self):
    result = self.dialog.run()
    self.new_dir = self.file_chooser.get_filename()
    self.copy = self.builder.get_object("copy_data_radio").get_active()
    self.move = self.builder.get_object("move_data_radio").get_active()
    self.leave = self.builder.get_object("leave_data_radio").get_active()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()

  # callback handlers
  
  def on_datadir_filechooser_selection_changed(self, w):
    self._update_sensitivity()
  
  def _update_sensitivity(self):
    dirname = self.file_chooser.get_filename()
    if dirname:
      enc_dirname = dirname.decode('utf-8')
    else:
      enc_dirname = None
    sens = True
    warning = ""
    if not dirname:
      warning = _("No directory was selected")
      sens = False
    elif dirname == self.original_dir:
      warning = _("Cannot use the orginal directory as destination")
      sens = False
    elif not os.access(enc_dirname, os.W_OK):
      warning = _("Application does not have write \
access to the selected directory")
      sens = False
    # update the dialog
    self.ok_button.set_sensitive(sens)
    self.builder.get_object("warning_label").set_text(warning)

  # end of callbacks
