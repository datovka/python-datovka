# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
This module is all in Czech in order to mimic the output of the 602 filler
as closely as possible 
"""
from datetime import datetime
import sys
import os

import os_support
import local

from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch, cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

PAGE_HEIGHT=defaultPageSize[1]
PAGE_WIDTH=defaultPageSize[0]

# FONT REGISTRATION
use_dejavu = False
try:
  font_dir = local.find_data_directory("fonts")
except ValueError:
  # try to find the desired fonts in the system
  for top, dirs, files in os.walk("/usr/share/fonts/"):
    if "DejaVuSans.ttf" in files:
      font_dir = top
      break
  else:
    font_dir = None

if not font_dir:
  print >> sys.stderr, "Cannot find the 'fonts' dir - the output might be ugly"
else:
  font_file = os.path.join(font_dir, 'DejaVuSans.ttf')
  if not os.path.isfile(font_file):
    print >> sys.stderr, ("Cannot find font file for DejaVu - "
                          "the output might be ugly")
  else:
    pdfmetrics.registerFont(TTFont('DejaVu', font_file))
    pdfmetrics.registerFontFamily('DejaVu',normal='DejaVu')
    use_dejavu = True

# STYLES
styles = getSampleStyleSheet()
styles['Normal'].fontSize = 10
if use_dejavu:
  FONT_NAME = 'DejaVu'
  styles['Normal'].fontName = FONT_NAME
  styles['Heading1'].fontName = FONT_NAME
else:
  FONT_NAME = styles['Normal'].fontName


class ReportLabExporter:
  
  @classmethod
  def format_datetime(cls, d):
    if not d:
      return " "
    ret = "%d.%d.%d" % (d.day, d.month, d.year)
    ret += d.strftime(", %H:%M:%S")
    return ret
  
  @classmethod
  def _localize_name(cls, fname):
      if type(fname) == unicode:
          return fname.encode(sys.getfilesystemencoding())
      return fname
  
  @classmethod
  def export_delivery_info(cls, fname, message):
    data = []
    commands = [('FONT',(0,0),(-1,-1), FONT_NAME),
                ('SIZE',(0,0),(0,-1),10),
                ('TOPPADDING',(0,0),(-1,-1),8),
                ('BOTTOMPADDING',(0,0),(-1,-1),8),
                ('VALIGN',(0,0),(-1,-1),"MIDDLE"),
                ('ALIGN',(0,0),(0,-1),"RIGHT")]
    def add_row(heading, text, para=True):
      _text = text or u"Nebylo zadáno"
      if para:
        _text = Paragraph(_text, styles['Normal'])
      data.append([heading+":", _text])
    def add_heading(text):
      data.append([text, ""])
      i = len(data)-1
      commands.append(('BACKGROUND',(0,i),(1,i),"#FFFF00"))
      commands.append(('SPAN',(0,i),(1,i)))
      commands.append(('SIZE',(0,i),(1,i),12))
      commands.append(('LEADING',(0,i),(1,i),16))
      commands.append(('ALIGN',(0,i),(1,i),"LEFT"))
    def yes_no(val):
      return val and u"\u2612" or u"\u2610"

    # the code itself
    doc = SimpleDocTemplate(cls._localize_name(fname))
    # header
    Story = [Table([[u"Doručenka", u"ID zprávy: %s"%message.dmID]],
                   style=[('ALIGN',(1,0),(1,-1),"RIGHT"),
                          ('FONT',(0,0),(-1,-1),FONT_NAME),
                          ('VALIGN',(0,0),(-1,-1),"BOTTOM"),
                          ('SIZE',(1,0),(1,0),12),
                          ('LEADING',(1,0),(1,0),16),
                          ('SIZE',(0,0),(0,0),16),
                          ('LEADING',(0,0),(0,0),20)],
                    colWidths=[8*cm,8*cm]
                    ),
             Spacer(1, 0.5*cm)]
    
    # identification
    add_heading(u"Odesílatel")
    add_row(u"Název", "%s, %s"%(message.dmSender,
                                 message.dmSenderAddress or ""))
    add_heading(u"Příjemce")
    add_row(u"Název", "%s, %s"%(message.dmRecipient,
                                 message.dmRecipientAddress or ""))
    add_heading(u"Obecné informace")
    add_row(u"Věc", message.dmAnnotation)
    legal_row = u"%s    / %s    § %s    odstavec %s    písmeno %s" % \
                (message.dmLegalTitleLaw or "", message.dmLegalTitleYear or "",
                 message.dmLegalTitleSect or "", message.dmLegalTitlePar or "",
                 message.dmLegalTitlePoint or "")
    add_row(u"Zmocnění", legal_row, para=False)
    for name, value in (("dmSenderRefNumber", u"Naše č. j."),
                        ("dmSenderIdent", u"Naše sp. zn."),
                        ("dmRecipientRefNumber", u"Vaše č. j."),
                        ("dmRecipientIdent", u"Vaše sp. zn."),
                        ("dmToHands", u"K rukám"),
                        ):
      add_row(value, getattr(message, name))
    pers_row = u"%s               Zakázat doručení fikcí:   %s"
    pers_row %= (yes_no(message.dmPersonalDelivery),
                 yes_no(not message.dmAllowSubstDelivery))
    add_row(u"Do vl. rukou", pers_row, para=False)
    # Status
    add_heading(u"Informace o dodání a doručení")
    add_row(u"Dodejka", cls.format_datetime(message.dmDeliveryTime))
    add_row(u"Doručenka", cls.format_datetime(message.dmAcceptanceTime))
    #add_row(_("Status"), "%d - %s" % (message.dmMessageStatus, message.get_status_description()))
    # events
    if message.dmEvents:
      add_heading(u"Události")
      for event in message.dmEvents:
        if type(event.dmEventTime) == unicode:
          # string dmEventTime
          if "." in event.dmEventTime:
            etime = datetime.strptime(event.dmEventTime,
                                      "%Y-%m-%d %H:%M:%S.%f")
          else:
            etime = datetime.strptime(event.dmEventTime, "%Y-%m-%d %H:%M:%S")  
        else:
          # already parsed event time
          etime = event.dmEventTime
        add_row(u"Čas", "%s - %s" % (cls.format_datetime(etime),
                                                         event.dmEventDescr))
    
    table = Table(data, colWidths=[3*cm, None], style=commands)
    Story.append(table)
    doc.build(Story)

  @classmethod
  def export_message_envelope(cls, fname, message):
    data = []
    commands = [('FONT',(0,0),(-1,-1),FONT_NAME),
                ('SIZE',(0,0),(0,-1),10),
                ('TOPPADDING',(0,0),(-1,-1),8),
                ('BOTTOMPADDING',(0,0),(-1,-1),8),
                ('VALIGN',(0,0),(-1,-1),"MIDDLE"),
                ('ALIGN',(0,0),(0,-1),"RIGHT")]
    def add_row(heading, text, para=True):
      _text = text or u"Nebylo zadáno"
      if para:
        _text = Paragraph(_text, styles['Normal'])
      data.append([heading+":", _text])
    def add_heading(text):
      data.append([text, ""])
      i = len(data)-1
      commands.append(('BACKGROUND',(0,i),(1,i),"#FFFF00"))
      commands.append(('SPAN',(0,i),(1,i)))
      commands.append(('SIZE',(0,i),(1,i),12))
      commands.append(('LEADING',(0,i),(1,i),16))
      commands.append(('ALIGN',(0,i),(1,i),"LEFT"))
    def yes_no(val):
      return val and u"\u2612" or u"\u2610"

    # the code itself
    doc = SimpleDocTemplate(cls._localize_name(fname))
    # header
    Story = [Table([[u"Obálka", u"ID zprávy: %s"%message.dmID]],
                   style=[('ALIGN',(1,0),(1,-1),"RIGHT"),
                          ('FONT',(0,0),(-1,-1),FONT_NAME),
                          ('VALIGN',(0,0),(-1,-1),"BOTTOM"),
                          ('SIZE',(1,0),(1,0),12),
                          ('LEADING',(1,0),(1,0),16),
                          ('SIZE',(0,0),(0,0),16),
                          ('LEADING',(0,0),(0,0),20)],
                    colWidths=[8*cm,8*cm]
                    ),
             Spacer(1, 0.5*cm)]
    
    # identification
    add_heading(u"Odesílatel")
    add_row(u"Název", "%s, %s"%(message.dmSender,
                                 message.dmSenderAddress or ""))
    add_row(u"ID schránky", "%s                          Typ schránky:  %s"%\
            (message.dbIDSender, message.get_sender_type()), para=False)
    
    add_heading(u"Příjemce")
    add_row(u"Název", "%s, %s"%(message.dmRecipient,
                                 message.dmRecipientAddress or ""))
    add_row(u"Dodáno", cls.format_datetime(message.dmDeliveryTime))
    
    add_heading(u"Obecné informace")
    add_row(u"Věc", message.dmAnnotation)
    legal_row = u"%s    / %s    § %s    odstavec %s    písmeno %s" % \
                (message.dmLegalTitleLaw or "", message.dmLegalTitleYear or "",
                 message.dmLegalTitleSect or "", message.dmLegalTitlePar or "",
                 message.dmLegalTitlePoint or "")
    add_row(u"Zmocnění", legal_row, para=False)
    for name, value in (("dmSenderRefNumber", u"Naše č. j."),
                        ("dmSenderIdent", u"Naše sp. zn."),
                        ("dmRecipientRefNumber", u"Vaše č. j."),
                        ("dmRecipientIdent", u"Vaše sp. zn."),
                        ("dmToHands", u"K rukám"),
                        ):
      add_row(value, getattr(message, name))
    pers_row = u"%s               Zakázat doručení fikcí:   %s"
    pers_row %= (yes_no(message.dmPersonalDelivery),
                 yes_no(not message.dmAllowSubstDelivery))
    add_row(u"Do vl. rukou", pers_row, para=False)
     # attachments
    add_heading(u"Přílohy")
    for dmfile in message.dmFiles:
      data.append(["",Paragraph(dmfile._dmFileDescr, styles['Normal'])])
    
    table = Table(data, colWidths=[3*cm, None], style=commands)
    Story.append(table)
    doc.build(Story)

