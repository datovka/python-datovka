#!/usr/bin/env python
# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import time
import datetime
START_TIME = time.time()

import os
import sys
import re
try:
  topdir = os.path.abspath(os.path.dirname(__file__))
except NameError:
  # __file__ is not defined in frozen builds using py2exe or cx_Freeze
  topdir = os.path.abspath(os.path.dirname(sys.executable))
topdir = topdir.decode(sys.getfilesystemencoding())

import locale
# gettext installation
if sys.platform == "darwin":
  # mac os x hack for locale settings
  lang = os.getenv('LANG')
  if lang is None:
    try:
      from CoreFoundation import CFBundleCopyLocalizationsForPreferences
    except:
      pass
    else:
      mac_langs = CFBundleCopyLocalizationsForPreferences(['en','cs'], None)
      if mac_langs:
        lang = mac_langs[0]
  if lang:
    os.environ['LANG'] = lang
    os.environ['LANGUAGE'] = lang
elif os.name == 'nt':
  # windows hack for locale settings
  lang = os.getenv('LANG')
  if lang is None:
    default_lang, default_enc = locale.getdefaultlocale()
    if default_lang:
      lang = default_lang
  if lang:
    os.environ['LANG'] = lang
    os.environ['LANGUAGE'] = lang
try:
  locale.setlocale(locale.LC_ALL)
except Exception as exc:
  pass
# search for translations
import local
tr = local.find_translation()
tr.install(names=("ngettext",))

# import and check gtk availability
GTK_REQUIRE = "2.0"
try:
  import pygtk
  pygtk.require("2.0") 
except:
  print >> sys.stderr, _("Required PyGTK 2.0 was not found - try installing \
python-gtk, pygtk or similar package.")
  sys.exit()
try:
  import gtk
except:
  print >> sys.stderr, _("Python module 'gtk' was not found - try installing \
python-gtk, pygtk or similar package.")
  sys.exit()

# check version of Python and availability of the ssl library
try:
  import ssl
except ImportError:
  # there is no ssl
  message = _("Your Python installation does not contain the <b>ssl</b> module \
used to verify server certificates, etc. For security reasons, Datovka does not\
 support such installations.")
  ver = sys.version_info[:3]
  if ver < (2,6):
    # python is older than 2.6 - no wonder there is not ssl
    message += "\n\n<i>" + _("HINT: Your version of Python is too old (%s), \
please install at least version 2.6") % (".".join(map(str, ver))) + "</i>"
  message += "\n\n<i>" + _("HINT: You can use version 0.4.1 of dsgui which \
does not require the 'ssl' module.") + "</i>"
  d = gtk.MessageDialog(None, message_format=message,
                        buttons=gtk.BUTTONS_OK, type=gtk.MESSAGE_ERROR)
  d.set_property("use-markup",True)
  d.run()
  d.destroy()
  sys.exit()
  
# check gtk version
GTK_REQUIRED_MINIMUM = (2,14,0)
check = gtk.check_version(*GTK_REQUIRED_MINIMUM)
if check: # check did not pass
  current_version = gtk.gtk_version
  message = _("""Your version of GTK is older that the required minimum.
The application will continue, but it will probably not work properly.

Information about versions:
  Your version: %d.%d.%d
  Required version: %d.%d.%d""") % (current_version+GTK_REQUIRED_MINIMUM)
  d = gtk.MessageDialog(None, message_format=message,
                        buttons=gtk.BUTTONS_OK, type=gtk.MESSAGE_WARNING)
  d.run()
  d.destroy()
del check

# check for sqlalchemy
alchemy_problem = None
try:
  import sqlalchemy
except:
  # no sqlalchemy
  alchemy_problem = _("SQLAlchemy could not be imported. \
Please install the sqlalchemy package.")
else:
  version = []
  for part in sqlalchemy.__version__.split("."):
    # unfortunately there can be letters in the version string
    m = re.search("^\d+",part)
    if m:
      version.append(int(m.group(0)))
    else:
      version.append(0)
  version = tuple(version)
  if version < (0,5,0):
    # version should be at least 0.5.0
    alchemy_problem = _("Your version of SQLAlchemy is too old. \
You need at least 0.5.0, but you have %s. Please try upgrading it.") %\
     sqlalchemy.__version__
if alchemy_problem:
  message = _("Since version 0.5, Datovka requires SQLAlchemy for its function.\
 The following problem with this package was found on your system:")
  message += "\n\n" + "<i>"+alchemy_problem+"</i>"
  message += "\n\n" + _("Datovka cannot be started, until this problem is fixed.")
  d = gtk.MessageDialog(None, message_format=message,
                        buttons=gtk.BUTTONS_OK, type=gtk.MESSAGE_ERROR)
  d.set_property("use-markup",True)
  d.run()
  d.destroy()
  sys.exit()

# check reportlab
PDF_EXPORT_AVAILABLE = False
try:
  import export
except:
  pass
else:
  PDF_EXPORT_AVAILABLE = True
  
# install custom top-level exception hook
from error_handling import show_exception_dialog, ErrorWrapper
sys.excepthook = show_exception_dialog
  
# everything went OK, we can finally start the application
import release
import pango
import base64
from dslib.properties.properties import Properties
#Properties.CHECK_CRL = False
from dslib.client import Client
from dslib.certs.cert_manager import CertificateManager
from dslib.ds_exceptions import DSOTPException, DSNotAuthorizedException, \
        DSSOAPException, DSServerCertificateException
from ds_search import DsSearchDialog
from ds_send import DsSendDialog
from proxy_dialog import ProxyDialog
from credentials_dialog import CredentialsDialog
from xml_translator import translate_xml_string
from icon_manager import IconManager
import gobject
import threading
import inspect
from account import Credentials, Account
import os_support
from configuration import ConfigManager
from password_dialog import ask_password
from dslib import local as dslib_local
from message_dialog import DsMessageDialog
import version_check
from message_store import DsdbMessageStore
import unicodedata
import webbrowser
import ConfigParser
import codecs
try:
    import gtkosx_application as QuartzApp
except ImportError:
    QUARTZ_ENABLE = False
else:
    QUARTZ_ENABLE = True

class GUI(object):

  class SoapReply(object):
    def __init__(self, reply, error=None, canceled=False, error_shown=False):
      self.reply = reply
      self.error = error
      self.canceled = canceled
      self.error_shown = error_shown
    def fatal_error(self):
      fatal_error = False
      if self.error and not isinstance(self.error, DSSOAPException):
        fatal_error = True
      return (self.canceled or fatal_error)
    def soap_error(self):
      if self.error and isinstance(self.error, DSSOAPException):
        return True
      return False
    def has_reply_data(self):
      if self.reply and self.reply.data:
        return True
      return False

  topdir = topdir
  # portable - in this mode config file is searched in the directory where
  # Datovka resides and password storage is not allowed
  portable = False
  date_time_format = "%Y-%m-%d %H:%M"
  account_info_attrs = (("dbID",_("Data box ID")),
                        ("dbType",_("Data box type")),
                        ("ic",_("IČ")),
                        ("pnFirstName",_("Given name")),
                        ("pnMiddleName",_("Middle name")),
                        ("pnLastName",_("Surname")),
                        ("pnLastNameAtBirth",_("Surname at birth")),
                        ("firmName",_("Firm name")),
                        ("biDate",_("Date of birth")),
                        ("biCity",_("City of birth")),
                        ("biCounty",_("County of birth")),
                        ("biState",_("State of birth")),
                        ("adCity",_("City of residence")),
                        ("adStreet",_("Street of residence")),
                        ("adNumberInStreet",_("Number in street")),
                        ("adNumberInMunicipality",_("Number in municipality")),
                        ("adZipCode",_("Zip code")),
                        ("adState",_("State of residence")),
                        ("nationality",_("Nationality")),
                        ("dbEffectiveOVM",_("Effective OVM")),
                        ("dbOpenAddressing",_("Open addressing")),
                      )
  
  message_type_to_text = {"K": _("Postal data message"),
                          "I": _("Initializing postal data message"),
                          "O": _("Reply postal data message"),
                          "X": _("Initializing postal data message - expired"),
                          "Y": _("Initializing postal data message - used"),
                          }
  
  author_type_to_text = {
      "PRIMARY_USER": _("Primary user"), #oprávněná osoba nebo likvidátor),
      "ENTRUSTED_USER": _("Entrusted user"), # (pověřená osoba),
      "ADMINISTRATOR": _("Administrator"), # (systémové DZ)
      "OFFICIAL": _("Official"), # (systémové DZ)
      "VIRTUAL": _("Virtual") # (spisovka)
  }
  
  trusted_certificate_dir = dslib_local.find_data_directory("trusted_certificates")
  CertificateManager.read_trusted_certificates_from_dir(trusted_certificate_dir)
  progressbar = None
  gui_lock = threading.Lock()
  _otp_request_finished = threading.Event()
  _soap_async = True  # used by try_client_method
  
  # constants representing results of adding a message into the database
  ADD_MESSAGE_RESULT_SKIPPED = 0  # new data was not used
  ADD_MESSAGE_RESULT_REPLACED = 1  # old data was replaced by similar quality data
  ADD_MESSAGE_RESULT_UPGRADE = 2  # old data was replaced from a more complete source
  ADD_MESSAGE_RESULT_DOWNGRADE = 3  # old data was replaced from a less complete
                                    # source because some data was missing
  ADD_MESSAGE_RESULT_NEW = 4  # this is a new message
  
  # constants for subaccount types
  SELECT_TYPE_ALL = 0
  SELECT_TYPE_RECENT = 1
  
  MESSAGE_TYPE_RECEIVED = DsdbMessageStore.MESSAGE_TYPE_RECEIVED
  MESSAGE_TYPE_SENT = DsdbMessageStore.MESSAGE_TYPE_SENT
  MESSAGE_TYPE_NONE = 0
  
  CONFIG_FORMAT_VERSION = 1
  
  # some column numbers
  COLUMN_ID = 0
  COLUMN_ANNOT = 1 
  COLUMN_SENDER = 2
  COLUMN_RECIPIENT = 3
  COLUMN_DELIVERY_TIME = 4
  COLUMN_ACCEPT_TIME = 5
  COLUMN_STATUS_STR = 6
  COLUMN_STATUS = 7
  COLUMN_READ = 8
  COLUMN_VERIFIED = 9
  COLUMN_SIGNED = 10
  COLUMN_VER_ATTEMPT = 11
  COLUMN_RECEIVED = 12
  COLUMN_SEND = 13
  COLUMN_DEL_TIME_SORT = 14
  COLUMN_ACC_TIME_SORT = 15
  COLUMN_SEARCH = 16
  COLUMN_ANNOT_SORT = 17
  COLUMN_SENDER_SORT = 18
  COLUMN_RECIPIENT_SORT = 19
  COLUMN_STATUS_SORT = 20
  COLUMN_READ_SORT = 21
  
  MESSAGE_STORE_DEF = [int, str, str, str, str, str, str, str, bool, bool, bool,
                       bool, bool, bool, str, str, bool, str, str, str, str, str]
  
  # account columns
  COLUMN_ACCOUNT_ID = 0
  COLUMN_ACCOUNT_TEST = 1
  COLUMN_ACCOUNT_TOOLTIP = 2
  COLUMN_ACCOUNT_NAME = 3
  COLUMN_ACCOUNT_SUB = 4
  COLUMN_ACCOUNT_UNREAD_COUNT = 5
  COLUMN_ACCOUNT_ARCH_YEAR = 6
  COLUMN_ACCOUNT_TOP_ORDER = 7  
  COLUMN_ACCOUNT_TOP_LEVEL = 8
  COLUMN_ACCOUNT_HAS_MESSAGES = 9
  COLUMN_ACCOUNT_MESSAGE_TYPE = 10
  COLUMN_ACCOUNT_SELECT_TYPE = 11
  
  # panes that have to be saved and restored
  # (id, portion_of_parent)
  PANES = (("hpaned1", 0.2),
           ("message_display_pane", 0.7),
           ("message_pane", 0.4),
           )
  
  def __init__(self, local_startup=True):
    self.https_proxy = -1
    self.http_proxy = -1
    self.accounts = []
    self.account = None
    self.client = None
    self._otp_value = None
    self._last_otp_problem = None
    self.toplevel_message_store = None
    if QUARTZ_ENABLE:
        self.macapp = QuartzApp.Application()
    # here we save last directories for different functions
    self._last_dirs = {}
    # this is a event for synchronization of async stuff
    self._otp_request_finished.set()
    # icon manager
    icon_dir = local.find_data_directory("icons")
    IconManager.read_directory(icon_dir)
    # build the GUI from xml
    builder = gtk.Builder()
    #builder.set_translation_domain("datovka")
    self.builder = builder
    # the following adds some custom icons
    self._add_custom_icons()
    # // end of custom icon support
    gui_xml = self.fix_ui_xml("datovka.ui")
    if gtk.gtk_version < (2,16,0):
      from ui_xml_versions import remove_incompatible_parts
      gui_xml_optimized = remove_incompatible_parts(gui_xml, gtk.gtk_version)
    else:
      gui_xml_optimized = gui_xml
    try:
      builder.add_from_string(gui_xml_optimized.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      builder.add_from_string(gui_xml_optimized.decode('utf-8'), -1)
    self.window1 = builder.get_object("window1")
    # icon dir was defined above when icon manager was initialized
    if icon_dir:
      try:
        self.window1.set_icon_from_file(os.path.join(icon_dir,"datovka.png"))
      except:
        pass
    self._post_builder_ui_compatibility_fixes(gui_xml)
    builder.connect_signals(self)
    # the following fixes links in about dialog not working under windows
    gtk.about_dialog_set_url_hook(self.on_about_dialog_website_hook)
    gtk.about_dialog_set_email_hook(self.on_about_dialog_email_hook)
    # the same for link buttons
    gtk.link_button_set_uri_hook(self.on_link_click_hook)
    # let's continue
    self.window1.show()
    GUI.progressbar = builder.get_object("main_progressbar")
    tag_table = builder.get_object("message_tagtable")
    self._connect_message_stores(builder.get_object("message_store"))
    # // end of filter setup
    self.received_message_textview = builder.get_object("received_message_textview")
    self.received_attachment_treeview = builder.get_object("received_attachment_treeview")
    self.received_attachment_store = builder.get_object("received_attachment_store")
    self.account_store = builder.get_object("account_store")
    self.account_tree_sort = builder.get_object("account_tree_sort") # sorted model
    self.account_treeview = builder.get_object("account_treeview")
    self.statusbar = builder.get_object("statusbar")
    self._last_statusbar_push = 0
    # for some reason, "message_tagtable" is associated only with the "sent" buffer,
    # not with "received" - I fix it here by hand
    # UPDATE - text tags don't work well with glade anyway - lets do it all by hand
    for tv_id in ("received_message_textview",
                  "account_info_textview"):
      tb = self.builder.get_object(tv_id).get_buffer()
      self._create_text_tags(tb)
    # some tweaks to permit nice display of read messages
    col = builder.get_object("received_message_read_column")
    cell = builder.get_object("received_message_read_cell_renderer")
    col.set_cell_data_func(cell, self._render_read_icon)
    # bind some special features to username rendering
    col = builder.get_object("account_treeview_username_column")
    cell = builder.get_object("account_treeview_username_cellrenderer")
    col.set_cell_data_func(cell, self._render_account_name)
    # bind text boldness to read locally status for received_attachment_treeview
    for colname in ("treeviewcolumn_id","treeviewcolumn_title","treeviewcolumn_sender",
                    "treeviewcolumn8","treeviewcolumn7"):
      col = builder.get_object(colname)
      renderer = col.get_cell_renderers()[0]
      col.set_cell_data_func(renderer, self._render_read_text)
    # check if python version works with HTTPS proxy
    python_version = tuple(sys.version_info[0:3])
    if python_version >= (2,6,3):
      self.proxy_support_ok = True
    else:
      self.proxy_support_ok = False
    if not self.proxy_support_ok:
      action = self.builder.get_object("change_proxy_settings")
      action.set_sensitive(False)
      try:
        # set_tooltip is not supported in older versions of PyGTK
        action.set_tooltip(_("Your version of Python does not support HTTPS proxy!"))
      except:
        pass
      self.https_proxy = None
      self.http_proxy = None
      self._statusbar_message(_("Your version of Python does not support HTTPS proxy!"))
    # activate tooltip display in statusbar for menu
    self._activate_tooltips_for_main_menu()
    # some manual callbacks
    self.received_message_treeview.get_selection().connect("changed",
                          self.on_received_message_treeview_selection_changed)
    self.sent_message_treeview.get_selection().connect("changed",
                          self.on_sent_message_treeview_selection_changed)
    self.account_treeview.get_selection().connect("changed",
                          self.on_account_treeview_selection_changed)
    self.received_attachment_treeview.get_selection().connect("changed",
                          self.on_received_attachment_treeview_selection_changed)
    # the following is used for saving window position and size
    self._position = None
    self._size = None
    self._app_id = None
    # Mac OS X Quartz interface
    if QUARTZ_ENABLE:
        menubar = self.builder.get_object("main_menu")
        menubar.hide()
        #quit_item = self.uimanager.get_widget("/MenuBar/FileMenu/Quit")
        #about_item = self.uimanager.get_widget("/MenuBar/HelpMenu/About")
        #prefs_item = self.uimanager.get_widget("/MenuBar/EditMenu/Preferences")
        self.macapp.set_menu_bar(menubar)
        #self.macapp.insert_app_menu_item(about_item, 0)
        #self.macapp.insert_app_menu_item(prefs_item, 1)
        self.macapp.ready()
        self.macapp.connect('NSApplicationWillTerminate',
                            self.on_window1_destroy)
    # if we run portable - we want to notify the user
    if self.portable:
      self.window1.set_title(self.window1.get_title() + " - " +
                             _("Portable version"))
      self.builder.get_object("portable_version_warning_label").show()
    if local_startup:
      self._local_startup()
    else:
      self.update_account_actions()
    import glib
    glib.timeout_add(2000, self.on_timeout)

    
  def _local_startup(self):
    """this is a helper function that separates some of the code that is run
    normally from the constructor, but might be helpful elsewhere sometimes"""
    # load a stored config file
    self._load_config_file()
    # open an account
    if not self.account:
      self.on_add_account_activate()
    self.update_account_actions()
    if self.account:
      self._check_empty_message_stores()
    self._check_new_versions()
    
  def _connect_message_stores(self, base_store, treeview=None):
    self.message_store = base_store
    # create filter for search function
    search_filter = self.message_store.filter_new()
    search_filter.set_visible_column(self.COLUMN_SEARCH)
    self.search_filtered_store = gtk.TreeModelSort(search_filter)
    # create filters for sent and received messages
    # received first
    # store the sorting method
    sort_col, sort_ord  = None, None
    if self.toplevel_message_store:
      sort_col, sort_ord = self.toplevel_message_store.get_sort_column_id()
    self.toplevel_message_store = gtk.TreeModelSort(self.search_filtered_store)
    if sort_col is not None:
      self.toplevel_message_store.set_sort_column_id(sort_col, int(sort_ord))
    # the following callbacks allow display of unread count
    self.toplevel_message_store.connect_after('row-changed', 
                                        self.on_toplevel_message_store_changed)
    self.toplevel_message_store.connect("rows_reordered",
                                      self.on_toplevel_message_store_reordered)
    # replace the model with the filter
    self.received_message_treeview = self.builder.get_object("received_message_treeview")
    self.sent_message_treeview = self.builder.get_object("sent_message_treeview")
    if treeview is self.received_message_treeview:
      self.received_message_treeview.set_model(self.toplevel_message_store)
      self.sent_message_treeview.set_model(None)
    elif treeview is self.sent_message_treeview:
      self.received_message_treeview.set_model(None)
      self.sent_message_treeview.set_model(self.toplevel_message_store)
    else:
      self.sent_message_treeview.set_model(None)
      self.received_message_treeview.set_model(None)
     
  # Callbacks
  
  def on_connect_action_activate(self, widget):
    def get_them_all(direction):
      dm_limit = 100
      dm_offset = 1
      soap_res, subcount, subcount_new = \
            self._sync_message_list(direction, limit=dm_limit, offset=dm_offset)
      count = subcount
      count_new = subcount_new 
      while subcount >= dm_limit and not soap_res.fatal_error(): 
        # there are potentially more messages, ask again with an offset
        dm_offset += dm_limit
        soap_res, subcount, subcount_new = \
            self._sync_message_list(direction, limit=dm_limit, offset=dm_offset)
        count += subcount
        count_new += subcount_new
      return count, count_new, soap_res
      
    # preparatory work
    # save scrollbar values
    scrolls_to_update = []
    for scrwin_name in ("sent_scrolledwindow","received_scrolledwindow"):
      scr_bar = self.builder.get_object(scrwin_name).get_vscrollbar()
      scr_adj = scr_bar.get_adjustment()
      if scr_bar.get_value() == scr_adj.get_lower():
        scrolls_to_update.append((scr_adj, 0))
      elif scr_bar.get_value() + scr_adj.get_page_size() == scr_adj.get_upper():
        scrolls_to_update.append((scr_adj, 1))
    # get received messages
    received_count, received_count_new, soap_res = get_them_all('received')
    if not soap_res.fatal_error():
      # get sent messages
      sent_count, sent_count_new, soap_res = get_them_all('sent')
      # report the number of messages in the fetched lists
      _message = _("Messages on the server:") + " "
      _message += ngettext("%d received", "%d received",
                           received_count)%received_count
      _message += " "
      _message += ngettext("(%d new)","(%d new)",
                           received_count_new)%received_count_new
      _message += "; "
      _message += ngettext("%d sent","%d sent",sent_count)%sent_count
      _message += " "        
      _message += ngettext("(%d new)","(%d new)",sent_count_new)%sent_count_new
      self._statusbar_message(_message)                
    self._check_empty_message_stores()
    if not soap_res.fatal_error():
      self._sync_account_and_password_info()
    # make sure the message views are updated properly
    message, typ = self._get_selected_message_and_type()
    if message:
      if typ == "sent":
        self.on_sent_message_treeview_selection_changed(None)
      elif typ == "received":
        self.on_received_message_treeview_selection_changed(None)
      self.activate_message_actions(message)
    # update the unread counts
    self._update_account_unread_from_db()
    # update scrollbar if needed
    if scrolls_to_update:
      while gtk.events_pending():
        gtk.main_iteration(block=False)        
      for scr_adj, value in scrolls_to_update: 
        if value == 0: 
          scr_adj.set_value(0.0)
        elif value == 1:
          scr_adj.set_value(scr_adj.get_upper() - scr_adj.get_page_size())


  def _sync_message_list(self, message_type, limit=1000, offset=0):
    """
    internal method to fetch a list defined by message_type (sent or received),
    add it to the database and return a tuple of (soap_reply, count, new_count)
    """
    # choose the parameters according to message_type
    if message_type == 'received':
      message_list_method = "GetListOfReceivedMessages"
      single_message_method = "SignedMessageDownload"
      error_message = _("It was not possible to download the list of received "
                        "messages.")
    else:
      message_list_method = "GetListOfSentMessages"
      single_message_method = "SignedSentMessageDownload"
      error_message=_("It was not possible to download the list of sent "
                      "messages.")
    # run the method and process the result
    count = 0
    count_new = 0
    # get received messages
    soap_res = self.try_client_method(message_list_method, (),
                                      error_message=error_message,
                                      kwargs=dict(dmLimit=limit,
                                                  dmOffset=offset))
    if not soap_res.fatal_error():
      if not soap_res.soap_error() and soap_res.has_reply_data():
        reply = soap_res.reply
        for m in reply.data:
          if ConfigManager.AUTO_DOWNLOAD_WHOLE_MESSAGES:
            # automatically download the message
            if self.account.messages.has_message(m.dmID):
              stored_message = self.account.messages.get_message_by_id(m.dmID)
              if stored_message.is_complete() and \
                (m.dmMessageStatus == stored_message.dmMessageStatus or \
                 (m.dmMessageStatus == 10 and \
                  stored_message.dmMessageStatus in (5,6,7))) and \
                self.account.messages.has_raw_data(m.dmID) and \
                self.account.messages.has_raw_delivery_info_data(m.dmID): 
                # we already have this message in full form
                # and raw_data is also present
                count += 1
                continue
            complete = self._try_download_complete_message(
                                    single_message_method, m, message_type,
                                    ignore_errors=True)
            if not complete:
              # download was not successful - use the envelope instead
              result = self._add_message(m, message_type)
              if result == GUI.ADD_MESSAGE_RESULT_NEW:
                count_new += 1
          else:
            result = self._add_message(m, message_type)
            # see if message was downgraded and attempt complete redownload
            # if yes, so that the user does not get confused by disappearing
            # attachments
            if result == GUI.ADD_MESSAGE_RESULT_DOWNGRADE:
              soap_res = self.try_client_method(single_message_method,
                                                 (m.dmID,), ignore_errors=True)
              if not soap_res.fatal_error() and soap_res.has_reply_data():
                reply = soap_res.reply
                self._add_message(reply.data, message_type,
                                  additional_data=reply.additional_data)
            elif result == GUI.ADD_MESSAGE_RESULT_NEW:
              count_new += 1
          count += 1
    return (soap_res, count, count_new)

          
  def _sync_account_and_password_info(self):
    """
    Fetch info about the active account and update it accordingly
    """
    # check info about the account
    soap_res = self.try_client_method(
      "GetOwnerInfoFromLogin", (),
      error_message=_("It was not possible to download info about the account."))
    if not soap_res.fatal_error() and soap_res.has_reply_data():
      reply = soap_res.reply
      info = reply.data
      self.set_account_info(info)
      check_exp_pass = False # should we check expiring password
      if self.account.credentials.login_method in ("username",
                                                   "user_certificate",
                                                   "hotp","totp"):
        # check info about password expiration
        # - available only for password logins
        soap_res = self.try_client_method(
          "GetPasswordInfo", (),
          error_message=_("It was not possible to download info about password "
                          "expiration."))
        if not soap_res.fatal_error() and not soap_res.soap_error():
          reply = soap_res.reply
          if self.account.password_expiration_date != reply.data:
            check_exp_pass = True
            self.account.password_expiration_date = reply.data
        if check_exp_pass:
          self.check_expired_password(self.account)
        self._show_account_info()
    
          
  def on_sync_all_accounts_activate(self, widget):
    self.window1.set_sensitive(False)
    try:
      # the iterator would be invalidated during update, so we need to make
      # an external copy of the account list
      accounts = [(row[self.COLUMN_ACCOUNT_ID], row[self.COLUMN_ACCOUNT_TEST])
                  for row in self.account_tree_sort]
      for acc_id, acc_test in accounts:
        for row in self.account_tree_sort:
          if row[self.COLUMN_ACCOUNT_ID] == acc_id and \
            row[self.COLUMN_ACCOUNT_TEST] == acc_test:
            self.account_treeview.get_selection().select_iter(row.iter)
            if self.account.sync_with_all:
              self._statusbar_message(_("Syncing account %s") % \
                                      row[self.COLUMN_ACCOUNT_NAME])
              self.on_connect_action_activate(None)
            break
    finally:
      self.window1.set_sensitive(True)
  

  def on_about_activate(self, widget):
    dial = self.builder.get_object("aboutdialog1")
    dial.set_version(release.DSGUI_VERSION)
    dial.run()
    dial.hide()

 
  def on_about_dialog_website_hook(self, dialog, url):
    webbrowser.open(url)


  def on_about_dialog_email_hook(self, dialog, mail):
    webbrowser.open("mailto://"+mail)


  def on_link_click_hook(self, button, url):
    webbrowser.open(url)


  def on_received_message_treeview_selection_changed(self, treeselection):
    if treeselection and \
      self._get_active_message_treeview() != treeselection.get_tree_view():
      # this change is not for the active treeview - do not react to it
      return
    message = self._get_selected_received_message()
    if message:
      if self.account:
        self.account.last_message_id = message.dmID
      self.show_received_message(message)
      self.activate_message_actions(message)
      # scroll to selection - it can disappear when sorting by read locally status
      # and reading something
      model, paths = self.received_message_treeview.get_selection().get_selected_rows()
      if len(paths) == 1:
        self.received_message_treeview.scroll_to_cell(paths[0])
    else:
      self.activate_message_actions(None)
      self.received_message_textview.get_buffer().set_text("")
      self.received_attachment_store.clear()
    self.on_received_attachment_treeview_selection_changed(None)


  def on_sent_message_treeview_selection_changed(self, treeselection):
    if treeselection and \
      self._get_active_message_treeview() != treeselection.get_tree_view():
      # this change is not for the active treeview - do not react to it
      return
    message = self._get_selected_sent_message()
    if message:
      self.show_sent_message(message)
      self.activate_message_actions(message)
      # scroll to selection - it can disappear when sorting by read locally status
      # and reading something
      model, paths = self.sent_message_treeview.get_selection().get_selected_rows()
      if len(paths) == 1:
        self.sent_message_treeview.scroll_to_cell(paths[0])
    else:
      self.activate_message_actions(None)
      self.received_message_textview.get_buffer().set_text("")
      self.received_attachment_store.clear() 
    self.on_received_attachment_treeview_selection_changed(None)


  def on_window1_destroy(self, w):
    self._save_state()
    self.destroy_current_ds_client()
    for account in self.accounts:
      if account.credentials and account.credentials.should_logout():
        cl = self._create_ds_client(account.credentials)
        try:
          cl.logout_from_server()
        except:
          pass # we ignore any possible errors
    # set back the original except hook - this might help solving
    # mysterious error showing up on exit on windows
    sys.excepthook = sys.__excepthook__
    gtk.main_quit()
    
  def on_window1_delete_event(self, w, v):
    self._position = w.get_position()
    self._size = w.get_size()

  def on_download_complete_message_activate(self, w):
    pass
    #self._try_download_complete_message("MessageDownload")
      
  def on_download_complete_message_signed_activate(self, w, textview=None):
    message, typ = self._get_selected_message_and_type()
    if message:
      if typ == "received":
        method = "SignedMessageDownload"
      elif typ == "sent":
        method = "SignedSentMessageDownload"
      new_message = self._try_download_complete_message(method, message, typ)
      # add the message into the database
      if new_message:
        if typ == "received":
          self.show_received_message(new_message, textview=textview)
        else:
          self.show_sent_message(new_message, textview=textview)
        self.on_received_attachment_treeview_selection_changed(None)
        self.activate_message_actions(new_message)
      else:
        self.on_received_attachment_treeview_selection_changed(None)
        self.activate_message_actions(message)
      
      
  def _try_download_complete_message(self, method, message, message_type,
                                     ignore_errors=False):
    """tries to download a complete message and add it into the database;
    ignore_errors makes it possible for the method to silently ignore errors
    """
    result = self.try_client_method(
      method,
      (message.dmID,),
      handle_status=not ignore_errors,
      ignore_errors=ignore_errors,
      error_message=_("It was not possible to download the message."),   
      )
    if not result.fatal_error():
      if not result.soap_error() and result.has_reply_data():
        reply = result.reply
        message = reply.data
        # try to download delivery info
        result2 = self.try_client_method(
            "GetSignedDeliveryInfo",
            (message.dmID,),
            handle_status=not ignore_errors,
            ignore_errors=ignore_errors,
            error_message=_("It was not possible to download message delivery info."),   
            )
        reply2 = result2.reply
        if not result2.has_reply_data():
          add = {"delivery_info": None,
                 "delivery_info_raw_data": None}
        else:
          add = {"delivery_info": reply2.data,
                 "delivery_info_raw_data": reply2.additional_data['raw_data']}
        add.update(reply.additional_data)
        # sender ident
        result3 = self.try_client_method(
            "GetMessageAuthor",
            (message.dmID,),
            handle_status=not ignore_errors,
            ignore_errors=ignore_errors,
            error_message=_("It was not possible to download message author info."),   
            )
        if result3.has_reply_data():
          add['custom_data'] = {'message_author': result3.reply.data}
        # add the message into the database
        self._add_message(message, message_type, additional_data=add)
        if result2.has_reply_data():
          message.dmEvents = reply2.data.dmEvents
        
        return message
    else:
      return None

      
  def on_verify_sent_message_signature_activate(self, w, textview):
    message = self._get_selected_sent_message()
    result = self.try_client_method(
      "SignedSentMessageDownload",
      (message.dmID,),
      error_message=_("It was not possible to download the message."),
      )
    if not result.fatal_error() and result.has_reply_data():
      message = result.reply.data
      self._add_message(message, "sent")
      self.show_sent_message(message, textview=textview)
    

  def on_received_attachment_treeview_row_activated(self, tree, path, column):
    self.builder.get_object("open_attachment").activate()

  def on_save_attachment_activate(self, w, message=None, attachment=None):
    if not message or not attachment:
      message, attachment = self._get_selected_attachment()
    if attachment:
      d = gtk.FileChooserDialog(title=_("Select where to save"),
                                parent=self.window1,
                                action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                         gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
      d.set_do_overwrite_confirmation(True)
      d.set_current_folder(self._get_topical_last_dir())
      d.set_current_name(os_support.sanitize_file_name(attachment._dmFileDescr))
      ok = d.run()
      if ok == gtk.RESPONSE_ACCEPT:
        fullname = d.get_filename()
        self._set_topical_last_dir(os.path.dirname(fullname))
        fullname = fullname.decode('utf-8') # it seems to default to this
        dir, name = os.path.split(fullname)
        attachment.save_file(dir, name)
      else:
        pass
      d.destroy()

  def on_open_attachment_activate(self, w, message=None, attachment=None):
    if not message or not attachment:
      message, attachment = self._get_selected_attachment()
    content = attachment.get_decoded_content()
    fname = os_support.sanitize_file_name(attachment._dmFileDescr)
    done = os_support.open_file_content_in_associated_app(fname, content)
    if not done:
      message = _("I could not find an application to open this attachment. \
You can save it and open it manually.")
      self._show_error(message)
      

  def on_edit_credentials_activate(self, w):
    if self.account:
      cred = self.account.credentials
      name = self.account.name
      sync_with_all = self.account.sync_with_all
    else:
      cred = None
      name = None
      sync_with_all = True
    d = CredentialsDialog(self, name=name, credentials=cred,
                          sync_with_all=sync_with_all)
    result = d.run()
    if result == 1:
      data = d.get_data()
      del data['username'] # the change_data method does not use this value
      self.account.name = data['name']
      self.account.sync_with_all = data.get('sync_with_all', True)
      del data['name'] # the change_data method does not use this value
      del data['sync_with_all']
      test_account_old_value = self.account.credentials.test_account
      change = self.account.credentials.change_data(**data)
      if change:
        # some change requiring SOAP client update took place 
        self.destroy_current_ds_client()
        self._save_state()  # save the config file
      # test_account was changed, we need to reflect it in the account_store
      for row in self.account_store:
        if row[self.COLUMN_ACCOUNT_ID]==self.account.credentials.username and \
           row[self.COLUMN_ACCOUNT_TEST]==test_account_old_value:
          row[self.COLUMN_ACCOUNT_TEST] = self.account.credentials.test_account
          row[self.COLUMN_ACCOUNT_NAME] = self.account.name
          for child in row.iterchildren():
            child[self.COLUMN_ACCOUNT_TEST] = self.account.credentials.test_account
          #row[4] = 0
          #break
    else:
      pass # cancel or something like this was pressed
    d.destroy()
    return result


  def on_find_data_box_activate(self, w):
    # for searching we need info about the account - obtain it when it is not
    # available already
    if not self.account.account_info:
      self._sync_account_and_password_info()
    # run the search dialog
    d = DsSearchDialog(self, trans_window=self.window1)
    result = d.run()
    d.hide()
    if result == 1:
      boxes = d.get_selected_box_names()
      m = "\n".join(["%s: %s"%(box[0],box[1]) for box in boxes])
      _message = ngettext("You selected the following box:",
                          "You selected the following boxes:", len(boxes))
      _message += "\n\n"+m
      d = gtk.MessageDialog(self.window1,
                            message_format=_message,
                            buttons=gtk.BUTTONS_OK,
                            type=gtk.MESSAGE_INFO)
      d.run()
      d.destroy()
    
  def on_send_message_activate(self, w):
    self._send_message()
    
  def _send_message(self, reply_to=None):
    # preparatory work
    # save scrollbar value - we will maybe need to move it later on
    scr_bar = self.builder.get_object("sent_scrolledwindow").get_vscrollbar()
    scr_adj = scr_bar.get_adjustment()
    scroll_to = None
    if scr_bar.get_value() == scr_adj.get_lower():
      scroll_to = 0
    elif scr_bar.get_value() + scr_adj.get_page_size() == scr_adj.get_upper():
      scroll_to = 1
    # for searching we need info about the account - obtain it when it is not
    # available already
    if not self.account.account_info:
      self._sync_account_and_password_info()
    if not self.account.account_info: # we weren't able to sync account, give it up
      return
    # the send dialog itself
    dial = DsSendDialog(self, reply_to=reply_to)
    DsSendDialog._last_dir = self._get_topical_last_dir("ds_send")
    ok = None
    new = True
    while not ok:
      if not new:
        dial.show_all()
      new = False
      result = dial.run()
      dial.hide()
      # update data about the last used directory
      self._set_topical_last_dir(DsSendDialog._last_dir, name="ds_send")
      if result == 1:
        # dummy operation at first to get a cookie
        result = self.try_client_method("DummyOperation", (),
                                        handle_status=False)
        if not result.fatal_error():
          # now send the file
          message_texts = []
          error_message = _("It was not possible to send the message to <i>%s</i>.")
          local_ok = None
          prepared_messages = dial.gen_prepared_messages()
          while True:
            # try to prepare next message
            try:
              rec_name, rec_id, envelope, dmfiles  = prepared_messages.next()
            except IOError as exc:
              local_ok = False
              message_texts.append(_("An error occurred when preparing message."))
              message_texts.append("  - "+unicode(exc))
              print exc
              break
            except StopIteration:
              break
            except Exception as exc:
              local_ok = False
              message_texts.append(_("An error occurred when preparing message."))
              message_texts.append("  - "+unicode(exc))
              break
            # try to send the message
            recipient = "%s (%s)" % (rec_name, rec_id)
            result = self.try_client_method("CreateMessage",
                                            (envelope, dmfiles),
                                             error_message=error_message%recipient,
                                             handle_status=False)
            if result.fatal_error():
              return
            if result.error and result.error_shown:
              # there was an error, but the user was presented with it already
              message_texts.append(
                _("An error occurred when sending the message to <i>%s</i>.")%recipient)
              message_texts.append("  - "+result.error.status_message)
              local_ok = False
            else:
              reply = result.reply
              # add the id returned by the server
              new_mess = self._envelope_to_sent_message(reply.data,
                                                        envelope,
                                                        dmfiles)
              self._add_message(new_mess, "sent")
              self._check_empty_message_stores()
              message_texts.append(
                _("Message was successfully sent to <i>%s</i>.")%recipient)
              if local_ok == None:
                local_ok = True
          # move scrollbar if needed
          if scroll_to != None:
            while gtk.events_pending():
              gtk.main_iteration(block=False)        
            if scroll_to == 0: 
              scr_adj.set_value(0.0)
            elif scroll_to == 1:
              scr_adj.set_value(scr_adj.get_upper() - scr_adj.get_page_size())
          # report the result
          if local_ok:
            mtype = gtk.MESSAGE_INFO
            message_text = "<b>%s</b>"%\
                _("Message was successfully sent to all recipients.")
          else:
            mtype = gtk.MESSAGE_ERROR
            message_text = "<b>%s</b>"%\
                _("It was not possible to send the message to all recipients.")
          
          message_text += "\n\n"
          message_text += "\n".join(message_texts)
          d = gtk.MessageDialog(self.window1,
                                buttons=gtk.BUTTONS_OK,
                                type=mtype)
          d.set_markup(message_text)
          d.run()
          d.destroy()
          ok = local_ok
      else:
        break

  def _envelope_to_sent_message(self, dmID, envelope, files):
    """takes an envelope used for sending a message and returns an instance
    of Message suitable for storage"""
    from dslib import models
    m = models.Message()
    m.dmID = dmID
    for attr in envelope.KNOWN_ATTRS:
      val = getattr(envelope, attr)
      if type(val) == str:
        val = val.decode('utf-8')
      setattr(m, attr, val)
    m.dmMessageStatus = 1
    m.dmFiles = files
    m.dmRecipient = envelope.dmRecipient.decode('utf-8')
    if self.account.account_info:
      ai = self.account.account_info
      m.dmSender = ai.firmName or (ai.pnFirstName + " " + ai.pnLastName)
      #m.dmSenderAddress = ai.dmSenderAddress
    return m

  def on_received_message_treeview_row_activated(self, tree, path, column):
    read_col = self.builder.get_object("received_message_read_column")
    if column == read_col:
      model = tree.get_model() 
      iter = model.get_iter(path) 
      msgid = model.get_value(iter, self.COLUMN_ID)
      read = self.account.messages.\
                    get_message_supplementary_data(msgid,'read_locally')
      read = not read
      self.account.messages.\
            set_message_supplementary_data(msgid,'read_locally',read)
      for row in self.message_store:
        if row[0] == msgid:
          row[8] = read
          break
    else:
      # open the message in a new window
      message = self._get_selected_received_message()
      dial = DsMessageDialog(self, message, show_attachments=True)
      dial.show_all()
      dial.run()
      dial.destroy()
      self.received_message_treeview.emit("cursor-changed")

  def on_sent_message_treeview_row_activated(self, tree, path, column):
    # open the message in a new window
    message = self._get_selected_sent_message()
    dial = DsMessageDialog(self, message, show_attachments=True)
    dial.show_all()
    dial.run()
    dial.destroy()
    self.sent_message_treeview.emit("cursor-changed")

  def _something_with_attachment_happened(self):
    message, attachment = self._get_selected_attachment()
    if attachment:
      self.builder.get_object("save_attachment").set_sensitive(True)
      self.builder.get_object("open_attachment").set_sensitive(True)
    else:
      self.builder.get_object("save_attachment").set_sensitive(False)
      self.builder.get_object("open_attachment").set_sensitive(False)
    
      
  def on_received_attachment_store_row_number_changed(self, model, path, iter=None):
    self._something_with_attachment_happened()

  def on_received_attachment_treeview_selection_changed(self, treeselection):
    self._something_with_attachment_happened()

  def on_account_treeview_selection_changed(self, treeselection):
    account_row = self._get_selected_account_row(writable=True)
    if not account_row:
      self.builder.get_object("message_pane").hide()
      self.builder.get_object("account_info_textview").hide()
      self.update_account_actions()
      self.activate_message_actions(None)
      return
    account = self._get_account_object_for_account_row(account_row)
    message_treeview = self._get_active_message_treeview()
    if account:
      if account != self.account:
        if message_treeview:
          message_treeview.get_selection().unselect_all()
        self.open_account(account)
      if account_row[self.COLUMN_ACCOUNT_TOP_LEVEL]:
        # we do not show messages here
        self._show_account_info()
        self.builder.get_object("message_pane").hide()
        self.builder.get_object("account_info_textview").show()
        self.activate_message_actions(None)
      elif account_row[self.COLUMN_ACCOUNT_HAS_MESSAGES]:
        self.builder.get_object("message_pane").show()
        self.builder.get_object("account_info_textview").hide()
        if message_treeview:
          message_treeview.get_selection().unselect_all()
        self.message_store.clear()
        self._activate_treeview(account_row)
        message_type = self._get_active_message_type()
        select_type = account_row[self.COLUMN_ACCOUNT_SELECT_TYPE]
        arch_year = account_row[self.COLUMN_ACCOUNT_ARCH_YEAR]
        self.account_treeview.set_sensitive(False)
        if message_type == 'sent':
          m_type = DsdbMessageStore.MESSAGE_TYPE_SENT
        else:
          m_type = DsdbMessageStore.MESSAGE_TYPE_RECEIVED
        self.load_messages_for_current_account(m_type, select_type,
                                               year=arch_year)
        if message_type == 'received':
          # this is here to sync unread between sub-accounts which share
          # the same massages (recent + recent year)
          self._update_account_unread_from_message_model(
                                      self.toplevel_message_store, account_row)
        self.account_treeview.set_sensitive(True)
      else:
        self._show_sub_account_info()
        self.builder.get_object("message_pane").hide()
        self.builder.get_object("account_info_textview").show()
        self.activate_message_actions(None)
      self.update_account_actions()
    else:
      raise Exception("nonexistent account was selected - this should not happen")
    
  def _activate_treeview(self, account_row):
    if account_row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == self.MESSAGE_TYPE_SENT:
      self.builder.get_object("sent_scrolledwindow").show()
      self.builder.get_object("received_scrolledwindow").hide()
      # disconnect and reconnect models - prevents unnecessary updates
      self.received_message_treeview.set_model(None)
      self.sent_message_treeview.set_model(self.toplevel_message_store)
    else:
      self.builder.get_object("sent_scrolledwindow").hide()
      self.builder.get_object("received_scrolledwindow").show()
      # disconnect and reconnect models - prevents unnecessary updates
      self.sent_message_treeview.set_model(None)
      self.received_message_treeview.set_model(self.toplevel_message_store)
      
  def on_add_account_activate(self, w=None):
    d = CredentialsDialog(self, name=None, credentials=None)
    result = d.run()
    if result == 1:
      data = d.get_data()
      row_id = self.add_account(**data)
      if row_id >= 0:
        self.account_treeview.set_cursor(row_id)
        self.account_treeview.expand_row(row_id, False)
      else:
        message = _("%(account_type)s '%(username)s' is already present, you cannot add it twice!")
        account_type = self._get_account_type_as_text(data.get('test_account',
                                                               False))
        message = message % {"username": data['username'],
                             "account_type": account_type}
        dial = gtk.MessageDialog(self.window1,
                                 message_format=message,
                                 buttons=gtk.BUTTONS_OK,
                                 type=gtk.MESSAGE_ERROR)
        dial.run()
        dial.destroy()
      self.update_account_actions()
      self._save_state()
    else:
      # cancel or something like this was pressed
      self.update_account_actions()
    d.destroy()

  def on_account_treeview_key_release_event(self, w, event):
    if event.keyval == gtk.keysyms.Delete:
      account = self._get_selected_account_row()
      if account:
        self.builder.get_object("remove_account").activate()
        
  def on_message_treeview_key_release_event(self, w, event):
    if event.keyval == gtk.keysyms.Delete:
      self.builder.get_object("delete_message").activate()

  def on_show_help_index_activate(self, w=None):
    doc_dir = local.find_data_directory("doc")
    if doc_dir:
        path = os.path.join(doc_dir, 'index.html')
        if os.path.exists(path):
            os_support.open_file_in_associated_app(path)
            path = path.replace("\\","/")
            url = "file://"
            if not path.startswith("/"):
              url += "/"
            url += path
            d = gtk.MessageDialog(self.window1,
                                  message_format=_("Help index should open in\
 your browser.\nIf it does not, you can use the following URL:\n%s")%url,
                                  buttons=gtk.BUTTONS_OK,
                                  type=gtk.MESSAGE_INFO)
            d.run()
            d.destroy()
            return
    # this is fallback option if the dir or file could not be found
    d = gtk.MessageDialog(self.window1,
                          message_format=
                                    _("The help file could not be found."),
                          buttons=gtk.BUTTONS_OK,
                          type=gtk.MESSAGE_ERROR)
    d.run()
    d.destroy()
    
  def on_change_proxy_settings_activate(self, w):
    dial = ProxyDialog(self)
    result = dial.run()
    if result == 1:
      self.http_proxy = dial.get_proxy_setting('http')
      self.https_proxy = dial.get_proxy_setting('https')
      dial.update_dslib_proxy_settings()
      self.destroy_current_ds_client()
      self._save_state()
    dial.destroy()

  def on_change_password_activate(self, w):
    self._change_account_password()
    
    
  def _change_account_password(self):
    if self.account.credentials.login_method in ('hotp','totp'):
      # display info about developers of the DS not being able to
      # provide clear way to do this
      msg = _("""Your are using one time password authentication. \
Unfortunately, according to the official statement from developers of ISDS, \
there is no set procedure to do this from an external application yet.\n
<b>Please use the web interface to change your password.</b>\n
We apologize for the inconvenience.""")
      d = gtk.MessageDialog(self.window1,
                            buttons=gtk.BUTTONS_OK,
                            type=gtk.MESSAGE_INFO)
      d.set_markup(msg)
      d.run()
      d.destroy()
      return
    if not self.account.account_info or not self.account.account_info.dbID:
      # we need id of the box - we have to ask for it
      result = self.try_client_method(
        "GetOwnerInfoFromLogin",
        (),
        error_message=_("It was not possible to download the ID of you box; \
without it, it is not possible to change the password to that box."))
      if not result.fatal_error() and result.has_reply_data():
        info = result.reply.data
        self.set_account_info(info)
      else:
        return
    # here login without OTP is processed
    # download password info, just to be sure we are logged onto the server
    soap_res = self.try_client_method(
            "GetPasswordInfo",
            (),
            error_message=_("It was not possible to download info about password expiration."))
    if soap_res.fatal_error():
      if soap_res.canceled:
        self._statusbar_message(_("Action was canceled."))
      return
    from change_password import DsChangePasswordDialog
    d = DsChangePasswordDialog(self, self.account.account_info.dbID)
    new = True
    go_on = True
    while go_on:
      if not new:
        d.show_all()
      new = False
      if self.account.credentials.login_method == "hotp":
        d.set_requires_otp(True)
      result = d.run()
      d.hide()
      if result == 1:
        old, new = d.get_passwords()
        error_message=_("It was not possible to change the password.")
        if self.account.credentials.login_method == "hotp":
          error_message += "\n"
          error_message += _("NOTE: Wrong one time code would show as wrong \
old password error.")
        soap_res = self.try_client_method(
          "ChangeISDSPassword", (old, new),
          error_message=error_message
        )
        if not soap_res.fatal_error():
          d.destroy()
          go_on = False
          self.account.credentials.change_password(new)
          self._save_state()
          self.destroy_current_ds_client()
          subd = gtk.MessageDialog(self.window1,
                                   message_format=_("Password was successfully changed."),
                                   buttons=gtk.BUTTONS_OK,
                                   type=gtk.MESSAGE_INFO)
          subd.run()
          subd.destroy()
          # update info about password expiration
          soap_res = self.try_client_method(
             "GetPasswordInfo",
             (),
             error_message=_("It was not possible to download info about password expiration."))
          if not soap_res.fatal_error() and not soap_res.soap_error():
            self.account.password_expiration_date = soap_res.reply.data
        else:
          go_on = True
      else:
        go_on = False
  
  def on_remove_account_activate(self, w):
    row = self._get_selected_account_row(writable=True)
    if row:
      account = self._get_account_object_for_account_row(row)
      if account:
        d = gtk.MessageDialog(self.window1,
                              message_format=_("Remove account '%s'") % account.name,
                              buttons=gtk.BUTTONS_OK_CANCEL,
                              type=gtk.MESSAGE_QUESTION)
        answer = d.run()
        if answer == gtk.RESPONSE_OK:
          # close the account
          account.messages.close()
          self.accounts.remove(account)
          self.message_store.clear()
          # clean up the treeview
          treeview = self.account_treeview
          if row.parent:
            row = row.parent # remove parent row is a child row is selected
          self.account_store.remove(row.iter)
          # select the first account
          first = treeview.get_model().get_iter_first()
          if first:
            treeview.get_selection().select_iter(first)
          else:
            self.account = None
          # save the new state
          self._save_state()
        d.destroy()
    self.update_account_actions()


  def on_show_signature_details_activate(self, w, message=None):
    if not message:
      message, _ = self._get_selected_message_and_type()
    if message:
      from cert_display import CertDisplayDialog
      down_date = None
      if self.account:
        down_date = self.account.messages.\
                    get_message_supplementary_data(message.dmID, "download_date")
      if not down_date:
        # download date is not available - use current date
        down_date = datetime.datetime.now()
      d = CertDisplayDialog(self,
                            message,
                            download_date=down_date)
      d.run()
      d.destroy()

  def on_reply_to_message_activate(self, w):
    #box.dbID,box.firmName,box.adCity
    message, type = self._get_selected_message_and_type()
    if type == 'received':
      self._send_message(reply_to=message)
    else:
      self._show_error(_("Cannot reply to sent message"))

  def on_edit_preferences_activate(self, w):
    import configuration
    d = configuration.ConfigDialog(self)
    res = d.run()
    d.destroy()
    if "STORE_MESSAGES_ON_DISK" in d.changed:
      message = _("Method of message storage was changed, \
this change will not take effect until the application is restarted.")
      d = gtk.MessageDialog(self.window1,
                            message_format=message,
                            buttons=gtk.BUTTONS_OK,
                            type=gtk.MESSAGE_INFO)
      d.run()
      d.destroy()
    if "CHECK_CRL" in d.changed:
      Properties.CHECK_CRL = ConfigManager.CHECK_CRL
      # update the signature data in model
      self.window1.set_sensitive(False)
      count = len(self.message_store)
      GUI.progressbar.set_text(_("Updating view"))
      for i,row in enumerate(self.message_store):
        if not ConfigManager.CHECK_CRL and row[10] is True:
          # we switched CRL check off and it was OK before,
          # so it must be ok still
          continue 
        msgid = row[0]
        message = self.account.messages.get_message_by_id(msgid)
        if message:
          typ = row[12] and 'received' or 'sent'
          #[msgid,annot,sender,recipient,delivery_time,accept_time,status_str,
          #    status,read,ver,sig,attempt,received,sent]
          new_row = self._message_to_row(message, typ)
          row[10] = new_row[10]
        GUI.progressbar.set_fraction(1.0*i/count)
        while gtk.events_pending():
          gtk.main_iteration(block=False)
      GUI.progressbar.set_text(_("Idle"))
      GUI.progressbar.set_fraction(0.0)
      # update shown message
      message, type = self._get_selected_message_and_type()
      if type == 'received':
        self.show_received_message(message)
      else:
        self.show_sent_message(message)
      # make main window sensitive again 
      self.window1.set_sensitive(True)
    # if anything has changed
    if d.changed:
      self._save_state()

  def on_authenticate_message_activate(self, w):
    message, message_type = self._get_selected_message_and_type()
    if self.account.messages.has_raw_data(message.dmID):
      raw_data = self.account.messages.get_raw_data(message.dmID)
      # dummy operation at first to get a cookie
      soap_res = self.try_client_method("DummyOperation", (),
                                         handle_status=False)
      if not soap_res.fatal_error() and not soap_res.soap_error():
        soap_res = self.try_client_method("AuthenticateMessage", (raw_data.data,))
        if not soap_res.fatal_error() and soap_res.has_reply_data():
          reply = soap_res.reply
          if reply.data == True:
            output = _(u"Message was <b>successfully verified</b> against data \
on the server.\n\nThis message has passed through the system of Datové schránky \
and has not been tampered with since.")
            typ = gtk.MESSAGE_INFO
            # update the message from the authenticated raw data
            new_message = self.account.messages.\
                            get_message_from_raw_data(message.dmID,
                                                      self.client)
            adata = {"raw_data":raw_data.data}
            if self.account.messages.has_raw_delivery_info_data(message.dmID):
              new_di = self.account.messages.\
                            get_delivery_info_from_raw_data(message.dmID,
                                                            self.client)
              adata['delivery_info'] = new_di
              adata['delivery_info_raw_data'] = self.account.messages.\
                                      get_raw_delivery_info_data(message.dmID).data
            self._add_message(new_message, message_type,
                              additional_data=adata)
            if message_type == "sent":
              self.on_sent_message_treeview_selection_changed(None)
            else:
              self.on_received_message_treeview_selection_changed(None)
          else:
            output = _("Message was <b>not successfully verified</b>!\n\n\
The data in your database have probably been tampered with.")
            typ = gtk.MESSAGE_WARNING
          d = gtk.MessageDialog(self.window1,
                                buttons=gtk.BUTTONS_OK,
                                type=typ)
          d.set_markup(output)
          d.run()
          d.destroy() 
      else:
        self._show_error("It was not possible to establish communication \
with the server.")
    else:
      self._show_error(_("Sorry but the data required for verification of \
this message is not present."))


  def on_authenticate_file_activate(self, w):
    dialog = gtk.FileChooserDialog(title=_("Select a ZFO file"),
                              parent=self.window1,
                              action=gtk.FILE_CHOOSER_ACTION_OPEN,
                              buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                       gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                              )
    dialog.set_current_folder(self._get_topical_last_dir())
    flt = gtk.FileFilter()
    flt.set_name(_("Data messages (*.zfo)"))
    flt.add_mime_type("application/x-extension-zfo")
    flt.add_pattern("*.zfo")
    dialog.add_filter(flt)
    flt = gtk.FileFilter()
    flt.set_name(_("All files"))
    flt.add_pattern("*")
    dialog.add_filter(flt)
    ok = dialog.run()
    infilename = dialog.get_filename()
    dialog.destroy()
    if ok == gtk.RESPONSE_ACCEPT:
      infilename = infilename.decode('utf-8')
      self._set_topical_last_dir(os.path.dirname(infilename))
      infile = file(infilename, 'rb')
      data = infile.read()
      infile.close()
      self._authenticate_message_and_show_result(data)
    return 0

  def _authenticate_message_and_show_result(self, data):
    typ = gtk.MESSAGE_WARNING
    if len(data):
      soap_res = self.try_client_method("DummyOperation", (),
                                         handle_status=False)
      if soap_res.fatal_error() or soap_res.soap_error():
        message = _("It was not possible to establish communication with the server.")
      else:
        soap_res = self.try_client_method("AuthenticateMessage",
                                          (base64.standard_b64encode(data),),
                                          handle_status=False)
        if not soap_res.fatal_error() and soap_res.has_reply_data() and \
           soap_res.reply.data == True:
          message = _(u"Message was <b>successfully verified</b> against data \
on the server.\n\nThis message has passed through the system of Datové schránky \
and has not been tampered with since.")
          typ = gtk.MESSAGE_INFO
        else:
          message = _(u"Message was <b>not</b> authenticated as processed by \
the system.\n\nIt is either not a valid ZFO file or it was modified since \
it was downloaded from Datové schránky.")
    else:
      message = _("File is empty.")
    d = gtk.MessageDialog(self.window1, buttons=gtk.BUTTONS_OK, type=typ)
    d.set_markup(message)
    d.run()
    d.destroy() 

  def on_export_zfo_activate(self, w):
    message, type = self._get_selected_message_and_type()
    if self.account.messages.has_raw_data(message.dmID):
      raw_data = self.account.messages.get_raw_data(message.dmID)
      d = gtk.FileChooserDialog(title=_("Select where to export ZFO"),
                                parent=self.window1,
                                action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                         gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
      d.set_do_overwrite_confirmation(True)
      if type == "received":
        d.set_current_name("DDZ_%s.zfo"%message.dmID)
      else:
        d.set_current_name("ODZ_%s.zfo"%message.dmID)
      d.set_current_folder(self._get_topical_last_dir())
      ok = d.run()
      if ok == gtk.RESPONSE_ACCEPT:
        fullname = d.get_filename()
        self._set_topical_last_dir(os.path.dirname(fullname))
        try:
          f = file(fullname.decode("utf-8"), "wb")
          f.write(base64.b64decode(raw_data.data))
          f.close()
        except IOError, e:
          self._show_error(_("An error occurred during saving of file '%s'") %
                           fullname, exception=e) 
        else:
          self._statusbar_message(_("The file '%s' was sucessfully exported.")%\
                                  fullname)
      d.destroy()
    else:
      self._show_error(_("Sorry but the data required for export of \
this message is not present."))    

  def on_open_message_zfo_externally_activate(self, w):
    message, type = self._get_selected_message_and_type()
    if self.account.messages.has_raw_data(message.dmID):
      raw_data = self.account.messages.get_raw_data(message.dmID)
      if type == "received":
        fname = "DDZ_%s.zfo" % message.dmID 
      else:
        fname = "ODZ_%s.zfo" % message.dmID
      done = os_support.open_file_content_in_associated_app(
                                                fname,
                                                base64.b64decode(raw_data.data))
      if not done:
        self._show_error(_("I could not find an application to open file."))
    else:
      self._show_error(_("Sorry but the data required for export of \
this message is not present.")) 
    

  def on_open_delivery_info_zfo_externally_activate(self, w):
    message, type = self._get_selected_message_and_type()
    if self.account.messages.has_raw_delivery_info_data(message.dmID):
      raw_data = self.account.messages.get_raw_delivery_info_data(message.dmID)
      done = os_support.open_file_content_in_associated_app(
                                                "DD_%s.zfo"%message.dmID,
                                                base64.b64decode(raw_data.data))
      if not done:
        self._show_error(_("I could not find an application to open file."))
    else:
      self._show_error(_("Sorry but the data required for export of \
delivery info of this message is not present.")) 
    

  def on_export_delivery_info_zfo_activate(self, w):
    message, type = self._get_selected_message_and_type()
    if self.account.messages.has_raw_delivery_info_data(message.dmID):
      raw_data = self.account.messages.get_raw_delivery_info_data(message.dmID)
      d = gtk.FileChooserDialog(title=_("Select where to export delivery info ZFO"),
                                parent=self.window1,
                                action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                         gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
      d.set_do_overwrite_confirmation(True)
      d.set_current_name("DD_%s.zfo"%message.dmID)
      d.set_current_folder(self._get_topical_last_dir(
                                          name="on_export_zfo_activate"))
      ok = d.run()
      if ok == gtk.RESPONSE_ACCEPT:
        fullname = d.get_filename()
        self._set_topical_last_dir(os.path.dirname(fullname),
                                   name="on_export_zfo_activate")
        try:
          f = file(fullname.decode("utf-8"), "wb")
          f.write(base64.b64decode(raw_data.data))
          f.close()
        except IOError, e:
          self._show_error(_("An error occurred during saving of file '%s'") %
                           fullname, exception=e) 
        else:
          self._statusbar_message(_("The file '%s' was sucessfully exported.")%\
                                  fullname)
      d.destroy()
    else:
      self._show_error(_("Sorry but the data required for export of \
this message is not present.")) 

  def on_show_message_from_file_activate(self, w, fname=None, authenticate=False):
    if not fname:
      dialog = gtk.FileChooserDialog(title=_("Select a ZFO file"),
                                parent=self.window1,
                                action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                         gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
      dialog.set_current_folder(self._get_topical_last_dir(
                                          name="on_export_zfo_activate"))
      flt = gtk.FileFilter()
      flt.set_name(_("Data messages (*.zfo)"))
      flt.add_mime_type("application/x-extension-zfo")
      flt.add_pattern("*.zfo")
      dialog.add_filter(flt)
      flt = gtk.FileFilter()
      flt.set_name(_("All files"))
      flt.add_pattern("*")
      dialog.add_filter(flt)
      # extra widget
      extra = gtk.CheckButton(_("Authenticate message before display"))
      if not self.account:
        extra.set_sensitive(False)
      dialog.set_extra_widget(extra)
      # run the dialog
      ok = dialog.run()
      if ok == gtk.RESPONSE_ACCEPT:
        infilename = dialog.get_filename().decode('utf-8')
        self._set_topical_last_dir(os.path.dirname(infilename),
                                   name="on_export_zfo_activate")
      else:
        infilename = None
      authenticate = extra.get_active()
      dialog.destroy()
    else:
      infilename = fname
    # process the file
    if infilename:
      if not os.path.exists(infilename):
        self._show_error(_("File '%s' does not exists!")%infilename)
        return 
      try:
        infile = file(infilename, 'rb')
      except Exception, e:
        self._show_error(_("Cannot open file '%s' for reading!")%infilename, e)
        return
      data = infile.read()
      infile.close()
      # authenticate
      if authenticate:
        self._authenticate_message_and_show_result(data)
      # create a client if needed
      dummy_client = False
      if not self.client:
        if self.account:
          cr = self.account.credentials
        else:
          cr = None
        self.client = self._create_ds_client(cr, dummy=True)
        dummy_client = True
      if not self.client:
        return None  # the client was not created
                     # the user did not supply password or something like that
      #get the message - try several methods
      for method, f in (("SignedMessageDownload","signature_to_message"),
                        ("GetSignedDeliveryInfo","signature_to_delivery_info")):
        try:
          func = getattr(self.client, f)
          message = func(base64.b64encode(data), method)
        except Exception, e:
          pass
        else:
          break
      else:
       # not method was successful
       text = _("""Neither message nor delivery info could be loaded from the file '%s'.

It is either not a supported format of ZFO file or it is corrupted.
(...or there is a bug in Datovka :)) 
""")
       d = gtk.MessageDialog(self.window1,
                            message_format=text % infilename,
                            buttons=gtk.BUTTONS_OK,
                            type=gtk.MESSAGE_ERROR)
       d.run()
       d.destroy()
       if dummy_client:
         self.client = None
       return
      # show the message
      show_attachments = True
      if not message.dmFiles:
        show_attachments = False
      dial = DsMessageDialog(self, message, show_attachments=show_attachments,
                             warn_about_old_messages=False)
      dial.dialog.set_title(_("Message from file '%s' - details") % infilename)
      dial.run()
      dial.destroy()
      if dummy_client:
        self.client = None
    return 0   

  def on_export_delivery_info_pdf_activate(self, w):
    from export import ReportLabExporter
    message, type = self._get_selected_message_and_type()
    if self.account.messages.has_raw_delivery_info_data(message.dmID):
      d = gtk.FileChooserDialog(title=_("Select where to export delivery info PDF"),
                                parent=self.window1,
                                action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                         gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
      d.set_do_overwrite_confirmation(True)
      d.set_current_name("DD_%s.pdf"%message.dmID)
      d.set_current_folder(self._get_topical_last_dir(
                                          name="on_export_zfo_activate"))
      # extra widget
      extra = gtk.CheckButton(_("Open the exported file in associated application"))
      d.set_extra_widget(extra)
      ok = d.run()
      if ok == gtk.RESPONSE_ACCEPT:
        fullname = d.get_filename()
        self._set_topical_last_dir(os.path.dirname(fullname),
                                   name="on_export_zfo_activate")
        fullname_enc = fullname.decode("utf-8") 
        try:
          ReportLabExporter.export_delivery_info(fullname_enc, message)
        except IOError, e:
          self._show_error(_("An error occurred during saving of file '%s'") %
                           fullname, exception=e) 
        else:
          self._statusbar_message(_("The file '%s' was sucessfully exported.")%\
                                  fullname)
          if extra.get_active():
            done = os_support.open_file_in_associated_app(fullname_enc)
            if not done:
              self._statusbar_message(
                      _("I could not find an application to open file."))
      d.destroy()

    else:
      self._show_error(_("Sorry but the data required for export of \
this message is not present.")) 
      
      
  def on_export_message_envelope_pdf_activate(self, w):
    from export import ReportLabExporter
    message, type = self._get_selected_message_and_type()
    if hasattr(message, 'dmFiles') and message.dmFiles:
      d = gtk.FileChooserDialog(title=_("Select where to export envelope PDF"),
                                parent=self.window1,
                                action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                         gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
      d.set_do_overwrite_confirmation(True)
      d.set_current_name("OZ_%s.pdf"%message.dmID)
      d.set_current_folder(self._get_topical_last_dir(
                                          name="on_export_zfo_activate"))
      extra = gtk.CheckButton(_("Open the exported file in associated application"))
      d.set_extra_widget(extra)
      ok = d.run()
      if ok == gtk.RESPONSE_ACCEPT:
        fullname = d.get_filename()
        self._set_topical_last_dir(os.path.dirname(fullname),
                                   name="on_export_zfo_activate")
        fullname_enc = fullname.decode("utf-8")
        try:
          ReportLabExporter.export_message_envelope(fullname_enc, message)
        except IOError, e:
          self._show_error(_("An error occurred during saving of file '%s'") %
                           fullname, exception=e) 
        else:
          self._statusbar_message(_("The file '%s' was sucessfully exported.")%\
                                  fullname)
          if extra.get_active():
            done = os_support.open_file_in_associated_app(fullname_enc)
            if not done:
              self._statusbar_message(
                      _("I could not find an application to open file."))
      d.destroy()
    else:
      self._show_error(_("Sorry but the data required for export of \
this message is not present.")) 
      

  def on_treeview_button_press_event(self, treeview, event):
    if event.button == 3:
      x = int(event.x)
      y = int(event.y)
      time = event.time
      pthinfo = treeview.get_path_at_pos(x, y)
      if pthinfo is not None:
        path, col, cellx, celly = pthinfo
        treeview.grab_focus()
        treeview.set_cursor(path, col, 0)
        if treeview is self.received_attachment_treeview:
          popup = self.builder.get_object("attachment_popup")
        elif treeview is self.received_message_treeview or \
             treeview is self.sent_message_treeview:
          popup = self.builder.get_object("message_popup")
        elif treeview is self.account_treeview:
          popup = self.builder.get_object("account_popup")
        popup.popup(None, None, None, event.button, time)
      return True

  def _export_message_zfo(self, message, type, dir):
    if self.account.messages.has_raw_data(message.dmID):
      raw_data = self.account.messages.get_raw_data(message.dmID)
      if type == "received":
        fname = "DDZ_%s.zfo"%message.dmID
      else:
        fname = "ODZ_%s.zfo"%message.dmID
      fullname = os.path.join(dir, fname)
      try:
        f = file(fullname.decode("utf-8"), "wb")
        f.write(base64.b64decode(raw_data.data))
        f.close()
        return True
      except IOError, e:
        return False
    return None
  
  def on_export_correspondence_overview_activate(self, w):
    self.export_correspondence_overview()
  
  def export_correspondence_overview(self):
    from correspondence_overview import CorrespondenceDialog
    d = CorrespondenceDialog(self)
    result = d.run()
    if result == 1:
      exp = d.get_exporter()
      format = d.format
      d.destroy()
      html = exp.generate_export()
      d = gtk.FileChooserDialog(title=_("Select where to save the overview"),
                                 parent=self.window1,
                                 action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                 buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                          gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
      d.set_do_overwrite_confirmation(True)
      filters = exp.get_file_filters()
      [d.add_filter(filter) for filter in filters]
      # extra widget
      extra = gtk.CheckButton(_("Use the same directory for output of ZFO files"))
      if not exp.export_zfo:
        extra.set_sensitive(False)
      d.set_extra_widget(extra)
      # default filename
      if exp.FILE_EXTENSIONS:
        ext = exp.FILE_EXTENSIONS[0].get('ext', "")
      else:
        ext = "."+format
      fname = _("Overview") + "-%s%s" % (exp.export_date.date(), ext)
      d.set_current_name(fname)
      d.set_current_folder(self._get_topical_last_dir())
      ok = d.run()
      if ok == gtk.RESPONSE_ACCEPT:
        fullname = d.get_filename().decode('utf-8')
        # select extension
        filter = d.get_filter()
        filter_idx = filters.index(filter)
        if filter_idx < len(exp.FILE_EXTENSIONS):
          ext = exp.FILE_EXTENSIONS[filter_idx].get('ext', "")
        else:
          ext = ""
        if not "." in fullname:
          fullname += ext
        d.destroy()
        self._set_topical_last_dir(os.path.dirname(fullname))
        # where to export zfo
        errors = []
        if exp.export_zfo:
          if extra.get_active():
            zfo_dir = os.path.dirname(fullname)
          else:
            # we have to ask
            d2 = gtk.FileChooserDialog(title=_("Select where to save ZFO files"),
                                 parent=self.window1,
                                 action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                 buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                          gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                                )
            d2.set_current_folder(os.path.dirname(fullname))
            ok2 = d2.run()
            if ok2 == gtk.RESPONSE_ACCEPT:
              zfo_dir = d2.get_filename()
              d2.destroy()
            else:
              d.destroy()
              self._statusbar_message(_("Action was cancelled"))
              d2.destroy()
              return
          # we export ZFOs here
          ok, bad, missing = 0, 0, 0
          if exp.export_received:
            for message in exp.received_messages:
              result = self._export_message_zfo(message, "received", zfo_dir)
              if result == False:
                errors.append(_("Export of received message %d was not successful!")%
                              message.dmID)
                bad += 1
              elif result == True:
                ok += 1
              else:
                errors.append(_("Received message %d does not contain data \
necessary for ZFO export.") % message.dmID)
                missing += 1
          if exp.export_sent:
            for message in exp.sent_messages:
              result = self._export_message_zfo(message, "sent", zfo_dir)
              if result == False:
                errors.append(_("Export of sent message %d was not successful!")%
                              message.dmID)
                bad += 1
              elif result == True:
                ok += 1
              else:
                errors.append(_("Sent message %d does not contain data \
necessary for ZFO export.") % message.dmID)
                missing += 1
        # here we save the overview itself
        try:
          local_fullname = fullname.decode('utf-8')
          outf = file(local_fullname, "w")
          outf.write(html)
          outf.close()
        except IOError, e:
          errors.append(_("An error occurred during saving of file '%s'") %
                           fullname)
        if errors:
          m = _("There were some errors during saving of the overview:") + "\n\n"
          if len(errors) < 15:
            m += "\n".join(errors)
          elif exp.export_zfo:
            # too many errors to display one by one
            if bad:
              m += ngettext("%d message could not be exported because of an error.",
                            "%d messages could not be exported because of an error.",
                            bad) % bad
            if missing:
              m += "\n"
              m += ngettext("%d message did not contain data necessary for ZFO export.",
                            "%d messages did not contain data necessary for ZFO export.",
                            missing) % missing
          if exp.export_zfo:
            m += "\n\n"
            m += ngettext("%d message was successfully exported to ZFO.",
                          "%d messages were successfully exported to ZFO.",
                          ok) % ok
          self._show_error(m)
        else:
          m = _("The file '%s' was sucessfully exported.") % fullname
          if exp.export_zfo:
            m += " "
            m += ngettext("%d message was successfully exported to ZFO.",
                          "%d messages were successfully exported to ZFO.",
                          ok) % ok
          self._statusbar_message(m)
      else:
        self._statusbar_message(_("Action was cancelled"))
        d.destroy()
    else:
      d.destroy()
    
  def on_clear_search_entry_clicked(self, w):
    self.builder.get_object("message_search_entry").set_text("")

  def on_message_search_entry_changed(self, w):
    #[msgid,annot,sender,recipient,delivery_time,accept_time,status_str,
    # status,read,ver,sig,attempt,received,sent,
    # delivery_time_sort, accept_time_sort, 1]
    def block(model, path, iter):
        model.emit_stop_by_name("row-changed")
    
    handle_id = self.toplevel_message_store.connect("row-changed", block)
    text = w.get_text()
    words = text.lower().split()
    if not words:
      # empty search term, show all rows
      for row in self.message_store:
        row[self.COLUMN_SEARCH] = 1
    else:
      for row in self.message_store:
        for word in words:
          if word in row[1].lower() or \
             word in row[2].lower() or \
             word in row[3].lower():
            # annot, sender or recipient
            row[self.COLUMN_SEARCH] = 1
          else:
            row[self.COLUMN_SEARCH] = 0
            break # no more tries - one word not matching is enough
    self.toplevel_message_store.disconnect(handle_id)
      

  def on_change_data_directory_activate(self, w):
    from change_datadir_dialog import DataDirDialog
    d = DataDirDialog(self, self.account.database_dir)
    res = d.run()
    if res == 1:
      # we proceed
      old_dir = self.account.database_dir
      new_dir = d.new_dir
      copy, move, leave = d.copy, d.move, d.leave
      rewrite = 1
      d.destroy()
      if not new_dir or old_dir == new_dir:
        pass
      else:
        # we really need to make the transfer
        new_filename = self.account.db_filename(new_dir)
        enc_new_filename = new_filename.decode('utf-8')
        # check if the new filename does not collide with existing file
        if os.path.exists(enc_new_filename):
          if os.path.isfile(enc_new_filename):
            # there is already a file with the same name
            message = _("The selected directory already contains a database file\
 for the same data box.\n\nPlease select what to do:")
            d = gtk.Dialog(_("What to do with existing data"),
                           self.window1,
                           buttons=(_("Cancel"),0,
                                    _("Overwrite existing data"),1,
                                    _("Use existing data"),2),
                          )
            box = gtk.HBox()
            d.get_content_area().pack_start(box, padding=5)
            img = gtk.Image()
            img.set_from_stock(gtk.STOCK_DIALOG_WARNING, gtk.ICON_SIZE_DIALOG)
            img.show()
            box.pack_start(img, padding=10)
            label = gtk.Label(message)
            box.pack_start(label, padding=10)
            label.set_padding(0, 10)
            label.show()
            box.show_all()
            d.set_has_separator(False)
            rewrite = d.run()
            d.destroy()
            if rewrite not in (1,2):
              self._statusbar_message(_("Operation was cancelled"))
              return
          elif os.path.isdir(enc_new_filename):
            self._show_error(
                _("The directory '%s' prevents creation of database file.")%\
                new_filename)
            return
          else:
            self._show_error(
                _("Something with the name '%s' prevents creation of database file.")%\
                new_filename)
            return
        old_filename = self.account.db_filename()
        enc_old_filename = old_filename.decode('utf-8')
        # clean the message storage
        self.window1.set_sensitive(False)
        self.progressbar.set_text(_("Unloading old data"))
        self.progressbar.pulse()
        self.message_store.clear()
        if self.account.messages:
          self.account.messages.close()
        ok = True
        error = ""
        force_copy = False
        # change the directory
        if rewrite == 2:
          # we use the data in the new directory and leave the old
          # data where it is - regardless of method
          pass
        else:
          # we try move at first - it might lead to copy+delete
          if move:
            self.progressbar.set_fraction(0.5)
            self.progressbar.set_text(_("Moving data"))
            try:
              os.rename(enc_old_filename, enc_new_filename)
            except OSError:
              # simple rename is not possible - we will use copy+delete
              force_copy = True
          # copy
          if copy or force_copy:
            self.progressbar.set_fraction(0.0)
            self.progressbar.set_text(_("Copying data"))
            try:
              infile = file(enc_old_filename, "rb")
            except Exception, exception:
              error = _("Could not open database file for reading\
   '%(old_filename)s':\n %(exception)s") % locals()
              ok = False
            else:
              try:
                outfile = file(enc_new_filename, "wb")
              except Exception, exception:
                error = _("Could not open database file for writing\
   '%(new_filename)s':\n %(exception)s") % locals()
                ok = False
                infile.close()
              else:
                filesize = os.path.getsize(enc_old_filename)
                bufsize = 1024000
                data = infile.read(bufsize)
                processed_size = len(data)
                while data:
                  outfile.write(data)
                  data = infile.read(bufsize)
                  processed_size += len(data)
                  if filesize:
                    self.progressbar.set_fraction(1.0*processed_size/filesize)
                  while gtk.events_pending():
                    gtk.main_iteration(block=False)
                infile.close()
                outfile.close()
                if move:
                  # we used force_copy and we delete the original now
                  try:
                    os.remove(enc_old_filename)
                  except Exception, exception:
                    error = _("Could not delete the original file\
   '%(old_filename)s':\n %(exception)s\n\nThe new database copy will be used,\
   but the old one was not deleted.") % locals()
          elif leave:
            self.progressbar.set_fraction(0.5)
            self.progressbar.set_text(_("Creating new database"))
            try:
              f = file(enc_new_filename, "wb")
            except Exception, exception:
              ok = False
              error = _("Could not create file '%(new_filename)s':\n\
   %(exception)s") % locals()
            else:
              f.close()
          elif move:
            # this should be already dealt with
            pass
          else:
            error = _("Unexpected error - directory change was not successful!")
            ok = False
          # read the data back
          if error:
            self._show_error(error)
        if ok:
          self.account.database_dir = new_dir
          if rewrite == 2:
            message = _("Database directory change was successful.\n\
The old database was left in the original location.")
          else:
            message = _("Database directory change was successful.")
          d = gtk.MessageDialog(self.window1,
                      message_format=message,
                      buttons=gtk.BUTTONS_OK,
                      type=gtk.MESSAGE_INFO)
          d.run()
          d.destroy()
        # load the data from the new destination
        self.progressbar.set_fraction(0.0)
        self.progressbar.set_text(_("Idle"))
        self.account.open_message_storage()
        try:
          self.account.messages.open()
        except Exception, e:
          self._show_error(_("Could not open database.\n\nDatabase file '%s' is\
 missing or corrupted.") % self.account.db_filename())
          self.account.messages = None         
        account_row = self._get_selected_account_row()
        message_type = account_row[self.COLUMN_ACCOUNT_MESSAGE_TYPE]
        select_type = account_row[self.COLUMN_ACCOUNT_SELECT_TYPE]
        arch_year = account_row[self.COLUMN_ACCOUNT_ARCH_YEAR]
        self.load_messages_for_current_account(message_type, select_type,
                                               year=arch_year)
        self.window1.set_sensitive(True)
    else:
      d.destroy()

  def on_import_database_dir_activate(self, w):
    """imports all accounts for which a database is found in a directory"""
    d = gtk.FileChooserDialog(title=_("Select a directory"),
                              parent=self.window1,
                              action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                              buttons=(gtk.STOCK_OK,gtk.RESPONSE_ACCEPT,
                                       gtk.STOCK_CANCEL,gtk.RESPONSE_REJECT)
                               )
    d.set_current_folder(self._get_topical_last_dir())
    answer = d.run()
    if answer == gtk.RESPONSE_ACCEPT:
      dirname = d.get_filename()
      self._set_topical_last_dir(os.path.dirname(dirname))
      dirname_enc = dirname.decode('utf-8')
      matcher = re.compile("^([a-z0-9]{6,12})___([01]).db$")
      candidates = []
      for fname in os.listdir(dirname_enc):
        db_filename = os.path.join(dirname_enc, fname)
        if os.path.isfile(db_filename):
          m = matcher.match(fname)
          if m:
            candidates.append((db_filename,)+tuple(map(str, m.groups())))
      import_count = 0
      for db_filename, name, test in candidates:
        test = (test == "1") # 1==True
        for account in self.accounts:
          if account.credentials.username == name and \
            account.credentials.test_account == test:
            # we already have this account
            d2 = gtk.MessageDialog(self.window1,
                        buttons=gtk.BUTTONS_OK,
                        type=gtk.MESSAGE_WARNING)
            d2.set_markup(_("Account <i>%s</i> is already active.\n\n\
If you really want to import it from this directory, you have to \
remove it from Datovka first.") % name)
            d2.run()
            d2.destroy()
            break
        else:
          self.add_account(name, name, test_account=test,
                           database_dir=dirname)
          import_count += 1
      self._statusbar_message(
          ngettext("%(count)d database file was imported from %(dirname)s.",
                   "%(count)d database files were imported from %(dirname)s.",
                   import_count)%\
                   {"count": import_count, "dirname": dirname})
    d.destroy()
   
  def on_move_account_up_activate(self, w):
    self._move_account(-1)
    self.update_account_actions()
  
  def on_move_account_down_activate(self, w):
    self._move_account(1)
    self.update_account_actions()
    
  def _move_account(self, shift):
    curr_row = self._get_selected_account_row()
    if not curr_row:
      return
    # find the corresponding top level row
    while curr_row.parent is not None:
      curr_row = curr_row.parent
    # find the appropriate row in the account_store and its index
    found = None
    idx = None
    for i, row in enumerate(self.account_store):
      if list(row) == list(curr_row):
        found = row
        idx = i
        break
    if found and 0 <= idx+shift < len(self.account_store):
      replaced = self.account_store[idx+shift]
      self.account_store.swap(replaced.iter, found.iter)
      # swap it in the accounts list as well - it has the same ordering
      self.accounts[idx], self.accounts[idx+shift] = \
                            self.accounts[idx+shift], self.accounts[idx]
             

  def on_toplevel_message_store_changed(self, treemodel, path, iter):
    """
    this is typically called when the read/unread property of a row changes
    """
    account_row = self._get_selected_account_row(writable=True)
    if account_row and account_row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == \
                                                  self.MESSAGE_TYPE_RECEIVED:
      self._update_account_unread_from_message_model(treemodel, account_row)


  def _update_account_unread_from_message_model(self, model, account_row):
    """
    internal method to synchronize account row unread count with the message
    model
    """
    unread = 0
    for row in model:
      if row[self.COLUMN_READ] == 0:
        unread += 1
    account_row[self.COLUMN_ACCOUNT_UNREAD_COUNT] = unread
    
  def _update_account_unread_from_db(self):
    """
    internal method to synchronize account row unread count with the database
    """
    # received
    stats = self.account.get_account_db_stats_lowlevel(
                            message_type=DsdbMessageStore.MESSAGE_TYPE_RECEIVED)
    if stats:
      # recent
      row = self._get_sub_account_row_for_selected_account(
                    message_type=self.MESSAGE_TYPE_RECEIVED,
                    select_type=self.SELECT_TYPE_RECENT, has_messages=True)
      row[self.COLUMN_ACCOUNT_UNREAD_COUNT] = stats.get('recent', {})\
                                                              .get('unread', 0)
      # all
      # received
      parent_row = self._get_sub_account_row_for_selected_account(
                          message_type=self.MESSAGE_TYPE_RECEIVED,
                          select_type=self.SELECT_TYPE_ALL, has_messages=False)
      self._sync_account_store_years(parent_row.iter, stats)
    # sent
    stats = self.account.get_account_db_stats_lowlevel(
                            message_type=DsdbMessageStore.MESSAGE_TYPE_SENT)
    if stats:
      parent_row = self._get_sub_account_row_for_selected_account(
                          message_type=self.MESSAGE_TYPE_SENT,
                          select_type=self.SELECT_TYPE_ALL, has_messages=False)
      self._sync_account_store_years(parent_row.iter, stats)

    
  def on_toplevel_message_store_reordered(self, model, path, iter, new_order):
    self._scroll_to_active_message(self._get_active_message_treeview())
    
  def _scroll_to_active_message(self, treeview):
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      treeview.scroll_to_cell(paths[0], use_align=True, row_align=0.5)

  def on_mark_all_as_read_activate(self, w):
    tree_view = self._get_active_message_treeview()
    if not tree_view:
      # not tree view available - probably top level view (sub account)
      return
    mes_ids = set()
    for row in self.message_store:
      if not row[self.COLUMN_READ]:
        mes_ids.add(row[self.COLUMN_ID])
    if mes_ids:
      self.account.messages.database.mark_messages_as_read_locally(mes_ids)
      self._update_account_unread_from_db()
      # update rows, we need to do it on the underlying model
      for row in self.message_store:
        if row[self.COLUMN_ID] in mes_ids:
          row[self.COLUMN_READ] = True

  def on_delete_message_activate(self, w):
    treeview = self._get_active_message_treeview()
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      d = gtk.MessageDialog(self.window1,
                              message_format=_("Do you really want to remove "
                                               "selected message?"),
                              buttons=gtk.BUTTONS_OK_CANCEL,
                              type=gtk.MESSAGE_QUESTION)
      answer = d.run()
      d.destroy()
      if answer == gtk.RESPONSE_OK:
        path = paths[0]
        # get the right path for the underlying model
        for i in range(3):
          path = model.convert_path_to_child_path(path)
          model = model.get_model()
        mid = model[path][0]
        self.account.messages.remove_message(mid)
        model.remove(model.get_iter(path))
    
  
  def on_timeout(self):
    """various periodic checks"""
    if self._last_statusbar_push + 5 < time.time():
      self.statusbar.push(-1, "")
    return True

  # // end of Callbacks

  # helper methods for display

  def _render_read_icon(self, column, cell, model, iter):
    data = model.get_value(iter, self.COLUMN_READ)
    if data == True:
      stock = gtk.STOCK_YES
    else:
      stock = gtk.STOCK_NO
    pixbuf = self.window1.render_icon(stock_id=stock, size=gtk.ICON_SIZE_MENU)
    cell.set_property('pixbuf', pixbuf)
    
  def _render_account_name(self, column, cell, model, iter):
    text = model.get_value(iter, self.COLUMN_ACCOUNT_NAME)
    top_level = model.get_value(iter, self.COLUMN_ACCOUNT_TOP_LEVEL)
    has_messages = model.get_value(iter, self.COLUMN_ACCOUNT_HAS_MESSAGES)
    if top_level:
      # top-level is always bold and italic in case of test account 
      test_acc = model.get_value(iter, self.COLUMN_ACCOUNT_TEST)
      if test_acc == True:
        style = pango.STYLE_ITALIC
      else:
        style = pango.STYLE_NORMAL
      cell.set_property('style', style)
      cell.set_property('weight', pango.WEIGHT_BOLD)
      #cell.set_property('background', "#e0e0e0")
    else:
      # render the text with unread messages count and corresponding boldness
      cell.set_property('style', pango.STYLE_NORMAL)
      unread = model.get_value(iter, self.COLUMN_ACCOUNT_UNREAD_COUNT)
      if has_messages and unread > 0:
        text += " (%d)" % unread
        cell.set_property('weight', pango.WEIGHT_BOLD)
      else:
        cell.set_property('weight', pango.WEIGHT_NORMAL)
      #cell.set_property('background', "#fff")
    cell.set_property('text', text)

  def _render_read_text(self, column, cell, model, iter):
    if not model.iter_is_valid(iter):
      return
    data = model.get_value(iter, self.COLUMN_READ)
    if data == True:
      cell.set_property('weight', pango.WEIGHT_NORMAL)
    else: 
      cell.set_property('weight', pango.WEIGHT_BOLD)


  def _fill_message_data_into_textview(self, message, textview,
                                       additional_lines=None,
                                       warn_about_old_messages=True,
                                       show_id=False):
    def add_row(heading, text, add_tags=None, newline=True, indent=True):
      add = add_tags or ()
      position = textbuffer.get_end_iter()
      if indent:
        add += ("indent1",)
      textbuffer.insert_with_tags_by_name(position, heading, "bold", *add)
      position = textbuffer.get_end_iter()
      _text = text or _("Not available")
      if type(_text) is not unicode:
        _text = unicode(_text, 'utf-8')
      if heading:
        _text = " "+_text
      textbuffer.insert_with_tags_by_name(position, _text, *add)
      if newline:
        add_newline()
    def add_newline():
      position = textbuffer.get_end_iter()
      textbuffer.insert_with_tags_by_name(position, "\n")
    def add_heading(text):
      position = textbuffer.get_end_iter()
      textbuffer.insert_with_tags_by_name(position, text+"\n", "larger")

    # some basic info
    message_direction = None
    if self.account and self.account.messages.has_message(message.dmID):
      if self.account.messages.is_received(message.dmID):
        message_direction = "received"
      else:
        message_direction = "sent"
    # fill the text buffer
    textbuffer = textview.get_buffer()
    textbuffer.set_text("")
    # identification
    add_heading(_("Identification"))
    if show_id:
      add_row(_("ID:"), str(message.dmID))
    add_row(_("Subject:"), message.dmAnnotation or "")
    if message._dmType and message._dmType in self.message_type_to_text:
      #add_row(_("Commercial message:"), _("Yes"))
      add_row(_("Message type:"),
              self.message_type_to_text.get(message._dmType, message._dmType))
    add_newline()
    # information about message author
    message_author_text = None
    if hasattr(message, 'custom_data') and message.custom_data:
      message_author = message.custom_data.get('message_author')
      author_name = message_author.get('authorName')
      author_type = self.author_type_to_text.get(message_author.get('userType'))
      parts = []
      if author_name:
        parts.append(author_name)
      if author_type:
        parts.append(author_type)
      if parts:
        message_author_text = u", ".join(parts)
    add_row(_("From:"), message.dmSender)
    add_row(_("Sender Address:"), message.dmSenderAddress)
    if message_author_text:
      add_row(_("Message author:"), message_author_text)
    add_newline()
    add_row(_("To:"), message.dmRecipient)
    add_row(_("Recipient Address:"), message.dmRecipientAddress)
    add_newline()
    for name, value in (("dmSenderIdent",_("Our file mark")),
                        ("dmSenderRefNumber",_("Our reference number")),
                        ("dmRecipientIdent",_("Your file mark")),
                        ("dmRecipientRefNumber",_("Your reference number")),
                        ("dmToHands",_("To hands")),
                        ("dmLegalTitleLaw",_("Law")),
                        ("dmLegalTitleYear",_("Year")),
                        ("dmLegalTitleSect",_("Section")),
                        ("dmLegalTitlePar",_("Paragraph")),
                        ("dmLegalTitlePoint",_("Letter"))):
      if getattr(message, name):
        add_row(value+":", getattr(message, name))
    add_newline()
    # Status
    add_heading(_("Status"))
    add_row(_("Delivery time:"), os_support.format_datetime(message.dmDeliveryTime))
    add_row(_("Acceptance time:"), os_support.format_datetime(message.dmAcceptanceTime))
    add_row(_("Status:"), "%d - %s" % (message.dmMessageStatus,
                                       message.get_status_description()))
    # events
    if message.dmEvents:
      add_row(_("Events:"), " ")
      for event in message.dmEvents:
        etime = event.dmEventTime
        if type(etime) in (str, unicode):
          if "." in etime:
            etime = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S.%f")
          else:
            etime = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
        add_row("", "%s - %s" % (os_support.format_datetime(etime),
                                 event.dmEventDescr),
                add_tags=("indent2",))
    if additional_lines:
      for head, text in additional_lines:
        add_row(head, text)
    if warn_about_old_messages and not message.still_on_server() \
      and not (hasattr(message, 'dmFiles') and message.dmFiles) \
      and message.dmAcceptanceTime:
      # message is not completely downloaded and it is not available anymore
      # do not warn about messages without dmAcceptanceTime
      add_row(_("Warning:"),
              _("The message is older than 90 days and therefore probably no longer available on the server."),
              add_tags=("italic","warning"))
      add_newline()
    # Signature
    add_heading(_("Signature"))
    if not message.message_verification_attempted():
      verified = _("Not present")
      add_row(_("Message signature:"), verified)
      position = textbuffer.get_end_iter()
      textbuffer.insert(position, "  ")
      anchor = textbuffer.create_child_anchor(position)
      btn = gtk.Button(_("Verify now!"))
      textview.add_child_at_anchor(btn, anchor)
      if message_direction == 'received':
        btn.connect("clicked",
                    self.on_download_complete_message_signed_activate,
                    textview)
      elif message_direction == 'sent':
        btn.connect("clicked",
                    self.on_verify_sent_message_signature_activate,
                    textview)
      btn.show()
      position = textbuffer.get_end_iter()
      textbuffer.insert_with_tags_by_name(position, "\n")
    else:
      warning = _("Message signature and content do not correspond!")
      verified = message.is_message_verified() and _("Valid") or \
                 _("Invalid")+" - "+warning
      add_row(_("Message signature:"), verified)
      if message.is_message_verified():
        # check signing certificate
        date = self._get_message_verification_date(message)
        verified = message.was_signature_valid_at_date(
                              date,
                              ignore_missing_crl_check=not ConfigManager.CHECK_CRL)
        verified_text = verified and _("Valid") or _("Invalid")
        if not ConfigManager.CHECK_CRL:
          verified_text += " (%s)" % _("Certificate revocation check is turned off!")
        add_row(_("Signing certificate:"), verified_text)
    tst_check = message.check_timestamp()
    if tst_check == None:
      tst_text = _("Not present")
    else:
      timestamp_check = tst_check and _("Valid") or _("Invalid!")
      if message.tstamp_token:
        timestamp_time = message.tstamp_token.get_genTime_as_datetime()
        tst_text = "%s (%s)" % (timestamp_check, os_support.format_datetime(timestamp_time))
      else:
        tst_text = timestamp_check  
    add_row(_("Timestamp:"), tst_text)
    # add button for displaying of details about the signature
    position = textbuffer.get_end_iter()
    textbuffer.insert(position, "  ")
    anchor = textbuffer.create_child_anchor(position)
    btn = gtk.Button(_("Signature details"))
    textview.add_child_at_anchor(btn, anchor)
    btn.connect("clicked", self.on_show_signature_details_activate, message)
    btn.show()
    #
    add_newline()

  def _scroll_to_last_visited_message(self):
    treeview = self._get_active_message_treeview()
    last_msg_id = self.account.last_message_id
    if treeview and last_msg_id:
      model = treeview.get_model()
      sel_path = None
      for row in model:
        if last_msg_id == row[0]:
          sel_path = row.path
      if sel_path is not None:
        treeview.scroll_to_cell(sel_path, use_align=True, row_align=0.5)
        treeview.get_selection().select_path(sel_path)

  def _scroll_to_newest_message(self):
    treeview = self._get_active_message_treeview()
    if treeview:
      model = treeview.get_model()
      newest = None
      max_id = -1
      for row in model:
        if max_id < row[0]:
          max_id = row[0]
          newest = row.path
      if newest is not None:
        treeview.scroll_to_cell(newest, use_align=True, row_align=0.5)
        treeview.get_selection().select_path(newest)

  # // end of helper methods for display

  def activate_message_actions(self, message):
    message_actions = ["download_complete_message","download_complete_message_signed",
                       "save_attachment","open_attachment","show_signature_details",
                       "reply_to_message","authenticate_message","export_zfo",
                       "export_delivery_info_zfo","export_delivery_info_pdf",
                       "export_envelope_pdf","open_message_zfo_externally",
                       "open_delivery_info_zfo_externally","delete_message"]
    if not message:
      for action in message_actions:
        self.builder.get_object(action).set_sensitive(False)
    elif not self.account.messages.has_message(message.dmID):
      return
    else:
      has_raw_data = self.account.messages.has_raw_data(message.dmID)
      has_raw_di = self.account.messages.has_raw_delivery_info_data(message.dmID)
      for action in message_actions:
        action_obj = self.builder.get_object(action)
        if action == "download_complete_message":
          if hasattr(message, 'dmFiles') and message.dmFiles:
            action_obj.set_sensitive(False)
          else:
            action_obj.set_sensitive(True)
        elif action in ("save_attachment","open_attachment"):
          action_obj.set_sensitive(False)
        elif action in ("authenticate_message","export_zfo",
                        "open_message_zfo_externally"):
          if has_raw_data:
            action_obj.set_sensitive(True)
          else:
            action_obj.set_sensitive(False)
        elif action in ("export_delivery_info_zfo",
                        "open_delivery_info_zfo_externally"):
          if has_raw_di:
            action_obj.set_sensitive(True)
          else:
            action_obj.set_sensitive(False)
        elif action in ("export_delivery_info_pdf",):
          if PDF_EXPORT_AVAILABLE and has_raw_di:
            action_obj.set_sensitive(True)
          else:
            action_obj.set_sensitive(False)  
        elif action == "export_envelope_pdf":
          if hasattr(message, 'dmFiles') and message.dmFiles and \
            PDF_EXPORT_AVAILABLE:
            action_obj.set_sensitive(True)
          else:
            action_obj.set_sensitive(False)
        elif action == "reply_to_message":
          if message.message_type == DsdbMessageStore.MESSAGE_TYPE_SENT:
            action_obj.set_sensitive(False)
          else:
            action_obj.set_sensitive(True)
        elif action == "delete_message":
          if message.still_on_server():
            # do not allow deleting recent messages 
            # - they would be redownloaded anyway
            action_obj.set_sensitive(False)
          else:
            action_obj.set_sensitive(True)
        else:
          action_obj.set_sensitive(True)
    if QUARTZ_ENABLE:
        self.macapp.sync_menubar()
          
  def update_account_actions(self):
    """
    Update actions that depend on the position and other properties of the
    active account
    """
    # find the corresponding top level row
    curr_row = self._get_selected_account_row()
    if curr_row:
        is_toplevel = curr_row[self.COLUMN_ACCOUNT_TOP_LEVEL]
        curr_top_row = self._get_sub_account_row_for_selected_account(
                                                                top_level=True)
        active_type = self._get_active_message_type()
        received_page = (active_type == 'received')
    else:
        curr_top_row = None
        is_toplevel = None
    actions = ("connect_action", "find_data_box", "send_message",
               "change_password", "authenticate_file",
               "export_correspondence_overview", "edit_credentials",
               "move_account_down", "move_account_up",
               "sync_all_accounts", "remove_account")
    if curr_top_row:
      for action in actions:
        self.builder.get_object(action).set_sensitive(True)
      # special cases
      # do not allow moving of data directories in portable version
      self.builder.get_object("change_data_directory").set_sensitive(
                                                              not self.portable)
      # mark read should be active only for non-toplevels
      maar_active = received_page and not is_toplevel
      self.builder.get_object("mark_all_as_read").set_sensitive(maar_active)
      # find the appropriate row in the account_store and its index
      idx = None
      for i, row in enumerate(self.account_store):
        if list(row) == list(curr_top_row):
          idx = i
          break
      # adjust the move_up and move_down actions according to current position
      if idx is not None:
        if idx == 0:
          self.builder.get_object("move_account_up").set_sensitive(False)
        else:
          self.builder.get_object("move_account_up").set_sensitive(True)
        if idx == len(self.account_store)-1:
          self.builder.get_object("move_account_down").set_sensitive(False)
        else:
          self.builder.get_object("move_account_down").set_sensitive(True)
    else:
      for action in actions:
        self.builder.get_object(action).set_sensitive(False)
      self.builder.get_object("show_signature_details").set_sensitive(False)
      self.builder.get_object("change_data_directory").set_sensitive(False)
      self.builder.get_object("mark_all_as_read").set_sensitive(False)
    # the following depend only on account existence, not its selection
    if len(self.account_store) > 0:
      self.builder.get_object("sync_all_accounts").set_sensitive(True)
    else:
      self.builder.get_object("sync_all_accounts").set_sensitive(False) 
        

  def show_received_message(self, message, textview=None, attachment_store=None,
                            warn_about_old_messages=True, show_id=False):
    if message:
      if not textview:
        textview = self.received_message_textview
      if not attachment_store:
        attachment_store = self.received_attachment_store
      if hasattr(message, 'dmFiles') and message.dmFiles:
        _message = ngettext("%d (downloaded and ready)",
                            "%d (downloaded and ready)",
                            len(message.dmFiles)) % len(message.dmFiles)
        attachments = (_("Attachments:"), _message)
      elif message.dmAttachmentSize:
        attachments = (_("Attachments:"),
                       _("not downloaded yet, ~%s KB; use 'Download' to get them.")%message.dmAttachmentSize)
      else:
        attachments = (_("Attachments:"), _("not available"))
      self._fill_message_data_into_textview(message, textview,
                              additional_lines=[attachments],
                              warn_about_old_messages=warn_about_old_messages,
                              show_id=show_id)
      # show the attachments
      attachment_store.clear()
      for i,f in enumerate(message.dmFiles):
        norm_att_fname = unicodedata.normalize("NFC", f._dmFileDescr)
        attachment_store.append([i,norm_att_fname,f.get_size(),
                                 f._dmMimeType,f._dmFormat])
      # mark message read
      mid = int(message.dmID)
      if self.account:
        if not self.account.messages.get_message_supplementary_data(mid,'read_locally'):
          self.account.messages.set_message_supplementary_data(mid,'read_locally',True)
          for row in self.message_store:
            if row[0] == mid:
              row[8] = True
              break

  def show_sent_message(self, message, textview=None):
    self.show_received_message(message, textview=textview)

  def _add_message(self, message, typ, additional_data=None):
    msgid = int(message.dmID)
    read_locally = False
    result = GUI.ADD_MESSAGE_RESULT_NEW
    if self.account.messages.has_message(msgid):
      stored_message = self.account.messages.get_message_by_id(msgid)
      if not message.is_complete() and stored_message.is_complete() and \
        (message.dmMessageStatus == stored_message.dmMessageStatus \
         or (message.dmMessageStatus == 10 and \
             stored_message.dmMessageStatus in (5,6,7))) and \
         message._dmType == stored_message._dmType:
        # this is a complete message - do not replace it
        # either the dmMessageStatus is the same or the new one has
        # 10 (data vault) and the old was delivered (5,6,7) - so no redownload
        # is necessary
        # (it could be useful to create a list of increasing origin importance)
        return GUI.ADD_MESSAGE_RESULT_SKIPPED
      else:
        # find what is the result of this action
        if not message.is_complete() and stored_message.is_complete():
          result = GUI.ADD_MESSAGE_RESULT_DOWNGRADE
        elif message.is_complete():
          result = GUI.ADD_MESSAGE_RESULT_REPLACED
        else:
          # both are incomplete
          if message.dmMessageStatus != stored_message.dmMessageStatus or \
            message._dmType != stored_message._dmType:
            # upgrade because status has changed
            result = GUI.ADD_MESSAGE_RESULT_UPGRADE
          else:
            # no need to upgrade - it is all the same
            return GUI.ADD_MESSAGE_RESULT_SKIPPED
        # use current data on read_locally
        read_locally = bool(self.account.messages.\
                            get_message_supplementary_data(msgid, 'read_locally'))
    if additional_data:
      raw_data = additional_data.get('raw_data', None)
      custom_data = additional_data.get('custom_data', None)
    else:
      raw_data = None
      custom_data = None

    supp_data = {'read_locally': read_locally,
                 'download_date': datetime.datetime.now(),
                 'message_type': self.account.messages.\
                                          message_type_str_to_code(typ),
                 'custom_data': custom_data}
    message.read_locally = supp_data['read_locally']
    message.message_type = supp_data['message_type']
    message.custom_data = custom_data
    self.account.messages.add_message(message, raw_data=raw_data,
                                      supp_data=supp_data)
    if additional_data and additional_data.get('delivery_info'):
      di_data = additional_data.get('delivery_info')
      di_raw_data = additional_data.get('delivery_info_raw_data')
      self.account.messages.add_delivery_info_data(int(message.dmID),
                                                   di_data,
                                                   raw_data=di_raw_data,
                                                   )
    # decide if the message should be added to the current message list
    account_row = self._get_selected_account_row()
    active_type = self._get_active_message_type()
    if typ == active_type:
      # only add message to model if it has the current type
      start, end = self._calculate_message_date_limits(
                                account_row[self.COLUMN_ACCOUNT_SELECT_TYPE],
                                year=account_row[self.COLUMN_ACCOUNT_ARCH_YEAR])
      add = False
      if start == 0 and end == 0:
        pass # do not add
      elif start is None and end is None:
        add = True
      elif end is None:
        # no upper limit
        if not message.dmDeliveryTime or message.dmDeliveryTime >= start:
          add = True
      elif start is None:
        # no lower limit
        if message.dmDeliveryTime and message.dmDeliveryTime < end:
          add = True
      else:
        # both start and end
        if message.dmDeliveryTime and message.dmDeliveryTime >= start and \
            message.dmDeliveryTime < end:
          add = True
      if add:
        data = self._message_to_row(message, typ)
        self._add_message_row(data)
    return result
    
    
  def _add_message_row(self, new_row):
    """adds a row into self.message_store store or updates it
    if it is already there"""
    mid = new_row[0]
    for i,row in enumerate(self.message_store):
      if row[0] == mid:
        if list(self.message_store[i]) != new_row:
          # update only if there is a difference
          self.message_store[i] = new_row
        break
    else:
      self.message_store.append(new_row)
      
  def _get_message_verification_date(self, message):
    if ConfigManager.CERTIFICATE_VALIDATION_DATE == ConfigManager.DOWNLOAD_DATE:
      if self.account:
        download_date = self.account.messages.get_message_supplementary_data\
                              (message.dmID, 'download_date')
        if download_date:
          return download_date
    return datetime.datetime.now()  
    
  def _message_to_row(self, message, typ):
    """typ is either 'received' or 'sent'"""
    msgid = int(message.dmID)
    annot = message.dmAnnotation or ""
    sender = message.dmSender
    recipient = message.dmRecipient
    # dates
    delivery_time = os_support.format_datetime(message.dmDeliveryTime)
    accept_time = os_support.format_datetime(message.dmAcceptanceTime)
    # this is for sorting
    delivery_time_sort = ""
    if message.dmDeliveryTime:
      delivery_time_sort = message.dmDeliveryTime.isoformat()
    accept_time_sort = ""
    if message.dmAcceptanceTime:
      accept_time_sort = message.dmAcceptanceTime.isoformat()
    # ready or not - look it the value is available on the object
    received = typ == 'received'
    sent = typ == 'sent'
    if received:
      if hasattr(message, 'read_locally'):
          read = message.read_locally
      else:
          read = self.account.messages.get_message_supplementary_data(msgid,
                                                                'read_locally')
    else:
      read = False
    status = str(message.dmMessageStatus)
    status_str = message.get_status_description()
    ver = True # deprecated - not in use anymore
    sig = True # deprecated - not in use anymore
    attempt = True # deprecated - not in use anymore
    id_for_sort = "%012d" % msgid
    annot_sort = annot+id_for_sort
    sender_sort = sender+id_for_sort
    recipient_sort = recipient+id_for_sort
    status_sort = ("%03d"%int(message.dmMessageStatus))+id_for_sort
    read_sort = sent and ("1"+id_for_sort) or ("0"+id_for_sort)
    _data = [msgid, annot, sender, recipient, delivery_time, accept_time,
             status_str, status, read, ver, sig, attempt, received, sent,
             delivery_time_sort, accept_time_sort, True,
             # sort columns follow
             annot_sort, sender_sort, recipient_sort, status_sort, read_sort]
    data = []
    for value in _data:
      if type(value) == unicode:
        value = value.encode('utf-8')
      data.append(value)
    return data
      
  def add_account(self, name, username, password=None, test_account=False,
                  p12file=None, login_method="username", remember_password=True,
                  database_dir=None, sync_with_all=True, last_message_id=None):
    for row in self.account_store:
      if row[0] == username and row[1] == test_account:
        # this account is already present - cannot add it twice
        return -1
    # create a Credentials instance
    cr = Credentials(username,
                    password=password,
                    test_account=test_account,
                    p12file=p12file,
                    login_method=login_method,
                    remember_password=remember_password)
    if ConfigManager.STORE_MESSAGES_ON_DISK:
      if not database_dir:
        database_dir = self.get_dsgui_private_dir()
      account = Account(name, cr, database_dir, sync_with_all=sync_with_all)
    else:
      account = Account(name, cr, None, sync_with_all=sync_with_all)
    if last_message_id:
      account.last_message_id = last_message_id
    # try to load additional data from shelf
    try:
      shelf = self._open_shelf()
    except Exception, e:
      self._statusbar_message(
              _("Additional account data could not be read from storage."))
    else:
      if shelf != None:
        account.load_from_shelf(shelf)
        shelf.close()
    # try to add info to the tooltip of an account
    text = self._get_tooltip_for_account(account)
    stats = account.get_account_db_stats_lowlevel(
                                        message_type=self.MESSAGE_TYPE_RECEIVED)
    recent_unread_count = 0
    if stats:
      recent_unread_count = stats.get('recent', {}).get('unread', 0)
    # populate the treemodel
    # parent row for the whole account
    row = self._create_account_row(username, test_account, text=text, name=name,
                                   top_level=True, order=1)
    iter = self.account_store.append(None, row)
                                     
    # recent sub-parent
    # recent children
    # received
    row = self._create_account_row(username, test_account,
                                   text=_("Received messages for last 90 days"),
                                   name=_("Recent received"),
                                   select_type=self.SELECT_TYPE_RECENT,
                                   has_messages=True,
                                   unread=recent_unread_count,
                                   message_type=self.MESSAGE_TYPE_RECEIVED,
                                   order=1)
    self.account_store.append(iter, row)
    # sent
    row = self._create_account_row(username, test_account,
                                   text=_("Sent messages for last 90 days"),
                                   name=_("Recent sent"),
                                   select_type=self.SELECT_TYPE_RECENT,
                                   has_messages=True,
                                   message_type=self.MESSAGE_TYPE_SENT,
                                   order=2)
    self.account_store.append(iter, row)
    # all sub-parent
    row = self._create_account_row(username, test_account,
                                   text=_("All messages"),
                                   name=_("All"),
                                   select_type=self.SELECT_TYPE_ALL,
                                   order=3)
    sub_iter = self.account_store.append(iter, row)
    # all children
    # received
    row = self._create_account_row(username, test_account,
                                   text=_("All received messages"),
                                   name=_("Received"),
                                   select_type=self.SELECT_TYPE_ALL,
                                   has_messages=False,
                                   message_type=self.MESSAGE_TYPE_RECEIVED,
                                   order=1)
    subsub_iter = self.account_store.append(sub_iter, row)
    # years
    self._sync_account_store_years(subsub_iter, stats)
    # sent
    row = self._create_account_row(username, test_account,
                                   text=_("All sent messages"),
                                   name=_("Sent"),
                                   select_type=self.SELECT_TYPE_ALL,
                                   has_messages=False,
                                   message_type=self.MESSAGE_TYPE_SENT,
                                   order=2)
    subsub_iter = self.account_store.append(sub_iter, row)
    # years
    stats = account.get_account_db_stats_lowlevel(
                                            message_type=self.MESSAGE_TYPE_SENT)
    self._sync_account_store_years(subsub_iter, stats)

    self.accounts.append(account)
    #self.account_treeview.expand_all()
    return self.account_store.get_path(iter)
  
  def _create_account_row(self, username, test_account, text="", name="",
                          message_type=MESSAGE_TYPE_NONE, top_level=False,
                          has_messages=False, select_type=SELECT_TYPE_ALL,
                          unread=0, year=0, order=0):
    # model [username, test_account, text, name, mods, unread, year, sorting,
    #        top_level, has_messages, message_type, select_type]
    return [username, test_account, text, name, 0, unread, year, order,
            top_level, has_messages, message_type, select_type]
    
  
  def _sync_account_store_years(self, parent_iter, stats):
    username, test, m_type = self.account_store.get(parent_iter,
                                            self.COLUMN_ACCOUNT_ID,
                                            self.COLUMN_ACCOUNT_TEST,
                                            self.COLUMN_ACCOUNT_MESSAGE_TYPE,
                                            )
    if stats:
      to_add = []
      for year in sorted(stats.get('all', {})):
        child_iter = self.account_store.iter_children(parent_iter)
        count = stats['all'][year]['unread']
        while child_iter:
          if self.account_store.get(child_iter,
                                    self.COLUMN_ACCOUNT_ARCH_YEAR)[0] == year:
            self.account_store.set(child_iter,
                                   self.COLUMN_ACCOUNT_UNREAD_COUNT, count)
            break
          child_iter = self.account_store.iter_next(child_iter)
        if not child_iter:
          row = self._create_account_row(username,
                                         test,
                                         text=_("Year %d") % year,
                                         name=str(year),
                                         message_type=m_type,
                                         has_messages=True,
                                         select_type=self.SELECT_TYPE_ALL,
                                         unread=count,
                                         year=year,
                                         order=year)
          to_add.append(row)
      for row in to_add:
        self.account_store.append(parent_iter, row)
    
  
  def set_account_info(self, info):
    self.account.account_info = info
    # update info in tooltip
    account_row = self._get_selected_account_row(writable=True)
    if account_row:
      account_row[self.COLUMN_ACCOUNT_TOOLTIP] = \
                                    self._get_tooltip_for_account(self.account)
      
      
  def check_expired_password(self, account):
    if account.has_password_expired():
      inp = {"username": account.credentials.username}
      message = _(u"""According to the last available information, \
your password for login '%(username)s' has expired.

In this case, it is unfortunately <b>not possible to change it directly from \
Datovka</b>.

Unless you have already done so, you need to use the official web interface \
of Datové schránky to change it. After doing so, you can either give your new \
password here or anytime later using the 'Databox/Edit credentials' \
command.

<i>Do you already have a new password and want to use it?</i>
""") % inp
      d = gtk.MessageDialog(self.window1,
                          buttons=gtk.BUTTONS_YES_NO,
                          type=gtk.MESSAGE_WARNING)
      d.set_markup(message)
      result = d.run()
      d.destroy()
      if result == gtk.RESPONSE_YES:
        ok = self.on_edit_credentials_activate(None)
        # update password info
        if ok == 1 and self.account.credentials.login_method in ("username",
                                                         "user_certificate",
                                                         "hotp", "totp"):
          # check info about password expiration
          # - available only for password logins
          soap_res = self.try_client_method(
            "GetPasswordInfo",
            (),
            error_message=_("It was not possible to download info about password expiration."))
          if not soap_res.fatal_error() and not soap_res.soap_error():
            reply = soap_res.reply
            if self.account.password_expiration_date != reply.data:
              self.account.password_expiration_date = reply.data
          self._show_account_info()
    elif account.is_password_expiring(10):
      delta = account.get_password_expiration_delta()
      inp = {"days": delta.days,
             "date": account.password_expiration_date,
             "username": account.credentials.username}
      message = ngettext("""According to the last available information, \
your password for login '%(username)s' will expire in %(days)s day (%(date)s).""",
                         """According to the last available information, \
your password for login '%(username)s' will expire in %(days)s days (%(date)s).""",
                inp['days']) % inp
      message += "\n"
      message += _("""
You can change your password now, or later using the 'Databox/Change password' \
command. Your new password will be valid for 90 days.

<i>Change password now?</i>""" % inp)
      d = gtk.MessageDialog(self.window1,
                            buttons=gtk.BUTTONS_YES_NO,
                            type=gtk.MESSAGE_WARNING)
      d.set_markup(message)
      result = d.run()
      d.destroy()
      if result == gtk.RESPONSE_YES:
        self._change_account_password()
    else:
      pass  

  def _get_tooltip_for_account(self, account):
    info = account.account_info
    text = "<b>%s</b>: <b>%s</b>\n" % (_("Account"),account.name)
    if account.credentials:
      text += "<b>%s</b>: %s\n" % (_("Username"),account.credentials.username)
    if info:
      for attr,name in GUI.account_info_attrs:
        if attr in ('dbID','pnFirstName','pnLastName',"biDate","firmName"):
          value = getattr(info, attr)
          if value:
            text += "<b>%s</b>: %s\n" % (name, value)
    text += "<b>%s</b>: %s" % \
                (_("Account type"),
                 self._get_account_type_as_text(account.credentials.test_account))
    return text

  def _show_account_info(self):
    # add info to the account_textview
    textbuffer = self.builder.get_object("account_info_textbuffer")
    textbuffer.set_text("")
    textbuffer.insert_with_tags_by_name(
      textbuffer.get_end_iter(),
      "%s\n" % self._get_account_type_as_text(self.account.credentials.test_account),
      'larger')
    textbuffer.insert_at_cursor("\n")
    position = textbuffer.get_end_iter()
    textbuffer.insert_with_tags_by_name(position, _("Account name")+":", "bold")
    textbuffer.insert_at_cursor(" %s\n\n"%self.account.name)
    position = textbuffer.get_end_iter()
    textbuffer.insert_with_tags_by_name(position, _("Username")+":", "bold")
    textbuffer.insert_at_cursor(" %s\n"%self.account.credentials.username)
    info = self.account.account_info
    if info:
      for attr,name in GUI.account_info_attrs:
        if hasattr(info, attr):
          value = getattr(info, attr)
          if value not in ("", None):
            position = textbuffer.get_end_iter()
            textbuffer.insert_with_tags_by_name(position, name+":", "bold")
            if type(value) == bool:
              textbuffer.insert_at_cursor(" %s\n"%(value and _("Yes") or _("No")))
            else:
              textbuffer.insert_at_cursor(" %s\n"%value)
      textbuffer.insert_at_cursor("\n")
      position = textbuffer.get_end_iter()
      textbuffer.insert_with_tags_by_name(position,
                                          _('Password expiration date:'),
                                          "bold")
      if self.account.credentials.login_method == "certificate":
        textbuffer.insert_at_cursor(
              " %s\n"%_("Not available when certificate login is used"))
      else:
        date = os_support.format_datetime(self.account.password_expiration_date)
        textbuffer.insert_at_cursor(
              " %s\n"%(date or _("unknown or without expiration")))
    else:
      position = textbuffer.get_end_iter()
      textbuffer.insert_at_cursor(_("Data has not been downloaded yet."))
      
      
  def _show_sub_account_info(self):
    """
    Fills the account_info_textbuffer with information about the currently
    selected sub-account
    """
    row = self._get_selected_account_row()
    title = ""
    if row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == self.MESSAGE_TYPE_NONE:
      if row[self.COLUMN_ACCOUNT_SELECT_TYPE] == self.SELECT_TYPE_ALL:
        title = _("All messages")
      else:
        title = _("All recent messages")
    elif row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == self.MESSAGE_TYPE_RECEIVED:
      if row[self.COLUMN_ACCOUNT_SELECT_TYPE] == self.SELECT_TYPE_ALL:
        title = _("All received messages")
      else:
        title = _("Recent received messages")
    elif row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == self.MESSAGE_TYPE_SENT:
      if row[self.COLUMN_ACCOUNT_SELECT_TYPE] == self.SELECT_TYPE_ALL:
        title = _("All sent messages")
      else:
        title = _("Recent sent messages")
    textbuffer = self.builder.get_object("account_info_textbuffer")
    textbuffer.set_text("")
    textbuffer.insert_with_tags_by_name(textbuffer.get_end_iter(),
                                        title, 'larger')
    textbuffer.insert_at_cursor("\n\n")
    if row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] in (self.MESSAGE_TYPE_NONE,
                                                 self.MESSAGE_TYPE_RECEIVED):
      position = textbuffer.get_end_iter()
      textbuffer.insert_with_tags_by_name(position, _("Received messages"),
                                          "bold")
      textbuffer.insert_at_cursor("\n")
      stat = self.account.get_account_db_stats_lowlevel(
                                        message_type=self.MESSAGE_TYPE_RECEIVED)
      if not stat:
        textbuffer.insert_at_cursor("    "+_("No messages")+"\n")
      else:
        for year, counts in sorted(stat.get('all', {}).iteritems()):
          text = "    %d: %d\n" % (year, counts.get('total', 0))
          textbuffer.insert_at_cursor(text)
      textbuffer.insert_at_cursor("\n")
    if row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] in (self.MESSAGE_TYPE_NONE,
                                                 self.MESSAGE_TYPE_SENT):
      position = textbuffer.get_end_iter()
      textbuffer.insert_with_tags_by_name(position, _("Sent messages"),
                                          "bold")
      textbuffer.insert_at_cursor("\n")
      stat = self.account.get_account_db_stats_lowlevel(
                                        message_type=self.MESSAGE_TYPE_SENT)
      if not stat:
        textbuffer.insert_at_cursor("    "+_("No messages")+"\n")
      else:
        for year, counts in sorted(stat.get('all', {}).iteritems()):
          text = "    %d: %d\n" % (year, counts.get('total', 0))
          textbuffer.insert_at_cursor(text)
      textbuffer.insert_at_cursor("\n") 


  def _get_selected_message_and_type(self):
    m_type = self._get_active_message_type()
    if not m_type:
      return None, None
    elif m_type == "sent":
      return self._get_selected_sent_message(), "sent"
    else:
      return self._get_selected_received_message(), "received"
  
  def _get_active_message_type(self):
    acc_row = self._get_selected_account_row()
    # top-level does not show treeview, so it would not make sense to return
    # something
    if acc_row[self.COLUMN_ACCOUNT_TOP_LEVEL]:
      return None
    if acc_row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == self.MESSAGE_TYPE_SENT:
      return "sent"
    elif acc_row[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == self.MESSAGE_TYPE_RECEIVED:
      return "received"
    else:
      return None

  def _get_active_message_treeview(self):
    m_type = self._get_active_message_type()
    if not m_type:
      return None
    elif m_type == "sent":
      return self.sent_message_treeview
    else:
      return self.received_message_treeview

  def _get_selected_received_message(self):
    treeview = self.received_message_treeview
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      message = self.account.messages.get_message_by_id(model[paths[0]][0])
      custom_data = self.account.messages.get_message_supplementary_data(
                                                  message.dmID, 'custom_data')
      message.custom_data = custom_data
      return message
    return None
    
  def _get_selected_attachment(self, treeview=None):
    """
    tree is a TreeView in which we are interested in the selected attachment,
    if not treeview is given, than 'received_message_treeview' is used
    """
    message, typ = self._get_selected_message_and_type()
    if message:
      if not treeview:
        treeview = self.received_attachment_treeview
      model, paths = treeview.get_selection().get_selected_rows()
      if len(paths) == 1:
        idx = model[paths[0]][0]
        if len(message.dmFiles) > idx:
          attachment = message.dmFiles[idx]
          return message, attachment
      return message, None
    return None, None

  def _get_selected_sent_message(self):
    treeview = self.sent_message_treeview
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      message = self.account.messages.get_message_by_id(model[paths[0]][0])
      custom_data = self.account.messages.get_message_supplementary_data(
                                                  message.dmID, 'custom_data')
      message.custom_data = custom_data
      return message
    return None
      
      
  def _get_selected_account_row(self, writable=False):
    treeview = self.account_treeview
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      path = paths[0]
      if writable:
        # we need to obtain the underlying row that is not only read-only
        path = model.convert_path_to_child_path(path)
        model = model.get_model()
      return model[path]
    return None
  
  def _get_sub_account_row_for_selected_account(self, top_level=False,
                                  message_type=MESSAGE_TYPE_NONE,
                                  has_messages=False,
                                  select_type=SELECT_TYPE_ALL, year=0):
    """
    returns the row corresponding to the received sub-account
    for current account
    """
    def walkit(_parent):
      if _parent[self.COLUMN_ACCOUNT_TOP_LEVEL] == top_level and \
         _parent[self.COLUMN_ACCOUNT_MESSAGE_TYPE] == message_type and \
         _parent[self.COLUMN_ACCOUNT_HAS_MESSAGES] == has_messages and \
         _parent[self.COLUMN_ACCOUNT_SELECT_TYPE] == select_type and \
         _parent[self.COLUMN_ACCOUNT_ARCH_YEAR] == year:
        return _parent
      for child in _parent.iterchildren():
        match = walkit(child)
        if match:
          return match
    
    treeview = self.account_treeview
    model, paths = treeview.get_selection().get_selected_rows()
    if len(paths) != 1:
      return None
    # we need to obtain the underlying row that is not only read-only
    path = paths[0]
    path = model.convert_path_to_child_path(path)
    model = model.get_model()
    # find parent
    parent = model[path]
    i = 0
    while not parent[self.COLUMN_ACCOUNT_TOP_LEVEL]:
      parent = parent.parent
      i += 1
      if i > 10:
        raise Exception(
                  "Probably infinite loop detected when looking for parent")
    if top_level:
      # we want the parent - return it immediately
      return parent
    return walkit(parent)

  
  def _get_account_object_for_account_row(self, row):
    for account in self.accounts:
      if account.credentials.username == row[0] and \
        account.credentials.test_account == row[1]:
        return account
    return None

  def _save_state(self):
    # dump additional data to the shelf
    shelf = self._open_shelf(dumb=True)
    if shelf != None:
      for account in self.accounts:
        account.store_to_shelf(shelf)
      shelf.close()
    # we now save data using dumbdbm, which does not use the file
    # directly, but makes several with different extensions;
    # we delete the old one in order to make sure the new one is used
    shelf_fname = self._shelf_filename()
    if os.path.exists(shelf_fname):
      os.remove(shelf_fname)
    # save config file
    config = ConfigParser.RawConfigParser()
    # version info
    config.add_section("version")
    config.set("version", 'config_format', self.CONFIG_FORMAT_VERSION)
    config.set("version", 'app_id', self._app_id)
    # call individual saving methods
    self._save_credentials(config)
    self._save_gui_state(config)
    self._save_connection_state(config)
    ConfigManager.save_to_config(config)
    # write it out and chmod to user-only access
    cr_filename = self._get_config_filename()
    cr_file = file(cr_filename, "wb")
    config.write(cr_file)
    cr_file.close()
    import stat
    os.chmod(cr_filename, stat.S_IREAD|stat.S_IWRITE)

  def open_account(self, account):
    """called when the an account is selected from the account list"""
    if self.account and self.account == account:
      pass
    else:
      if self.account and self.account.messages:
        self.account.messages.close()
      self.message_store.clear()
      self.received_attachment_store.clear()
      self.received_message_textview.get_buffer().set_text("")
      self.builder.get_object("account_info_textbuffer").set_text("")
      self.builder.get_object("download_complete_message").set_sensitive(False)
      self.destroy_current_ds_client()
      self.account = account
      # check old account type
      try:
        shelf = self._open_shelf()
      except Exception, e:
        self._statusbar_message(
                _("Additional account data could not be read from storage."))
      else:
        if shelf != None:
          if self.account.shelf_key() in shelf and \
              "received_messages" in shelf[self.account.shelf_key()]:
                # shelf must be closed when we move data
                self._check_old_account_type(shelf)
          shelf.close()
      # end of check
      ok = False
      while not ok:
        try:
          self.account.messages.open()
        except Exception, e:
          d = gtk.MessageDialog(self.window1,
                                buttons=gtk.BUTTONS_YES_NO,
                                type=gtk.MESSAGE_ERROR)
          d.set_transient_for(self.window1)
          d.set_markup(_("Could not open database.\n\nDatabase file '%s' is\
 missing or corrupted.") % self.account.db_filename())
          d.format_secondary_markup(_("Should I try to create an empty one?"))
          answer = d.run()
          d.destroy()
          if answer == gtk.RESPONSE_YES:
            # create the directory if possible
            dname = os.path.normpath(self.account.database_dir)
            to_mkdir = []
            while dname:
              if os.path.isdir(dname):
                break
              dname, cur = os.path.split(dname)
              to_mkdir.append(cur)
            while dname != os.path.normpath(self.account.database_dir):
              dname = os.path.join(dname, to_mkdir.pop(-1))
              try:
                os.mkdir(dname)
              except:
                break
            # try to create the file
            try:
              f = file(self.account.db_filename(), "w")
              f.close()
            except:
              pass
          else:
            self.account.use_memory_db = True
            self.account.open_message_storage()
            self._show_error(_("Database stored in memory will be used instead. \
This storage is not persistent between Datovka sessions!"))
        else:
          ok = True 
      if not ConfigManager.STORE_MESSAGES_ON_DISK:
        self._statusbar_message(_("Persistent storage of messages is turned off - \
messages will have to be downloaded each time, even after account switching!"))


  def load_messages_for_current_account(self, message_type, select_type,
                                        year=None):
    """typ is the type of messages to load.
    At present it is limitted to RECENT and ARCHIVE,
    year is used in conjuntion with ARCHIVE
    """
    # load data for the treeviews in a separate thread
    t = time.time()
    treeview = self._get_active_message_treeview()
    if self.account.messages:
      # async access is not possible with in-memory database
      async = (not self.account.use_memory_db)
      if async:
        parent = self.window1
        GUI.gui_lock.acquire()
        from soap_thread import SoapThread
        thread = SoapThread(self._populate_data_from_account,
                            (message_type, select_type, year))
        self.progressbar.pulse()
        self.progressbar.set_text(_("Loading message data"))
        parent.set_sensitive(False)
        thread.start()
        while thread.isAlive():
          gobject.idle_add(GUI.progressbar.pulse)
          time.sleep(0.1)
          while gtk.events_pending():
            gtk.main_iteration(block=False)
        # thread ended
        self.progressbar.set_fraction(0.0)
        self.progressbar.set_text(_("Idle"))
        parent.set_sensitive(True)
        GUI.gui_lock.release()
        # populate the treeview with loaded data
        rows = thread.reply
        if thread.exception:
          self._show_error(_("Could not load data from the database"),
                           thread.exception)
          rows = []
      else:
        rows = self._populate_data_from_account(message_type, select_type, year)
      #print "POPULATE", time.time() - t
      new_store = gtk.ListStore(*self.MESSAGE_STORE_DEF)
      [new_store.append(data) for data in rows]
      self.message_store.clear()
      self._connect_message_stores(new_store, treeview=treeview)
      #print "APPEND", time.time() - t
      self.progressbar.set_fraction(0.0)
      self.progressbar.set_text(_("Idle"))
      self.window1.set_sensitive(True)
    self.update_account_actions()
    self._check_empty_message_stores()
    # resize treeview columns to optimal size
    if treeview:
      treeview.columns_autosize()
    # scrolling to newest message is done after previous events were processed
    # in order to let the treeview adjust itself
    # otherwise discrepances may occur
    # explicitly dealing with the events seems cleaner than using a timeout
    # with arbitrary delay
    #print "BEFORE PENDING", time.time() - t
    while gtk.events_pending():
      gtk.main_iteration(block=False)
    #print "TOTAL", time.time() - t
    if ConfigManager.AFTER_START_SELECT == ConfigManager.SELECT_NEWEST:
      self._scroll_to_newest_message()
    elif ConfigManager.AFTER_START_SELECT == ConfigManager.SELECT_LAST_VISITED:
      self._scroll_to_last_visited_message()
    else:
      pass
      
   
  def _check_old_account_type(self, shelf):
    from dslib.models import Message, dmFile, dmHash
    def _update_progress():
      GUI.progressbar.pulse()
      while gtk.events_pending():
        gtk.main_iteration(block=False)
        
    # potentially create the home directory
    self.get_dsgui_private_dir(True)
    fname = self.account.db_filename()
    if not os.path.exists(fname):
      # no new style database yet and shelf data exist
      message = _("Since version 0.5, Datovka uses a different method of \
message storage.\n\n\
Your data will be automatically converted to the new format, but information about\
 signing certificate for signed messages cannot be retained.\n\nBecause of this,\
 signed messages will show info about invalid signing certificate. When possible,\
 download these messages again, to obtain the full signature information.")
      d = gtk.MessageDialog(self.window1,
                            message_format=message,
                            buttons=gtk.BUTTONS_OK,
                            type=gtk.MESSAGE_INFO)
      d.run()
      d.destroy()
      GUI.progressbar.set_text(_("Migrating old messages..."))
      self.window1.set_sensitive(False)
      _update_progress()
      shelf_data = shelf[self.account.shelf_key()]
      received_messages = []
      if "received_messages" in shelf_data:
        # received messages in the shelf - is seems we need to update
        # load it at first
        for dmID, message in shelf_data['received_messages'].iteritems():
          if message.dmFiles:
            fs = []
            for f in message.dmFiles:
              fs.append(f.__dict__)
            message.dmFiles = fs
          if message.dmHash:
            message.dmHash = message.dmHash.__dict__
          m = message.__dict__
          received_messages.append(m)
      sent_messages = []
      _update_progress()
      if "sent_messages" in shelf_data:
        # sent messages in the shelf - is seems we need to update
        # load it at first
        for dmID, message in shelf_data['sent_messages'].iteritems():
          if message.dmHash:
            message.dmHash = message.dmHash.__dict__
          m = message.__dict__
          sent_messages.append(m)
      try:
        self.account.messages.open()
      except Exception, e:
        self._show_error(_("Could not open database.\n\nDatabase file '%s' is\
 missing or corrupted.") % self.account.db_filename())
        self.account.messages = None
        return
      for typ,messages in ((self.account.messages.MESSAGE_TYPE_RECEIVED,
                            received_messages),
                            (self.account.messages.MESSAGE_TYPE_SENT,
                             sent_messages)):
        for message in messages:
          _update_progress()
          m = Message()
          for k,v in message.iteritems():
            if k == 'dmFiles':
              fs = []
              for fd in v:
                f = dmFile()
                for _k2, _v2 in fd.items():
                  setattr(f, _k2, _v2)
                fs.append(f)
              m.dmFiles = fs
            elif k == "dmHash" and v:
              h = dmHash()
              for _k, _v in v.iteritems():
                setattr(h, _k, _v)
              m.dmHash = h
            else:
              setattr(m, k, v)
          try:
            read = shelf_data['message_data'][int(m.dmID)]['read_locally']
          except:
            read = False
          supp_data = {'read_locally': read,
                       'download_date': datetime.datetime.now(),
                       'message_type': typ}
          self.account.messages.add_message(m, supp_data=supp_data)
      GUI.progressbar.set_text(_("Idle"))
      self.window1.set_sensitive(False)
      self.account.messages.close()


  def _calculate_message_date_limits(self, select_type, year=None):
    if select_type == self.SELECT_TYPE_ALL:
      if not year:
        return (0, 0)
      else:
        start = datetime.datetime(year, 1, 1)
        end = datetime.datetime(year+1, 1, 1)
        return (start, end)
    # recent
    now = datetime.datetime.now()
    time_threshold = now - datetime.timedelta(days=90)
    return (time_threshold, None)


  def _populate_data_from_account(self, message_type, select_type, year=None):
    """fill the gui treeview and other widgets with data from current account,
    typ is the type of messages to load"""
    rows = []
    start, end = self._calculate_message_date_limits(select_type, year=year)
    kwargs = dict(add_pkcs7_data=False)
    if select_type == self.SELECT_TYPE_ALL:
      if not year:
        messages = []
      else:
        messages = self.account.messages.get_messages_between_dates(start, end,
                                            message_type=message_type, **kwargs)
    else:
      messages = self.account.messages.get_messages_between_dates(start, end,
                                            message_type=message_type, **kwargs)
      messages += self.account.messages.get_messages_without_date(
                                            message_type=message_type, **kwargs)
    if message_type == self.account.messages.MESSAGE_TYPE_RECEIVED:
      typ = 'received'
    else:
      typ = 'sent'
    for message in messages:
      data = self._message_to_row(message, typ)
      rows.append(data)
    return rows


  def _open_known_account(self, username):
    for i, row in enumerate(self.account_store):
      if row[0] == username:
        account = self._get_account_object_for_account_row(row)
        self.open_account(account)
        self.message_store.clear()
        #self.load_messages_for_current_account(row[4])
        self.account_treeview.get_selection().select_path((i,0))
        break

  def _open_shelf(self, dumb=False):
    """if dumb is given, custom DumbShelf is used, which is completely
    platform independent"""
    if ConfigManager.STORE_ADDITIONAL_DATA_ON_DISK:
      fname = self._shelf_filename()
      if not dumb:
        import shelve
        shelve_module = shelve
      else:
        import dumb_shelf
        shelve_module = dumb_shelf
      try:
        shelf = shelve_module.open(fname, protocol=-1)
      except Exception, e:
        # the shelf seems to be corrupt - lets do some damage control
        # at first try to delete the old files
        for ext in ("",".dat",".bak",".dir"):
          if os.path.exists(fname+ext):
            try:
              os.remove(fname+ext)
            except Exception, e:
              pass
        # not retry to open the shelf
        try:
          shelf = shelve_module.open(fname, protocol=-1)
        except Exception, e:
          # ok, it did not help, there is nothing much to do
          return None
    else:
      if not hasattr(self, "_shelf"):
        class PseudoShelf(dict):
          def close(self):
            pass
          def sync(self):
            pass
        self._shelf = PseudoShelf()
      shelf = self._shelf
    return shelf

  def _shelf_filename(self):
    fname = os.path.join(GUI.get_dsgui_private_dir(create=True),
                         "messages.shelf")
    #if in_filesystem_encoding:
    #  encoding = sys.getfilesystemencoding()
    #  if encoding:
    #    fname = fname.encode(encoding)
    return fname

  def _show_error(self, message, exception=None):
    if exception:
      from xml.sax.saxutils import escape
      errw = ErrorWrapper(exception)
      detail = _("Error description: %s") % escape(unicode(errw))
      message += "\n\n<b>"+_("Details:")+"</b>\n"+ detail
      help_message = errw.get_help()
      if isinstance(exception, DSServerCertificateException):
        help_message = _("This error is a sign of improper installation.\n"
                         "Because a missing or invalid server certificate might"
                         " lead to several types of attacks, Datovka will"
                         " refuse to work until this problem is fixed.\n"
                         "You might try reinstalling Datovka or contacting the"
                         " developers for help.")
      if help_message:
        message += "\n\n<b>" + _("Hint:") + "</b>\n" + help_message
    d = gtk.MessageDialog(self.window1,
                          buttons=gtk.BUTTONS_OK,
                          type=gtk.MESSAGE_ERROR)
    if self.account:
        d.set_title(_("Error - account %s") % self.account.name)
    else:
        d.set_title(_("Error"))
    d.set_markup(message.encode("utf-8"))
    d.set_transient_for(self.window1)
    d.run()
    d.destroy()

  def _handle_authorization_error(self, e, message_header=None):
    """convenience method to display information about failed authorization;
    returns True if the exception was handled, False otherwise"""
    _m_untrust_cert = _("""It was not possible to verify the certificate of\
 the server.
It would not be safe to submit your credentials to an untrusted server, so\
 Datovka closed the connection.

It might be possible to fix this problem by adding a trusted certificate in\
 PEM format to the file dslib/trusted_certificates/all_trusted.pem inside\
 the Datovka directory that would enable validation of the server certificate.
""")
    #raise e
    message = ""
    # DSNotAuthorizedException instances
    if isinstance(e, DSNotAuthorizedException):
      message = '<b>' +_("Authorization failed!") + '</b>'
      message += "\n\n" + _("<i>Error type</i>: %s") % e.code 
      message += "\n" + _("<i>Error detail</i>: %s") % e.text
    # an additional check on the servers identity failed
    elif type(e.args) in (tuple,list) and e.args and \
     e.args[0] == "Server certificate did not pass security check.":
      cert = e.args[1]
      org_name = ""
      for parts in cert['subject']:
        # somehow 1-member tuples are used so we cycle again...
        for name, value in parts:
          if name == "organizationName":
            org_name = value
            break
      message = '<b>' + _("Server certificate verification failed!") + '</b>'
      message += "\n\n" + _("""The certificate of the server did not pass a\
 security check.
This check requires the certificate of the server to be issued for the Home\
 Department of CR.""")
      if org_name:
        message += "\n\n" + _("Server certificate was issued for:")
        message += "\n<i>%s</i>" % org_name 
    # normal authorization failure
    elif type(e.args) in (tuple,list) and \
     len(e.args) > 0 and type(e.args[0]) == tuple and len(e.args[0]) > 0:
      if e.args[0][0] == 401:
        # Unauthorized
        message = '<b>' +_("Authorization failed!") + '</b>'
        message += "\n\n" + _(u"""Please check your credentials including the\
 test-environment setting.
It is possible that your password has expired - in this case, you need to use\
 the official web interface of Datové schránky to change it.""")
    # the following happens when I supply non-existing filename
    # as server_certs in client
    elif type(e.args) in (tuple,list) and len(e.args) > 0 and \
       hasattr(e.args[0], 'args') and type(e.args[0].args) == tuple and \
       len(e.args[0].args) > 1 and \
       e.args[0].args[1].find("x509 certificate routines") >= 0:
          message = '<b>'+ _("Server certificate verification failed!") +'</b>'
          message += "\n\n" + _m_untrust_cert
    # this happens when the certificate in server_certs cannot be used
    # to validate the server
    elif type(e.args) in (tuple,list) and len(e.args) > 0 and \
       hasattr(e.args[0], 'args') and type(e.args[0].args) == tuple and \
       len(e.args[0].args) > 1 and \
       e.args[0].args[1].find("certificate verify failed") >= 0:
          message = '<b>'+ _("Server certificate verification failed!") +'</b>'
          message += "\n\n" + _m_untrust_cert
    # this is the same but with PyOpenSSL
    elif type(e.args) in (tuple,list) and len(e.args) > 0 and \
       type(e.args[0]) in (tuple,list) and len(e.args[0]) > 0 and \
       type(e.args[0][0]) in (tuple,list) and len(e.args[0][0]) > 2 and \
       e.args[0][0][2].find("certificate verify failed") >= 0:
          message = '<b>'+ _("Server certificate verification failed!") +'</b>'
          message += "\n\n" + _m_untrust_cert
    if message:
      head = message_header and message_header+"\n\n" or ""
      self._show_error(head + message)
      return True
    return False
  
  def _save_credentials(self, config):
    """save current credentials to a file"""
    for i, account in enumerate(self.accounts):
      cr = account.credentials
      num = ((i>0) and str(i+1) or "")
      section = "credentials"+num
      config.add_section(section)
      config.set(section, 'name', account.name)
      config.set(section, 'username', cr.username)
      config.set(section, 'login_method', cr.login_method)
      if cr.login_method in ('username','user_certificate',"hotp","totp"):
        if cr.remember_password and cr.password:
          config.set(section, 'password', base64.b64encode(cr.password))
      if cr.login_method in ('certificate','user_certificate'):
        if cr.p12file:
          config.set(section, 'p12file', cr.p12file.encode('utf-8'))
      config.set(section, 'test_account', cr.test_account)
      config.set(section, 'remember_password', cr.remember_password)
      # only save database_dir if it is outside the default place
      app_pref_dir = self.get_dsgui_private_dir()
      if os.path.normpath(app_pref_dir) != os.path.normpath(account.database_dir):
        config.set(section, 'database_dir', account.database_dir.encode('utf-8'))
      # sync with all
      config.set(section, 'sync_with_all', account.sync_with_all)
      # last message id
      if ConfigManager.AFTER_START_SELECT == ConfigManager.SELECT_LAST_VISITED:
        if account.last_message_id:
          config.set(section, 'last_message_id', account.last_message_id)
    current = self._get_selected_account_row()
    if current:
      config.add_section("default_account")
      config.set("default_account", "username", current[0])

  def _save_gui_state(self, config):
    config.add_section("message_ordering")
    # column ordering
    rec_col, rec_ord = self.toplevel_message_store.get_sort_column_id()
    if rec_col is not None:
      config.set('message_ordering', 'sort_column', rec_col)
      if rec_ord != None:
        rec_ord = rec_ord.value_name[4:]
        config.set('message_ordering', 'sort_order', rec_ord)
    # column widths
    config.add_section("column_widths")
    for tree_name in ("received", "sent"):
      tree_view = getattr(self, tree_name+"_message_treeview")
      for i in (1,2):
        # resizable columns
        col = tree_view.get_column(i)
        width = col.get_width()
        if not width:
          width = col.get_fixed_width()
        config.set("column_widths", "%s_%d"%(tree_name, i), width)
    # window position and size
    config.add_section("window_position")
    if self._position and self._size:
      x, y = self._position
      w, h = self._size
      config.set("window_position", "x", x)
      config.set("window_position", "y", y)
      config.set("window_position", "w", w)
      config.set("window_position", "h", h)
    # save last used directories
    config.add_section("last_directories")
    for key, val in self._last_dirs.iteritems():
      config.set("last_directories", key, val.encode('utf-8'))
    # pane sizes
    config.add_section("panes")
    for name, _parent_ratio in self.PANES:
      widget = self.builder.get_object(name)
      config.set("panes", name, widget.get_position())
    # account treeview expansions
    section = "account_tree"
    config.add_section(section)
    base = "acc_collapsed_"
    def walk_it(parent):
      if self.account_store.iter_has_child(parent):
        path = self.account_store.get_path(parent)
        path_text = base + "_".join([str(x) for x in path])
        exp = self.account_treeview.row_expanded(path)
        if exp:
          config.set(section, path_text, False)
          # if the parent is not collapsed, check its children
          child_iter = self.account_store.iter_children(parent)
          if child_iter:
            walk_it(child_iter)
        else:
          config.set(section, path_text, True)
      parent = self.account_store.iter_next(parent)
      if parent:
        walk_it(parent)
    iter = self.account_store.get_iter_first()
    if iter is not None:
      walk_it(iter)

  def _save_connection_state(self, config):
    config.add_section("connection")
    from dslib.network import ProxyManager
    # HTTPS proxy
    if self.https_proxy in (None, -1):
      config.set('connection', 'https_proxy', self.https_proxy)
    else:
      config.set('connection', 'https_proxy', ProxyManager.HTTPS_PROXY.hostname)
    if ProxyManager.HTTPS_PROXY.username:
      config.set('connection', 'https_proxy_username',
                 ProxyManager.HTTPS_PROXY.username)
    if ProxyManager.HTTPS_PROXY.password:
      config.set('connection', 'https_proxy_password',
                 base64.b64encode(ProxyManager.HTTPS_PROXY.password))
    # HTTP proxy
    if self.http_proxy in (None, -1):
      config.set('connection', 'http_proxy', self.http_proxy)
    else:
      config.set('connection', 'http_proxy', ProxyManager.HTTP_PROXY.hostname)
    if ProxyManager.HTTP_PROXY.username:
      config.set('connection', 'http_proxy_username',
                 ProxyManager.HTTP_PROXY.username)
    if ProxyManager.HTTP_PROXY.password:
      config.set('connection', 'http_proxy_password',
                 base64.b64encode(ProxyManager.HTTP_PROXY.password))
    

  def _load_gui_state(self, config):
    # message ordering
    if config.has_option("message_ordering", "sort_column"):
      rec_col = config.getint("message_ordering", "sort_column")
      if rec_col in (4,5):
        rec_col += 10 # 4 and 5 are now locale dependent, 14, 15 are for sorting
      rec_ord = gtk.SORT_ASCENDING
      if config.has_option("message_ordering", "sort_order"):
        try:
          rec_ord = getattr(gtk, config.get("message_ordering", "sort_order"))
        except:
          pass
      self.toplevel_message_store.set_sort_column_id(rec_col, rec_ord)
    # column widths
    if config.has_section("column_widths"):
      for tree_name in ("received", "sent"):
        tree_view = getattr(self, tree_name+"_message_treeview")
        for i in (1,2):
          if config.has_option("column_widths", "%s_%d"%(tree_name, i)):
            width = config.getint("column_widths", "%s_%d"%(tree_name, i))
            if width > 0:
              tree_view.get_column(i).set_fixed_width(width)
    # window size and position
    x, y, w, h = None, None, None, None
    if config.has_option("window_position", "x"):
      x = config.getint("window_position", "x")
    if config.has_option("window_position", "y"):
      y = config.getint("window_position", "y")
    if config.has_option("window_position", "w"):
      w = config.getint("window_position", "w")
    if config.has_option("window_position", "h"):
      h = config.getint("window_position", "h")
    if x is not None and y is not None and w is not None and h is not None:
      sw = self.window1.get_screen().get_width()
      sh = self.window1.get_screen().get_height()
      if 0 < x < sw and 0 < y < sh:
        self.window1.move(x, y)
      else:
        self.window1.move(0, 0)
      self.window1.resize(w, h)
    # load last used directories
    if config.has_section("last_directories"):
        for name, val in config.items("last_directories"):
            self._set_topical_last_dir(val, name=name)
    # account treeview expansions
    section = "account_tree"
    base = "acc_collapsed_"
    def walk_it(parent):
      if self.account_store.iter_has_child(parent):
        path = self.account_store.get_path(parent)
        path_text = base + "_".join([str(x) for x in path])
        if config.has_option(section, path_text):
          exp = not config.getboolean(section, path_text)
        else:
          exp = (len(path) <= 1) # top level is by default expanded, rest not
        if exp:
          self.account_treeview.expand_row(path, False)
          # if the parent is not collapsed, check its children
          child_iter = self.account_store.iter_children(parent)
          if child_iter:
            walk_it(child_iter)
        else:
          self.account_treeview.collapse_row(path)
      parent = self.account_store.iter_next(parent)
      if parent:
        walk_it(parent)
    iter = self.account_store.get_iter_first()
    if iter is not None:
      walk_it(iter)
      
  def _load_gui_state_late(self, config):
    # message pane position
    for name, parent_rel in self.PANES:
      pos = None
      if config.has_section("panes"):
        if config.has_option("panes", name):
          pos = config.getint("panes", name)
      widget = self.builder.get_object(name)
      parent_alloc = widget.get_parent().get_allocation()
      if isinstance(widget, gtk.VPaned):
        parent_size = parent_alloc.height
      else:
        parent_size = parent_alloc.width
      max_position = widget.get_property('max_position')
      min_position = widget.get_property('min_position')
      if not pos or pos > max_position or pos < min_position:
        pos = int(parent_rel * parent_size)
      widget.set_position(pos)
      while gtk.events_pending():
        gtk.main_iteration(block=False)


  def _load_connection_state(self, config):
    if not self.proxy_support_ok:
      # this version of Python does not support HTTPS proxy anyway
      self.https_proxy = None
      self.http_proxy = None
      return
    if config.has_section("connection"):
      from dslib.network import ProxyManager
      # HTTPS Proxy
      if config.has_option('connection', 'https_proxy'):
        proxy = config.get('connection', 'https_proxy')
        if proxy == "-1":
          self.https_proxy = -1
        elif proxy == "None":
          self.https_proxy = None
        else:
          self.https_proxy = proxy
        ProxyManager.HTTPS_PROXY.set_uri(self.https_proxy)
      if config.has_option('connection', 'https_proxy_username'):
        ProxyManager.HTTPS_PROXY.username = config.get('connection',
                                                       'https_proxy_username')
      if config.has_option('connection', 'https_proxy_password'):
        ProxyManager.HTTPS_PROXY.password = \
              base64.b64decode(config.get('connection','https_proxy_password'))
      # HTTP Proxy
      if config.has_option('connection', 'http_proxy'):
        proxy = config.get('connection', 'http_proxy')
        if proxy == "-1":
          self.http_proxy = -1
        elif proxy == "None":
          self.http_proxy = None
        else:
          self.http_proxy = proxy
      else:
        # HTTP proxy was not set - this means an older version where there
        # was no distinction - use the HTTPS proxy settings
        self.http_proxy = self.https_proxy
      ProxyManager.HTTP_PROXY.set_uri(self.http_proxy)
      if config.has_option('connection', 'http_proxy_username'):
        ProxyManager.HTTP_PROXY.username = config.get('connection',
                                                       'http_proxy_username')
      if config.has_option('connection', 'http_proxy_password'):
        ProxyManager.HTTP_PROXY.password = \
              base64.b64decode(config.get('connection','http_proxy_password'))
            
  def _load_config_file(self):
    config = self.get_config_from_app_config_file()
    ConfigManager.load_from_config(config)
    Properties.CHECK_CRL = ConfigManager.CHECK_CRL
    # format version
    if config.has_option("version","config_format"):
      config_format_version = config.getint("version", "config_format")
    else:
      config_format_version = 0
    # app id
    if config.has_option("version","app_id"):
      self._app_id = config.get("version", "app_id")
    else:
      from hashlib import md5
      app_id = md5(str(time.time()))
      self._app_id = app_id.hexdigest()
    # load credentials
    i = 1
    section = "credentials" + ((i>1) and str(i) or "")
    while config.has_section(section):
      data = {}
      # some simple string data
      for keyname in ("username","p12file","password","login_method","name",
                      "database_dir"):
        if config.has_option(section, keyname):
          value = config.get(section, keyname)
        else:
          value = None
        data[keyname] = value
      # password
      if config_format_version > 0 and data.get('password'):
        # from version 1 up, password is stored in base64
        data['password'] = base64.b64decode(data['password'])
      # login method
      if data['login_method'] == None:
        data['login_method'] = 'username'
      # test account
      if config.has_option(section, "test_account"):
        data['test_account'] = config.getboolean(section, "test_account")
      else:
        data['test_account'] = True
      # remember password
      if config.has_option(section, "remember_password"):
        data['remember_password'] = config.getboolean(section,
                                                      "remember_password")
      elif config.has_option("preferences", "store_passwords_on_disk"):
        # we use the default value if it is available
        data['remember_password'] = config.getboolean("preferences",
                                                      "store_passwords_on_disk")
      else:
        data['remember_password'] = ConfigManager.STORE_PASSWORDS_ON_DISK
      # sync with all
      if config.has_option(section, "sync_with_all"):
        data['sync_with_all'] = config.getboolean(section, "sync_with_all")
      else:
        data['sync_with_all'] = True
      # last visited message
      if config.has_option(section, "last_message_id"):
        data['last_message_id'] = config.getint(section, "last_message_id")
      # the portable version of Datovka is now graphically distinguishable
      # so it might not be necessary to impose the following limitation
      # and I am disabling it experimentally
      
      ## we are in portable mode - we do not want to store password in any case
      ## as it could lead to an attack of the following nature:
      ## 1/ black hat tricks the user to using portable version
      ## 2/ user uses Datovka and stores password in non-default,
      ##    unexpected location
      ## 3/ black hat retreives the password from the stored config file 
      #if self.portable:
      #  data['remember_password'] = False
      #  if 'password' in data:
      #      del data['password']
      # // end of experimental password storage disabling
      # database directory
      if data['database_dir'] == None:
        data['database_dir'] = self.get_dsgui_private_dir()
      # in older versions account name was not used
      if data['name'] == None:
        data['name'] = data['username']
      self.add_account(**data)
      i += 1
      section = "credentials" + ((i>1) and str(i) or "")
    self._load_gui_state(config)
    self._load_connection_state(config)
    # check passwords
    old_account = self.account
    for account in self.accounts:
      self.account = account
      # we are short-wiring the account changing mechanism, so we need to
      # take case of old ds clients
      self.destroy_current_ds_client()
      self.check_expired_password(self.account) # only self.account works
    self.account = old_account
    # then open the default account or the first one 
    if config.has_option("default_account", "username"):
      username = config.get("default_account", "username")
      self._open_known_account(username)
    elif self.accounts:
      self.open_account(self.accounts[0])
      self.message_store.clear()
      self.account_treeview.get_selection().select_path((0,0,0))
    # scroll to current selection
    model, paths = self.account_treeview.get_selection().get_selected_rows()
    if len(paths) == 1:
      self.account_treeview.scroll_to_cell(paths[0],
                                           use_align=True, row_align=0.5)
    self._load_gui_state_late(config)

  @classmethod
  def _get_config_filename(cls):
    return os.path.join(GUI.get_dsgui_private_dir(create=True), "dsgui.conf")
  
  @classmethod
  def get_config_from_app_config_file(cls):
    cr_filename = cls._get_config_filename()
    # we read the file through utf-8 decoder because otherwise the parser
    # will strip chr(160)(nbsp) from the end of lines even though it is
    # part of legitimate unicode characters
    config = ConfigParser.RawConfigParser()
    if os.path.isfile(cr_filename):
      try:
        fp = codecs.open(cr_filename, "r", "utf-8")
        config.readfp(fp)
        fp.close()
      except IOError:
        pass
    return config

  
  def _check_empty_message_stores(self):
    """check if the store of received or sent messages is empty
    and warn if need be.
    """
    # it is not needed anymore and it does not look good
    return


  def _activate_tooltips_for_main_menu(self):
    def _X_(w):
      if w.get_action():
        tooltip = w.get_action().get_property("tooltip")
        if tooltip:
          self._statusbar_message(tooltip)
    def _Y_(w):
      self.statusbar.pop(-1)
    def _act_one(x):
      for child in x:
        child.connect("select", _X_)
        child.connect("deselect", _Y_)
        submenu = child.get_submenu()
        if submenu:
          _act_one(submenu)
    menu = self.main_menu
    _act_one(menu)
    
  def _statusbar_message(self, message):
    self.statusbar.push(-1, message)
    self._last_statusbar_push = time.time()
    
  def _post_builder_ui_compatibility_fixes(self, gui_xml):
    if gtk.gtk_version < (2,16,0):
      # in 2.14 menu is not properly loaded from XML
      import xml.dom.minidom as dom
      doc = dom.parseString(gui_xml)
      from ui_xml_versions import manually_create_menu, bind_nonmenu_action_references
      menu = manually_create_menu(doc, "main_menu", self, self.builder)
      bind_nonmenu_action_references(doc, self.builder)
      old_menu = self.builder.get_object("main_menu")
      if old_menu:
        old_menu.destroy()
      self.builder.get_object("vbox1").pack_start(menu, False, False)
      self.builder.get_object("vbox1").reorder_child(menu, 0)
      menu.show_all()
    else:
      menu = self.builder.get_object("main_menu")
    self.main_menu = menu
    # now try hildon support (Maemo)
    try:
      import hildon
    except:
      pass
    else:
      self.app = hildon.Program()
      hildon_window = hildon.Window()
      self.app.add_window(hildon_window)
      self.builder.get_object("vbox1").reparent(hildon_window)
      hildon_window.set_title("Datovka")
      self.window1.destroy()
      new_menu = gtk.Menu()
      for child in menu.get_children():
        child.reparent(new_menu)
      hildon_window.set_menu(new_menu)
      menu.destroy()
      self.builder.get_object("toolbar1").destroy()
      self.builder.get_object("warning_label").destroy()
      hildon_window.connect("destroy", self.on_window1_destroy)
      self.window1 = hildon_window
    
  def try_client_method(self, method_name, args, error_message=None, handle_status=True,
                        parent=None, async=True, show_exception_details=True,
                        ignore_errors=False, kwargs=None):
    """convenience function to run a method of self.client and display
    error messages, etc. as appropriate.
    Returns a tuple (reply, error_shown), where reply is the reply of the method and
    error_shown is a boolean telling if an error message was displayed.
    If parent is given then it is disabled while the soap thread is running instead of the main window
    """
    # check if client exists
    if not self.client:
      self.client = self.create_ds_client()
      if not self.client:
        self._statusbar_message(_("Action was cancelled"))
        return self.SoapReply(None, canceled=True) # creation of dsclient was canceled
    if not error_message:
      error_message = _("An error occured during the following operation: %s") % method_name
    # init some variables used throughout the method
    exception = None
    reply = None
    canceled = False
    # try to get the method - may return error when client cannot be inited
    try:
      method = getattr(self.client, method_name)
    except Exception as e:
      exception = e
    else:
      # here we run the method if everything went OK
      parent = parent or self.window1
      self.__class__._soap_async = async  # we use this in callback methods
      if async:
        # asynchronous processing - we start a separate thread
        GUI.gui_lock.acquire()
        from soap_thread import SoapThread
        thread = SoapThread(method, args, kwargs)
        self.progressbar.pulse()
        self.progressbar.set_text(method_name)
        was_sensitive = parent.get_property("sensitive")
        parent.set_sensitive(False)
        thread.start()
        while thread.isAlive():
          # clearing _can_update_gui effectively blocks this until set again
          gobject.idle_add(GUI.progressbar.pulse)
          time.sleep(0.1)
          while gtk.events_pending():
            gtk.main_iteration(block=False)
          if not self._otp_request_finished.is_set():
            self._otp_value = self._process_otp_request()
            self._otp_request_finished.set()
        self.progressbar.set_fraction(0.0)
        self.progressbar.set_text(_("Idle"))
        if was_sensitive:
          parent.set_sensitive(True)
        GUI.gui_lock.release()
        exception = thread.exception
        reply = thread.reply
        canceled = thread.canceled
      else:
        # non-async - for some reason we do not want it
        kwargs = kwargs or dict()
        try:
          reply = method(*args, **kwargs)
        except DSOTPException, e:
          exception = e
          if e.code == DSOTPException.OTP_CANCELED_BY_USER:
            canceled = True
        except Exception, e:
          exception = e
        else:
          exception = None
    # OK, the method was run and we should process the result
    # see what we got back
    if canceled:
      return self.SoapReply(None, canceled=True)
    if exception:
      if ignore_errors:
        return self.SoapReply(None, error=exception, error_shown=False)
      if not self._handle_authorization_error(exception, message_header=error_message):
        if not show_exception_details:
          self._show_error(error_message, exception=None)
        else:
          self._show_error(error_message, exception=exception)
      return self.SoapReply(None, error=exception, error_shown=True)
    else:
      error_shown = False
      if handle_status:
        code, message = GUI._extract_code_and_message_from_reply(reply)
        if code != "0000":
          error_message += "\n\n"
          error_message += _("Reply status: %(message)s\nReply code: %(code)s")%\
                           {"message":message,"code":code}
          self._show_error(error_message)
          error_shown = True
          exception = code
        #self.client.logout_from_server()
      return self.SoapReply(reply, error=exception, error_shown=error_shown)
    
  def create_ds_client(self):
    """creates a working client, it asks for password when necessary;
    """
    cr = self.account.credentials
    if (cr.login_method in ("username","user_certificate","hotp","totp") 
        and not cr.password)\
       or \
       (cr.login_method in ("certificate","user_certificate") and  
            (not cr.p12file or not Client.CERT_LOGIN_AVAILABLE)):
      # ask for password
      d = CredentialsDialog(self, name=self.account.name, credentials=cr,
                            only_password=True,
                            sync_with_all=self.account.sync_with_all)
      result = d.run()
      if result == 1:
        data = d.get_data()
        # the change_data method does not use the following values
        del data['username']
        del data['name']
        del data['sync_with_all']
        self.account.credentials.change_data(**data)
        d.destroy()
      else:
        d.destroy()
        return None
    if cr.login_method in ("certificate", "user_certificate") and \
       cr.p12file and Client.CERT_LOGIN_AVAILABLE and not cr.keyobj:
      # we need to decode the p12file and we need a password to do that
      go_on = True
      while go_on:
        ok, password = ask_password(self.window1,
                                    _("Password required"),
                                    _("Please enter a password to\
 unlock your PKCS12 certificate file:"))
        if ok:
          try:
            cr.decode_p12file(password)
            go_on = False
          except Credentials.WrongPasswordError:
            self._show_error(_("Wrong password! Try it again."))
          except Exception, e:
            self._show_error(_("""Problem decoding PKCS12 file '%s'!

Probably a corrupted or incompatible file.""") % cr.p12file, exception=e)
            return None
        else:
          return None
    return self._create_ds_client(self.account.credentials)
    
  def destroy_current_ds_client(self, save_auth_cookie=True):
    if self.client and self.account and self.account.credentials:
      if save_auth_cookie and self.client.requires_login():
        cookie_jar = self.client.get_cookie_jar(do_login=False)
        if cookie_jar:
          for cookie in cookie_jar:
            self.account.credentials.auth_cookies.set_cookie(cookie)
    self.client = None
    
  @classmethod
  def get_dsgui_private_dir(cls, create=False):
    if cls.portable:
        homedir = cls.topdir
    else:
        homedir = os_support.get_home_app_dir(cls.topdir)
    priv_dir = os.path.join(homedir, ".dsgui")
    if create and not os.path.exists(priv_dir):
      os.mkdir(priv_dir)
    return priv_dir
  
  @classmethod
  def _get_account_type_as_text(self, test_account):
    account_type = test_account and _("Test account") or _("Standard account")
    return account_type
      
  @classmethod
  def _extract_code_and_message_from_reply(cls, reply):
    if not reply:
      return None, None
    if not hasattr(reply, 'status'):
      return None, None
    elif hasattr(reply.status, 'dmStatusCode'):
      return reply.status.dmStatusCode, reply.status.dmStatusMessage
    elif hasattr(reply.status, 'dbStatusCode'):
      return reply.status.dbStatusCode, reply.status.dbStatusMessage
    return None, None


  def _create_ds_client(self, credentials, dummy=False):
    """by setting dummy to True a client will be created without any password
    or ceritificate. It could be used for things that do not access the
    server, but still need a client. Such a client *must* be disposed of
    after its use, not to break any consequent uses."""
    if dummy:
      # dummy client
      if credentials:
        test_account = credentials.test_account
      else:
        test_account = False
      return Client(login="aaaaaa", 
                    password="asasasasa",
                    login_method="username",
                    test_environment=test_account,
                    )
    # normal client
    if credentials.login_method in ("hotp","totp"):
      otp_callback = self._otp_callback
    else:
      otp_callback = None
    ret = Client(login=credentials.username, 
                  password=credentials.password,
                  login_method=credentials.login_method,
                  client_keyobj=credentials.keyobj,
                  client_certobj=credentials.certobj, 
                  test_environment=credentials.test_account,
                  server_certs=os.path.join(self.trusted_certificate_dir,
                                            "all_trusted.pem"),
                  otp_callback=otp_callback,
                  )
    if ret.requires_login() and len(credentials.auth_cookies):
      for cookie in credentials.auth_cookies:
        ret.set_auth_cookie(cookie)
    return ret

  def _process_otp_request(self):
    from otp_dialog import OTPDialog
    d = OTPDialog(self, last_problem=self._last_otp_problem)
    answer = d.run()
    if answer == 1:
      otp = d.get_otp()
      d.destroy()
      return otp
    else:
      d.destroy()
      return None
  
  def _otp_callback(self, last_problem=None):
    """Unfortunately the thread that runs the SOAP query is not the one
    that runs the GTK and race conditions can occur. Becuase of this, we
    use an indirect approach in which this method just sets a request
    for the main thread to obtain the OTP and waits for the result"""
    if self.__class__._soap_async:
      self._otp_request_finished.clear()
      self._last_otp_problem = last_problem
      self._otp_request_finished.wait()
      val = self._otp_value
      self._otp_value = None
      self._last_otp_problem = None
      return val
    else:
      return self._process_otp_request()

  @classmethod
  def fix_ui_xml(cls, filename):
    """convenience function wrapping several one-purpose methods"""
    ui_dir = local.find_data_directory("ui")
    filename = os.path.join(ui_dir, filename)
    gui_xml = cls.compatibilize_ui_xml(filename).encode('utf-8')
    gui_xml = cls.translate_ui_xml(gui_xml)
    return gui_xml
  
  @classmethod
  def translate_ui_xml(cls, gui_xml):
    gui_xml = translate_xml_string(gui_xml, _)
    return gui_xml

  @classmethod
  def compatibilize_ui_xml(cls, filename):
    from ui_xml_versions import compatibilize_xml_file
    xml_string = compatibilize_xml_file(os.path.join(cls.topdir,filename), gtk.gtk_version)
    return xml_string

  @classmethod
  def _add_custom_icons(cls):
    # custom icon support
    factory = gtk.IconFactory()
    for icon_base_name in ("ok", "error", "message-reply", "message-download",
                           "message-verify", "account-sync",
                           "all-accounts-sync"):
        icon_name = "datovka-" + icon_base_name
        pixbuf = IconManager.get_pixbuf_by_name(icon_name)
        if pixbuf:
            iconset = gtk.IconSet(pixbuf)
            factory.add(icon_name, iconset)
        else:
            print >> sys.stderr, "Could not find icon", icon_name
    factory.add_default()

  def _create_text_tags(self, tb):
    """tb is a TextBuffer"""
    try:
      tb.create_tag("bold", weight=pango.WEIGHT_BOLD)
    except:
      pass
    try:
      tb.create_tag("larger",
                    scale=1.25,
                    pixels_above_lines=10,
                    pixels_below_lines=10)
    except:
      pass
    try:
      tb.create_tag("italic", style=pango.STYLE_ITALIC)
    except:
      pass
    try:
      tb.create_tag("warning", foreground="#900")
    except:
      pass
    try:
      tb.create_tag("indent1", left_margin=30)
    except:
      pass
    try:
      tb.create_tag("indent2", left_margin=60, indent=-30)
    except:
      pass
    
  def _check_new_versions(self):    
    if ConfigManager.CHECK_NEW_VERSIONS:
      newest_version, info = version_check.check_version(release.DSGUI_VERSION,
                        self._app_id,
                        send_data=ConfigManager.SEND_STATS_WITH_VERSION_CHECKS)
      if newest_version:
        _message = _("There is a new version of Datovka available online: %s")%\
                     newest_version
        _second = ("<b>%s</b>" % _("Info:")) + "\n"
        _second += info + "\n"
        _second += _('<a href="http://labs.nic.cz/page/969/">Click here\
</a> for more info and download links.')
        d = gtk.MessageDialog(self.window1,
                              buttons=gtk.BUTTONS_OK,
                              type=gtk.MESSAGE_INFO)
        d.set_markup("<b>%s</b>" % _message)
        d.format_secondary_markup(_second)
        d.run()
        d.destroy()
        
        
  def _get_topical_last_dir(self, name=None):
    """this function obtains a directory name that was last associated with
    'name'. If name is not given, the calling function name is used"""
    if not name:
      name = inspect.stack()[1][3]
    return self._last_dirs.get(name, os_support.get_home_dir("./"))
  
  def _set_topical_last_dir(self, dir, name=None):
    """this function stores the dir directory name together with 'name';
    if no name is given, the calling function name is used"""
    if not name:
      name = inspect.stack()[1][3]
    self._last_dirs[name] = dir
      

      

def run():
  #logging.getLogger('pkcs7.rsa_verifier').setLevel(logging.CRITICAL)
  if os.path.basename(sys.argv[0]) == "datovka_portable.exe":
    GUI.portable = True
  if "-p" in sys.argv:
    GUI.portable = True
  if "-d" in sys.argv:
    # we are in debug mode
    import logging
    logname = os.path.join(GUI.get_dsgui_private_dir(True), "debug.log")
    mess = _("Debugging mode is active.")
    mess += "\n\n" + _("You can find debugging output in '%s'.")%logname
    d = gtk.MessageDialog(None,
                          message_format=mess,
                          buttons=gtk.BUTTONS_OK, type=gtk.MESSAGE_INFO)
    d.run()
    d.destroy()
    logging.basicConfig(filename=logname, level=logging.DEBUG)
    handler = logging.FileHandler(logname)
    handler.setLevel(logging.DEBUG)
    logging.getLogger('suds').addHandler(handler)
    logging.getLogger('suds').setLevel(level=logging.DEBUG)
  else:
    import logging
    logging.basicConfig(level=logging.CRITICAL)
      
  gobject.threads_init()
  args = sys.argv[1:]
  finish_immediately = False
  if "-n" in args:
    args.remove("-n")
    finish_immediately = True
  if "-d" in args:
    args.remove("-d")
  if "-p" in args:
    args.remove("-p")
  # deal with language
  config = GUI.get_config_from_app_config_file()
  ConfigManager.load_from_config(config)
  if ConfigManager.LANGUAGE and ConfigManager.LANGUAGE != "system":
    # user set language - override the system settings
    os.environ['LANG'] = ConfigManager.LANGUAGE
    os.environ['LANGUAGE'] = ConfigManager.LANGUAGE
    try:
      locale.setlocale(locale.LC_ALL)
    except:
      pass
    # search for translations
    tr = local.find_translation(lang=ConfigManager.LANGUAGE)
    tr.install(names=("ngettext",))
  if len(args) > 0:
    # user supplied a filename on the command line, we start only part the way
    # and leave the decision about later normal startup to the user
    app = GUI(local_startup=False)
    for infname in args:
      app.on_show_message_from_file_activate(None, fname=infname)
    if not finish_immediately:
      d = gtk.MessageDialog(app.window1,
                            buttons=gtk.BUTTONS_YES_NO,
                            type=gtk.MESSAGE_QUESTION)
      d.set_transient_for(app.window1)
      d.set_markup(_("Continue normal startup?"))
      d.format_secondary_markup(_("Answering 'No' would close the application."))
      answer = d.run()
      d.destroy()
      if answer == gtk.RESPONSE_YES:
        app._local_startup()
        gtk.main()
  else:
    app = GUI()
    gtk.main()

if __name__ == "__main__":
  run()
