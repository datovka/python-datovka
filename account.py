#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
class Credentials for storage of account related info resides here
"""

import datetime, os
from message_store import DictMessageStore, DsdbMessageStore
from cookielib import CookieJar
import sqlalchemy

class Credentials(object):
  """stores credentials to an account"""
  
  class WrongPasswordError(Exception):
    pass
  
  def __init__(self, username, password=None, test_account=False,
               p12file=None, login_method='username', remember_password=True):
    """data for both login methods can be saved at once to allow switching
    between the methods without forgetting some of the data"""
    self.username = username
    self.password = password
    self.test_account = test_account
    self.p12file = p12file
    self.login_method = login_method
    self.keyobj = None
    self.certobj = None
    self.remember_password = remember_password
    self.auth_cookies = CookieJar()
    
  def __str__(self):
    return "%s:%s" % (self.username, self.test_account) 
    
  def change_password(self, new_password):
    """returns true if password was really changed"""
    if new_password == self.password:
      return False
    else:
      self.password = new_password
      return True
    
  def get_id(self):
    return (self.username, self.test_account)
    
  def change_data(self, password=None, test_account=False,
                  p12file=None, login_method='username', remember_password=True):
    """return True if something 'important' has changed - that means something
    that would require soap client change; for example password change when
    using login method 'certificate' should not return True as the change does
    not influence the process of logging in"""
    # at first determine if we should report a change
    change = False
    if login_method != self.login_method:
      self.login_method = login_method
      change = True
    if self.login_method in ('hotp','totp','username'):
      if password != self.password:
        change = True
    elif self.login_method == 'certificate':
      if p12file != self.p12file:
        # we must invalidate the key and certificate data
        self.keyobj = None
        self.certobj = None
        change = True
    if test_account != self.test_account:
      change = True
    # now update all not yet updated values
    self.password = password
    self.test_account = test_account
    self.p12file = p12file
    self.remember_password = remember_password
    return change
  
  def decode_p12file(self, password):
    """parses self.p12file and sets self.keyobj and self.certobj"""
    import OpenSSL
    f = file(self.p12file, 'rb')
    p12text = f.read()
    f.close()
    try:
      p12obj = OpenSSL.crypto.load_pkcs12(p12text, password)
    except OpenSSL.crypto.Error, e:
      a = e.args
      if type(a) in (list,tuple) and type(a[0]) in (list,tuple) and \
        type(a[0][0]) in (list,tuple) and e.args[0][0][2] == 'mac verify failure':
        raise self.WrongPasswordError()
      else:
        raise e
    self.keyobj = p12obj.get_privatekey()
    self.certobj = p12obj.get_certificate()
    
  def should_logout(self):
    """returns True if it seems like we are logged in"""
    if len(self.auth_cookies) and self.login_method in ("hotp","totp"):
      return True
    return False 
    

class Account(object):
  """stores info about an account"""
  
  def __init__(self, name, credentials, dbdir, sync_with_all=True):
    self.name = name
    self.credentials = credentials
    self.account_info = None
    self.password_expiration_date = None
    self.database_dir = dbdir
    self.use_memory_db = False
    self.sync_with_all = sync_with_all
    self.messages = None
    self.last_message_id = None
    self.open_message_storage()
    
  def __str__(self):
    acc_type = self.credentials.test_account and "Test" or "Normal"
    return "Account '%s' - %s" % (self.credentials.username, acc_type)

  def open_message_storage(self):
    """if dbdir is None, in-memory database will be used"""
    if self.database_dir is None or self.use_memory_db:
      self.messages = DsdbMessageStore(None)
    else:
      self.messages = DsdbMessageStore(self.db_filename())
    
  def set_account_info(self, ai):
    self.account_info = ai
    
  def is_password_expiring(self, tolerance=5):
    """tolerance is in days or a datetime.timedelta"""
    if self.credentials.login_method == "certificate":
      return None
    if type(tolerance) == int:
      tolerance = datetime.timedelta(days=tolerance)
    if self.password_expiration_date:
      return self.password_expiration_date < datetime.datetime.now() + tolerance
    else:
      return None
    
  def has_password_expired(self):
    return self.is_password_expiring(0)
    
  def get_password_expiration_delta(self):
    return self.password_expiration_date - datetime.datetime.now()

  def store_to_shelf(self, shelf):
    key = self.shelf_key()
    shelf[key] = {"account_info":self.account_info,
                  "password_expiration_date":self.password_expiration_date,
                  }
    shelf.sync()
    
  def load_from_shelf(self, shelf):
    try:
        data = shelf.get(self.shelf_key(), None)
    except Exception as exc:
        pass
    else:
        if data:
          account_info = data.get("account_info", None)
          ped = data.get("password_expiration_date", None)
          if ped != self.password_expiration_date:
            self.password_expiration_date = ped
          self.set_account_info(account_info)

  def db_filename(self, dbdir=None):
    if self.database_dir == None:
      return None
    fname = self.shelf_key().replace("True","1").replace("False","0")
    return os.path.join((dbdir or self.database_dir), fname+".db")
      
  def shelf_key(self):
    """returns a key to be used for shelf based on current credentials"""
    return Account._create_shelf_key(self.credentials.username,
                                     self.credentials.test_account)
  
  @classmethod
  def _create_shelf_key(cls, username, test_account):
    """returns a key to be used for shelf based on current credentials"""
    ret = "%s___%s" % (username, test_account)
    if type(ret) == unicode:
        ret = ret.encode('utf-8')
    return ret

  def get_account_db_stats_lowlevel(self,
                          message_type=DsdbMessageStore.MESSAGE_TYPE_RECEIVED):
    def fetch(select, one=True):
      try:
        res = engine.execute(select)
      except:
        return None
      row = res.fetchone()
      if row: 
        if one:
          return row[0]
        else:
          return row
      else:
        return None
    def finalize():
      if engine_dispose:  
        # we created the engine by hand, so we dispose with it
        engine.dispose()
    
    result = {'all':{}}
    engine_dispose = False
    if self.messages and self.messages.database:
      engine = self.messages.database.engine
    else:
      db_filename = self.db_filename()
      engine_dispose = True
      if db_filename:
        try:
          engine = sqlalchemy.create_engine('sqlite:///%s' % db_filename,
                                            echo=False)
        except:
          return None
      else:
        return None
    now = datetime.datetime.now()
    time_threshold = now - datetime.timedelta(days=90)
    # find oldest message in the db
    select = """SELECT MIN(datetime(dmDeliveryTime)),
                       MAX(datetime(dmDeliveryTime)) FROM messages
                JOIN supplementary_message_data ON dmID = message_id
                WHERE message_type = %d AND dmDeliveryTime IS NOT NULL""" %\
                (message_type,)
    dates = fetch(select, one=False)
    if not dates:
      finalize()
      return None
    oldest_date, newest_date = dates
    if not oldest_date:
      finalize()
      return None
    oldest_date = datetime.datetime.strptime(oldest_date, "%Y-%m-%d %H:%M:%S")
    newest_date = datetime.datetime.strptime(newest_date, "%Y-%m-%d %H:%M:%S")
    oldest_year = oldest_date.year
    newest_year = newest_date.year
    # find unread counts for all matching messages
    for year in range(oldest_year, newest_year+1):
      start = datetime.datetime(year, 1, 1)
      end = datetime.datetime(year+1, 1, 1)
      select_temp = """SELECT count(*) FROM messages
        JOIN supplementary_message_data ON dmID = message_id
        WHERE %s message_type = %d AND 
          datetime(dmDeliveryTime) >= datetime('%s') AND
          datetime(dmDeliveryTime) < datetime('%s')"""
      all_count = fetch(select_temp % ("", message_type, start, end))
      if message_type == DsdbMessageStore.MESSAGE_TYPE_RECEIVED:
        unread_count = fetch(select_temp % ("read_locally = 0 AND",
                                            message_type, start, end))
      else:
        # for sent messages, it is always zero
        unread_count = 0                        
      if all_count is None:
        finalize()
        return None
      result['all'][year] = {"total": all_count, "unread": unread_count}
    # current messages
    select_temp = """SELECT count(*) FROM messages
        JOIN supplementary_message_data ON dmID = message_id
        WHERE %s message_type = %d AND 
          datetime(dmDeliveryTime) >= datetime('%s');"""
    all_count = fetch(select_temp % ("", message_type, time_threshold))
    if message_type == DsdbMessageStore.MESSAGE_TYPE_RECEIVED:
      unread_count = fetch(select_temp % ("read_locally = 0 AND", message_type,
                                          time_threshold))
    else:
      # for sent messages, it is always zero
      unread_count = 0   
    
    if all_count is None:
      finalize()
      return None
    result['recent'] = {"total": all_count, "unread": unread_count}
    # finalization
    return result

