# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for entry of credentials
"""

import pygtk
import gtk
import sys
pygtk.require("2.0")
from configuration import ConfigManager
import os_support


class CredentialsDialog(object):
  
  # maps login_method to a set of widget names
  widget_groups = {"username": ('password_entry','password_label'),
                   "certificate": ('p12file_label','p12file_chooserbutton'),
                  } 
  
  login_method_to_sensitivities = {"username": (True,False),
                                   "user_certificate": (True, True),
                                   "certificate": (False, True),
                                   "hotp": (True,False),
                                   "totp": (True,False)}
  
  LOGIN_METHODS = ["username","certificate","user_certificate","hotp","totp"]
  
  def __init__(self, parent, name=None, credentials=None, only_password=False,
               sync_with_all=True):
    """
    only_password specifies that changes to the credentials should not be
    allowed, only the filling of the password and potentially certificate
    file. 
    """
    self.parent = parent
    self.only_password = only_password
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("credentials_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.warning_label = self.builder.get_object("warning_label")
    self.dialog = self.builder.get_object("credentials_dialog")
    self.test_environment_checkbutton = \
                    self.builder.get_object("test_environment_checkbutton")
    self.login_method_combo = self.builder.get_object("login_method_combo")
    self.login_methods = self.login_method_combo.get_model()
    self.remember_password_checkbutton = \
                    self.builder.get_object("remember_password_checkbutton")
    login_entry = self.builder.get_object("login_entry")
    # set the value of the remember_password_checkbutton
    if not credentials:
      value = ConfigManager.STORE_PASSWORDS_ON_DISK
      self.remember_password_checkbutton.set_active(value)
    else:
      self.remember_password_checkbutton.set_active(credentials.remember_password)
    # portable mode - we disable this limitation experimentally
    #if self.parent.portable:
    #  self.remember_password_checkbutton.set_active(False)
    #  self.remember_password_checkbutton.set_sensitive(False)
    #  self.remember_password_checkbutton.set_tooltip_text(
    #        _("Datovka is running in portable mode - storage of passwords on \
    #disk is turned off for security reasons"))
    # set the test account button
    if credentials and credentials.test_account == True:
      # make normal account the default 
      self.test_environment_checkbutton.set_active(True)
    else:
      self.test_environment_checkbutton.set_active(False)
    # set the p12file
    if credentials:
      self.p12file = credentials.p12file
    else:
      self.p12file = None
    if self.p12file:
      self.builder.get_object('p12file_chooserbutton').\
          set_label(self.p12file)
    # check if certificate login is available
    from dslib.client import Client
    CredentialsDialog.CERT_LOGIN_AVAILABLE = Client.CERT_LOGIN_AVAILABLE
    # select the proper login method and force callbacks on the rest
    if not credentials:
      login_method_index = 0
    else:
      login_method_index = self.LOGIN_METHODS.index(credentials.login_method)
    # make some options inaccessible if support for certificates is not available
    if not self.CERT_LOGIN_AVAILABLE:
      # deactivate the certificate option
      self.login_methods[1][3] = False
      self.login_methods[2][3] = False
      if login_method_index in (1,2):
        login_method_index = 0
    self.login_method_combo.set_active(login_method_index)
    if not self.CERT_LOGIN_AVAILABLE:
      self.warning_label.set_text(_("""Certificate login is not available -
 the PyOpenSSL package could not be loaded."""))
    self._update_sensitivity()
    # disable things if only_password was requested
    if self.only_password:
      login_method = self.LOGIN_METHODS[login_method_index]
      sens_p, sens_c = self.login_method_to_sensitivities[login_method]
      if sens_p and sens_c:
        req_text = _("Please enter your password and certificate file.")
      elif sens_c:
        req_text = _("Please enter your certificate file.")
      elif sens_p:
        req_text = _("Please enter your password.")
      else:
        raise ValueError()
      self.set_main_label_text(req_text)
      self.test_environment_checkbutton.set_sensitive(False)
      self.builder.get_object("name_entry").set_sensitive(False)
      self.login_method_combo.set_sensitive(False)
    password_entry = self.builder.get_object("password_entry")
    # fill in the name
    if name:
      self.builder.get_object("name_entry").set_text(name)
    # fill in the data from credentials
    if credentials:
      login_entry.set_text(credentials.username or "")
      login_entry.set_sensitive(False)
      self.test_environment_checkbutton.set_sensitive(False)
    password_entry.set_text(credentials and credentials.password or "")
    # sync with all
    self.builder.get_object('sync_with_all_checkbutton').set_active(sync_with_all)
    if self.only_password:
      self.builder.get_object('sync_with_all_checkbutton').set_sensitive(False)
    self.dialog.set_transient_for(self.parent.window1)

  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()

  # callback handlers
  
  def on_login_method_combo_changed(self, w):
    self._update_sensitivity()
    self._update_warning()
  
  def on_password_entry_changed(self, w):
    self._update_warning()
    
  def on_login_entry_changed(self, w):
    self._update_warning()
    
  def on_name_entry_changed(self, w):
    self._update_warning()
  
  def on_p12file_chooserbutton_clicked(self, w):
    new = self._ask_a_certificate_filename(
                    self.p12file,
                    _("Select a certificate file in PKCS12 format")
                    )
    self.p12file = new
    self.builder.get_object('p12file_chooserbutton').\
                    set_label(self.p12file or _("(None)"))    
    self._update_warning()
  
 
  def _ask_a_certificate_filename(self, init_value, title):
    if not init_value:
      value = "(None)"
    else:
      value = init_value
    d = gtk.FileChooserDialog(title=title,
                              parent=self.dialog,
                              action=gtk.FILE_CHOOSER_ACTION_OPEN,
                              buttons=(gtk.STOCK_OK,gtk.RESPONSE_OK,
                                       gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL)
                              )
    d.set_current_folder(os_support.get_home_dir("./"))
    f2 = gtk.FileFilter()
    f2.set_name(_("PKCS12 files (*.p12)"))
    f2.add_pattern("*.p12")
    d.add_filter(f2)
    f1 = gtk.FileFilter()
    f1.set_name(_("All files (*)"))
    f1.add_pattern("*")
    d.add_filter(f1)
    d.set_filename(value)
    d.set_filter(f2) # I have no idea why this does not in fact work :(
    ok = d.run()
    if ok == gtk.RESPONSE_OK:
      fullname = d.get_filename().decode('utf-8')
      d.destroy()
      return fullname
    else:
      d.destroy()
      return init_value
  
  def _set_widget_group_sensitivity(self, widget_group, sensitive=True):
    for widget_id in self.widget_groups[widget_group]:
        self.builder.get_object(widget_id).set_sensitive(sensitive)

  def _update_sensitivity(self):
    login_method = self.get_login_method()
    sens1, sens2 = self.login_method_to_sensitivities.get(login_method)
    self._set_widget_group_sensitivity("username", sens1)
    self._set_widget_group_sensitivity("certificate", sens2)

  def _update_warning(self):
    # at first construct the text for warnings, function is lazy
    def get_warning_text():
      if not satis_p and not satis_c:
        return _("You must supply a password and a certificate file.")
      elif not satis_c:
        return _("You must supply a certificate file.")
      elif not satis_p:
        return _("You must supply a password.")
      else:
        raise ValueError()
    # check the requirements
    login_method = self.get_login_method()
    password = self.builder.get_object('password_entry').get_text()
    sens_p, sens_c = self.login_method_to_sensitivities.get(login_method)
    # what is satisfied
    satis_p = not sens_p or not self.only_password or bool(password)
    satis_c = not sens_c or self.p12file
    # activate the stars besides the entries
    if not satis_c:
      self.builder.get_object("certificate_warning").show()
    else:
      self.builder.get_object("certificate_warning").hide()
    if not satis_p:
      self.builder.get_object("password_warning").show()
    else:
      self.builder.get_object("password_warning").hide()
    # make the adjustments
    if satis_c and satis_p:
      self.warning_label.set_text("")
      # take into account if the username is entered
      if self.builder.get_object("login_entry").get_text() and \
         self.builder.get_object("name_entry").get_text():
        self.builder.get_object("ok_button").set_sensitive(True)
      else:
        self.builder.get_object("ok_button").set_sensitive(False)
    else:
      self.warning_label.set_text(get_warning_text())
      self.builder.get_object("ok_button").set_sensitive(False)
    # check if password seems valid
    ok, wrong = self.check_password(
                        self.builder.get_object('password_entry').get_text())
    label = self.builder.get_object("password_validation_label")
    if not ok:
        label.set_text(
                _("Password contains disallowed characters! See tooltip."))
        whitespace = False
        bad_letters = []
        for letter in wrong:
            if letter.isspace():
                whitespace = True
            else:
                bad_letters.append(letter)
        tooltips = []
        if bad_letters:
            tooltips.append(_("The following letters you entered are not \
allowed in an ISDS password: '%s'") % ",".join(bad_letters))
        if whitespace:
            tooltips.append(_("The password you entered contains whitespace \
characters which are not allowed by ISDS rules."))
        label.set_tooltip_text("\n\n".join(tooltips))
        label.show()
    else:
        label.hide()

  # end of callbacks
  
  def get_login_method(self):
    login_method = self.LOGIN_METHODS[self.login_method_combo.get_active()]
    return login_method

  def get_data(self):
    """returns a dictionary with filled-in data"""
    login_method = self.get_login_method()
    name = self.builder.get_object('name_entry').get_text()
    username = self.builder.get_object('login_entry').get_text()
    password = self.builder.get_object('password_entry').get_text()
    p12file = self.p12file
    test_account = self.test_environment_checkbutton.get_active()
    remember_password = self.remember_password_checkbutton.get_active()
    sync_with_all = self.builder.get_object('sync_with_all_checkbutton').\
                                                                  get_active()
    return dict(login_method=login_method,
                name=name,
                username=username,
                password=password,
                p12file=p12file,
                test_account=test_account,
                remember_password=remember_password,
                sync_with_all=sync_with_all)
    
  def set_main_label_text(self, text):
    self.builder.get_object("credentials_dialog_main_label").set_text(text)
    
#    ok, wrong = self.check_password(credentials.password)
#    if not ok:
#        self._show_error(u"Your password contains characters that should not be\
# part of a password - %s" % (u",".join(wrong)))
    
  @classmethod
  def check_password(self, password):
    """checks if password matches criteria set by ISDS;
    returns a tuple (ok, wrong) where ok is a boolean telling if password is ok
    and wrong is a set of letters that are not allowed in the password"""
    import string
    allowed = string.ascii_letters + string.digits + "!#$%&()*+,-.:=?@[]_{|}~"
    wrong = set()
    if type(password) == str:
        password = password.decode('utf-8')
    for letter in password:
        if letter not in allowed:
            wrong.add(letter)
    if wrong:
        return False, wrong
    else:
        return True, wrong
