# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
#*    02110-1301  USA

"""this module is used for custom handling of exceptions that make it
to the top level exception handler"""

# built-in imports
import traceback
import sys
import webbrowser
import logging
import locale

# external imports
import gtk

# local imports
import os_support

class ErrorWrapper(object):
  """
  This class encapsulates and exception and provides convenience method for
  its display
  """
  # the following dict maps errno's to some kind of help or description text
  # that could be used by the application to inform the user about the possible
  # cause of the error
  ERRNO_TO_HELP = {
  101: _("""It was not possible to establish a connection between your computer\
 and the server of Datove schranky.
This is usually caused by either lack of internet connection or by a firewall\
 on the way.
It might be necessary to use a proxy to connect to the server. If yes, please\
 set it up in the File/Proxy settings menu."""),
  10060: _("""It was not possible to establish a connection within a set time.
This is either caused by an extremely slow and/or unstable connection or by\
 an improper setup.
It is possible that the proxy settings used by Datovka\
 are wrong - you might need to supply a proxy or disable the one that\
 Datovka is using - please check your proxy settings in the File/Proxy\
 settings menu."""),
  10065: 101, # use the same text as for 101
  -2: _("""It was not possible to translate a domain name of the server of\
 Datove schranky into an IP address.
This error might be caused either by an incorrect setup of your internet\
 connection or a temporary problem of your DNS server. On some systems, this\
 error also appears when the computer is completely disconnected from the\
 network (thus also unable to connect to the DNS server).""")
  }

  def __init__(self, exception):
    self.exception = exception
    self.inner_exception, self.errno = \
                self.extract_innermost_exception_and_errno(exception)

  def __unicode__(self):
    return self.to_text()

  def to_text(self):
    """
    Creates a formated text that is 
    """
    cp = (os_support.get_default_text_encoding()
            or locale.getlocale()[1]
            or locale.getdefaultlocale()[1]
            or "ascii")
    detail = ""
    
    try:
        if hasattr(self.inner_exception, "strerror"):
            if isinstance(self.inner_exception.strerror, unicode):
                desc = self.inner_exception.strerror
            else:
                desc = self.inner_exception.strerror.decode(cp)
        else:
            desc = unicode(self.inner_exception)
    except Exception, e:
        desc = u"unknown error"
    
    return desc[:300]

  def get_help(self):
    """
    Returns a help message specific to Datovka based on the errno or other
    unique identifier of the exception
    """
    if self.errno:
      help = self.errno
      while help in self.ERRNO_TO_HELP:
          help = self.ERRNO_TO_HELP[help]
      if type(help) == int:
        # the errno was not (fully) resolved into a help message
        return None
      return help
  
  @classmethod
  def extract_innermost_exception_and_errno(self, exception):
    """
    Many network related exceptions contain an embedded exception from the lower
    level in the reason attribute. Extract the innermost one and possibly an
    errno on the way  
    """
    errno = None
    # get to the innermost exception and errno
    while hasattr(exception, "reason"):
      if hasattr(exception, "errno") and exception.errno is not None:
        errno = exception.errno
      exception = exception.reason
    if hasattr(exception, "errno") and exception.errno is not None:
        errno = exception.errno
    return exception, errno


class ErrorDisplayDialog(gtk.Dialog):

  def __init__(self, message):
    gtk.Dialog.__init__(self, parent=None, title=_("Error message"),
                        buttons=(gtk.STOCK_OK, 1))
    mainbox = gtk.VBox()
    mainbox.set_border_width(10)
    self.vbox.pack_start(mainbox)
    # label 1
    l1 = gtk.Label(_("""<b>An exception occurred in Datovka</b>.
  
You might be interested in letting us know by sending the following debugging \
information along with description of when the error occurred to \
'<b><a href='mailto:datove-schranky@labs.nic.cz'>datove-schranky@labs.nic.cz\
</a></b>'.
This would help us fix this problem in future versions of Datovka.
<b>We apologize for any inconvenience caused by this problem.</b>"""))
    l1.set_use_markup(True)
    l1.set_selectable(True)
    l1.set_line_wrap(True)
    l1.set_alignment(0,0)
    l1.set_width_chars(80)
    l1.connect("activate-link", self._open_link_callback)
    mainbox.pack_start(l1, padding=10, fill=False, expand=False)
    l1.show()
    # debug text
    f1 = gtk.Frame(_("Debugging info"))
    a1 = gtk.Alignment(xscale=1, yscale=1)
    s1 = gtk.ScrolledWindow() 
    s1.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC) 
    self.textview = gtk.TextView()
    textbuffer = self.textview.get_buffer()
    textbuffer.set_text(message)
    s1.add(self.textview)
    #w, h = self.textview.size_request()
    a1.set_padding(10,10,10,10)
    a1.add(s1)
    f1.add(a1)
    mainbox.pack_start(f1)
    self.vbox.show_all()
    sh = self.get_screen().get_height()
    self.set_size_request(-1, min(sh-50, 600))

  def _open_link_callback(self, link, url):
    webbrowser.open(url)


def show_exception_dialog(type, value, tb):
  tb_str = "".join( traceback.format_tb(tb))
  if hasattr(type, "__name__"):
    type_name = type.__name__
  else:
    type_name = str(type)
  message = "Type: %s\nValue: %s\nTraceback:\n%s" % (type_name, value, tb_str)
  logging.error(message)
  print >> sys.stderr, message
  if type is KeyboardInterrupt:
    return
  try:
    # add additional info
    message += "\n --- Additional info --- \n"
    message += "Python: %s\n" % ".".join([str(x) for x in sys.version_info])
    message += "Platform: %s\n" % sys.platform
    try:
      import release
      message += "Datovka version: %s\n" % release.DSGUI_VERSION
    except:
      message += "Datovka version: not available\n"
    try:
      import dslib.release
      message += "dslib version: %s\n" % dslib.release.DSLIB_VERSION
    except:
      message += "dslib version: not available\n"
    logging.error(message)
    d = ErrorDisplayDialog(message)
    d.run()
    d.destroy()
  except Exception, e:
    print >> sys.stderr,\
       "An error has occurred when trying to display an error message :("
    print >> sys.stderr, e
  
if __name__ == "__main__":
  sys.excepthook = show_exception_dialog
  __builtins__["_"] = lambda x: x
  print AAA
  