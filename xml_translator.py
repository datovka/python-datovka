
#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import xml.sax
from xml.sax.saxutils import XMLGenerator
from cStringIO import StringIO

class LocalizingHandler(xml.sax.ContentHandler):

  def __init__(self, writer, trans):
    xml.sax.ContentHandler.__init__(self)
    self.writer = writer
    self.trans = trans
    self._characters = ""
    self._translatable = False
    self._above = []

  def startElement(self, name, attrs):
    self._above.append((name,attrs))
    self._write_characters()
    if attrs.get('translatable',None)=="yes":
      self._translatable = True
    elif name == "property" and attrs.get('name',None) in ("tooltip","label","short_label","title"):
      self._translatable = True
    else:
      self._translatable = False
    self.writer.startElement(name, attrs)

  def endElement( self, name):
    self._write_characters()
    self.writer.endElement(name)
    self._above.pop(-1)

  def characters( self, data):
    self._characters += data

  def _write_characters(self):
    data = self._characters
    if self._translatable:
      self.writer.characters(self.trans(data.strip())) #.encode('utf-8'))
    else:
      self.writer.characters(data) #.encode('utf-8'))
    self._characters = ""


class ExtractingHandler(xml.sax.ContentHandler):

  def __init__(self):
    xml.sax.ContentHandler.__init__(self)
    self.messages = set()
    self._characters = ""
    self._translatable = False
    self._above = []

  def startElement(self, name, attrs):
    self._above.append((name,attrs))
    self._write_characters()
    if attrs.get('translatable',None)=="yes":
      self._translatable = True
    elif name == "property" and attrs.get('name',None) in ("tooltip","label","short_label","title"):
      self._translatable = True
    else:
      self._translatable = False

  def endElement( self, name):
    self._write_characters()
    self._above.pop(-1)

  def characters( self, data):
    self._characters += data

  def _write_characters(self):
    data = self._characters.strip()
    if self._translatable and data:
      if not data.startswith("gtk-"):
        # nasty hack - should use "<property name="use_stock">True</property>" for detection 
        self.messages.add(data)
    self._characters = ""



def translate_xml(filename, translation):
  out = StringIO()
  writer = XMLGenerator(out, encoding="utf-8")
  handler = LocalizingHandler(writer, translation)
  xml.sax.parse(filename, handler)
  return out.getvalue()

def translate_xml_string(text, translation):
  out = StringIO()
  writer = XMLGenerator(out, encoding="utf-8")
  handler = LocalizingHandler(writer, translation)
  xml.sax.parseString(text, handler)
  return out.getvalue()

def extract_translatable_strings(filename):
  """everything with @translatable
  all tooltips (property[@name='tooltip'])
  """
  handler = ExtractingHandler()
  xml.sax.parse(filename, handler)
  return handler.messages

def create_python_code_for_translation(infile):
  """creates a python file containing all the messages from a XML file
  that should be translated marked for translation
  """
  messages = extract_translatable_strings(infile)
  out = ""
  for message in messages:
    if '"' in message:
      out += "_(%s)\n" % repr(message)
    else:
      out += '_(%s)\n' % repr(message)
  return out
  

if __name__ == "__main__":
  import sys
  for arg in sys.argv[1:]:
    print create_python_code_for_translation(arg).encode('utf-8')
  
