# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for finding Data Boxes
"""

import pygtk
import gtk
pygtk.require("2.0")
from dslib import models


class DsSearchDialog(object):
  
  dbtypes = (("OVM",_("Public administration body")),
             ("PO",_("Corporate body")),
             ("PFO",_("Personal entity conducting business")),
             ("FO",_("Personal entity")))

  def __init__(self, parent, trans_window=None):
    """
    parent = GUI instance
    if trans_window is given, make the dialog transitional to this window
    """
    self.parent = parent
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("ds_search.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    self.dialog = self.builder.get_object("find_data_box_dialog")
    self.search_button = self.builder.get_object("search_button")
    self.found_data_boxes_store = self.builder.get_object("found_data_boxes_store")
    self.found_data_boxes_treeview = self.builder.get_object("found_data_boxes_treeview")
    self.type_info_label = self.builder.get_object("type_info_label")
    # here available dbtypes are stored
    self.dbtype_store = self.builder.get_object("dbtype_store")
    for code, title in self.dbtypes:
      self.dbtype_store.append([code, title])
    self.builder.get_object("dbtype_cb").set_active(0)
    if trans_window:
      self.dialog.set_transient_for(trans_window)


  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()

  # ========== Callbacks ==========

  def on_some_entry_changed(self, w):
    self._update_sensitivity_of_widgets()
    
  def _update_sensitivity_of_widgets(self):
    activate_button = False
    id_entry = self.builder.get_object("id_entry")
    ic_entry = self.builder.get_object("ic_entry")
    name_entry = self.builder.get_object("name_entry")
    zipcode_entry = self.builder.get_object("zipcode_entry")
    if id_entry.get_text():
      name_entry.set_sensitive(False)
      zipcode_entry.set_sensitive(False)
      ic_entry.set_sensitive(False)
      if len(id_entry.get_text()) == 7:
        activate_button = True
    elif name_entry.get_text() or zipcode_entry.get_text():
      if len(name_entry.get_text()) >= 2:
        activate_button = True
      id_entry.set_sensitive(False)
      ic_entry.set_sensitive(False)
    elif ic_entry.get_text():
      if len(ic_entry.get_text()) == 8:
        activate_button = True
      id_entry.set_sensitive(False)
      name_entry.set_sensitive(False)
      zipcode_entry.set_sensitive(False)
    else:
      name_entry.set_sensitive(True)
      zipcode_entry.set_sensitive(True)
      id_entry.set_sensitive(True)
      ic_entry.set_sensitive(True)
    if self._get_selected_dbtype() == "FO":
      ic_entry.set_sensitive(False)
    self.search_button.set_sensitive(activate_button)

  def on_dbtype_cb_changed(self, w):
    new_dbtype = self.dbtype_store[w.get_active_iter()][0]
    # at first deal with ic entry
    if new_dbtype == "FO":
      self.builder.get_object("ic_entry").set_sensitive(False)
    else:
      self.builder.get_object("ic_entry").set_sensitive(True)
    self._update_sensitivity_of_widgets()
    # inform about limitations to search according to current account status
    acc_info = self.parent.account.account_info
    message = ""
    if acc_info:
      acc_type = acc_info.dbType
      eff_ovm = acc_info.dbEffectiveOVM
      if acc_type is not None and eff_ovm is not None:
        # this information is available
        if new_dbtype != "OVM" and not (acc_type == "OVM" or eff_ovm):
          # only OVM is able to search non-OVM databoxes
          dbtypes_dict = dict(self.dbtypes)
          info_data = {"account_type": acc_type,
                       "account_type_str": dbtypes_dict.get(acc_type, acc_type)}
          if acc_info.dbOpenAddressing:
            # the only exception is when the account has commercial messages
            # activated - then it can talk to those who have the same activated
            message = _("Your account is of type %(account_type)s "
                        "(%(account_type_str)s) and you have Post Data Messages"
                        " activated.\n"
                        "This means you can only search for accounts of type "
                        "OVM and accounts that have Post Data Messages "
                        "delivery activated.\n"
                        "Because of this limitation the results of your "
                        "current search might not contain all otherwise "
                        "matching databoxes.") % info_data
          else:
            message = _("Your account is of type %(account_type)s "
                        "(%(account_type_str)s).\n"
                        "This means you can only search for accounts of "
                        "type OVM.\n"
                        "The current search settings will thus probably yield "
                        "no result.") % info_data
    if message:
      self.type_info_label.show()
      self.builder.get_object("type_info_warn_icon").show()
      note = _("Your search results will be limited. "
               "See tooltip for more information.")
      self.type_info_label.set_markup("<i>%s</i>" % note)
      self.type_info_label.set_tooltip_text(message)
    else:
      self.type_info_label.hide()
      self.builder.get_object("type_info_warn_icon").hide()
      self.type_info_label.set_tooltip_text("")
        
    
  def on_search_button_clicked(self, w):
    # extract data from the dialog
    dsid = self.builder.get_object("id_entry").get_text()
    ic = self.builder.get_object("ic_entry").get_text()
    dbtype = self._get_selected_dbtype()
    # create dbOwnerInfo for searching
    info = models.dbOwnerInfo()
    info.dbType = dbtype
    if dsid and len(dsid) == 7:
      # we search using dbID
      info.dbID = dsid
    elif ic:
      info.ic = ic
    else:
      # we have to find the box using name
      # in case of a physical entity, it is the name, otherwise firmName
      if dbtype in ("FO","PFO"):
        info.pnLastName = self.builder.get_object("name_entry").get_text()
      else:
        info.firmName = self.builder.get_object("name_entry").get_text()
      info.adZipCode = self.builder.get_object("zipcode_entry").get_text() or None
    info.dbState = 1
    soap_res = self.parent.try_client_method("FindDataBox",
                                             (info,),
                                             handle_status=False,
                                             parent=self.dialog,
                                             async=False)
    if not soap_res.fatal_error() and soap_res.has_reply_data():
      boxes = soap_res.reply.data
      self.found_data_boxes_treeview.show()
      self.found_data_boxes_store.clear()
      for box in boxes:
        name = box.firmName or (box.pnFirstName + " " + box.pnLastName)
        self.found_data_boxes_store.append([box.dbID,name,box.adCity,False,
                                            box.adZipCode])
      if not boxes:
        d = gtk.MessageDialog(self.dialog,
                              message_format=_("No matching Data boxes found."),
                              buttons=gtk.BUTTONS_OK,
                              type=gtk.MESSAGE_WARNING)
        d.run()
        d.destroy() 
      elif soap_res.reply.status.dbStatusCode == "0003":
        d = gtk.MessageDialog(self.dialog,
                              message_format=_("Too many results were found.\n"
                                    "Only the first %d were obtained.") %\
                                    len(boxes),
                              buttons=gtk.BUTTONS_OK,
                              type=gtk.MESSAGE_WARNING)
        d.run()
        d.destroy()        

    
  def select_toggle_toggled(self, w, row):
    column = 3
    self.found_data_boxes_store[row][column] = \
        not self.found_data_boxes_store[row][column]
    for row in self.found_data_boxes_store:
      if row[column]:
        self.builder.get_object("ok_button").set_sensitive(True)
        break
    else:
      self.builder.get_object("ok_button").set_sensitive(False)

  # // end of Callbacks
  
  def get_selected_box_names(self): 
    ret = []
    for row in self.found_data_boxes_store:
      if row[3]:
        # this row is selected
        ret.append([row[0],row[1],row[2],row[4]])
    return ret
    
  def _get_selected_dbtype(self):
    dbtype_cb = self.builder.get_object("dbtype_cb")
    dbtype_index = dbtype_cb.get_active()
    dbtype = dbtype_cb.get_model()[dbtype_index][0]
    return dbtype
    
