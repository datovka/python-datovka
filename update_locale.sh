#!/bin/sh

POTFILE=datovka.pot
DIR=locale
LANGS=`ls $DIR/*.po | sed "s/.po//"`

for ui in ui/*.ui; do python xml_translator.py $ui >> _tmp.py; done
xgettext --no-location -s -o $POTFILE -p $DIR -d datovka *.py
rm _tmp.py
for LANG in $LANGS; do
	msgmerge -U $LANG.po $DIR/$POTFILE
	mkdir -p $LANG/LC_MESSAGES
	msgfmt -o $LANG/LC_MESSAGES/datovka.mo $LANG.po
done
