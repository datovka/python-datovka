Instalační instrukce pro FreeBSD
--------------------------------

- nainstalujte balíček pro PyGTK - py26-gtk
- nainstalujte balíček pro sqlalchemy - databases/py-sqlalchemy
- pokud plánujete využít přihlašování certifikátem, nainstalujte PyOpenSSL 


Poloautomatická instalace
--------------------------

Stáhněte a rozbalte balíček se zdrojovými kódy dslib a spusťte jako root příkaz

>> python setup.py install

Obdobným postupem nainstalujte program Datovka. Program spustíte příkazem "datovka"


Provoz bez instalace
---------------------

- vstupte do adresáře datovka-X.Y ("cd" z příkazové řádky či pomocí manažeru souborů)
- rozbalte zde zdrojové kódy knihovny dslib do adresáře "dslib"
- spusťte soubor "datovka.py"
