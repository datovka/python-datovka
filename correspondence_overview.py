# encoding: utf-8

#*    Datovka (formerly dsgui) - a graphical user interface for Datove schranky
#*    Copyright (C) 2009-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
#*
#*    This library is free software; you can redistribute it and/or
#*    modify it under the terms of the GNU Library General Public
#*    License as published by the Free Software Foundation; either
#*    version 2 of the License, or (at your option) any later version.
#*
#*    This library is distributed in the hope that it will be useful,
#*    but WITHOUT ANY WARRANTY; without even the implied warranty of
#*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#*    Library General Public License for more details.
#*
#*    You should have received a copy of the GNU Library General Public
#*    License along with this library; if not, write to the Free
#*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Here resides a separate dialog for export of overview of correspondence for
a period of time
"""

import pygtk
import gtk
import sys
from datetime import datetime
import calendar
import os_support
pygtk.require("2.0")

class CorrespondenceDialog(object):
  
  def __init__(self, parent):
    self.parent = parent
    self.format = None
    self.builder = gtk.Builder()
    gui_xml = self.parent.fix_ui_xml("correspondence_overview_dialog.ui")
    try:
      self.builder.add_from_string(gui_xml.decode('utf-8'))
    except:
      # this fixes problem with different API on windows and PyGTK 2.12
      self.builder.add_from_string(gui_xml.decode('utf-8'), -1)
    self.builder.connect_signals(self)
    acc_name = "%s (%s)" % (self.parent.account.name,
                            self.parent.account.credentials.username)
    self.builder.get_object('account_name_label').set_text(acc_name)
    self.dialog = self.builder.get_object("correspondence_dialog")
    self.from_calendar = self.builder.get_object("from_calendar")
    self.to_calendar = self.builder.get_object("to_calendar")
    self.received_checkbutton = self.builder.get_object("received_checkbutton")
    self.sent_checkbutton = self.builder.get_object("sent_checkbutton")
    now = datetime.now()
    _scratch, maxday = calendar.monthrange(now.year, now.month)
    self.from_calendar.select_month(now.month-1, now.year)
    self.from_calendar.select_day(1)
    self.to_calendar.select_month(now.month-1, now.year)
    self.to_calendar.select_day(maxday)

  def run(self):
    result = self.dialog.run()
    return result

  def destroy(self):
    self.dialog.destroy()

  def hide(self):
    self.dialog.hide()
    
  def show_all(self):
    self.dialog.show_all()

  def on_some_calendar_day_selected(self, w):
    rec_data, sent_data = self.get_selected_messages()
    received = rec_data[1]
    sent = sent_data[1]
    send_text = ngettext("(%d message)", "(%d messages)", sent) % sent
    self.builder.get_object("info_label_sent").set_markup(send_text)
    rec_text = ngettext("(%d message)", "(%d messages)", received) % received
    self.builder.get_object("info_label_received").set_markup(rec_text)


  # callback handlers

  def get_dates(self):
    y,m,d = self.from_calendar.get_date()
    from_date = datetime(y,m+1,d,0,0,0)
    y,m,d = self.to_calendar.get_date()
    to_date = datetime(y,m+1,d,23,59,59)
    return from_date, to_date
  
  def get_selected_messages(self):
    from_date, to_date = self.get_dates()
    get_received = self.received_checkbutton.get_active()
    get_sent = self.sent_checkbutton.get_active()
    db = self.parent.account.messages
    received = []
    sent = []
    rec_count = 0
    sent_count = 0
    for m in db.get_messages_between_dates(from_date, to_date):
      typ = db.get_message_supplementary_data(m.dmID,"message_type")
      if typ == db.MESSAGE_TYPE_RECEIVED:
        rec_count += 1
        if get_received:
          received.append(m)
      if typ == db.MESSAGE_TYPE_SENT:
        sent_count += 1
        if get_sent:
          sent.append(m)
    return (get_received, rec_count, received), (get_sent, sent_count, sent)
  
  def get_exporter(self):
    from_date, to_date = self.get_dates()
    (get_rec, r, received), (get_sent, s, sent) = self.get_selected_messages()
    export_zfo = self.builder.get_object("export_zfo").get_active()
    format_select = self.builder.get_object("format_select")
    self.format = format_select.get_model()[format_select.get_active()][0]
    if self.format == 'html':
      export_class = CorrespondenceOverviewExporterHTML
    elif self.format == 'csv':
      export_class = CorrespondenceOverviewExporterCSV
    exp = export_class(from_date=from_date,
                       to_date=to_date,
                       export_received=get_rec,
                       export_sent=get_sent,
                       received_messages=received,
                       sent_messages=sent,
                       export_zfo=export_zfo)
    return exp
  

class CorrespondenceOverviewExporter(object): 
  
  FILE_EXTENSIONS = []

  def __init__(self, from_date=None, to_date=None,
               export_received=True, export_sent=True,
               received_messages=None, sent_messages=None,
               export_zfo=False):
    self.from_date = from_date
    self.to_date = to_date
    self.export_received = export_received
    self.export_sent = export_sent
    if received_messages:
      self.received_messages = received_messages
    else:
      self.received_messages = []
    if sent_messages:
      self.sent_messages = sent_messages
    else:
      self.sent_messages = []
    self.export_date = None
    self.export_zfo = export_zfo
      
  def generate_export(self):
    """return an export representing this overview as one big string"""
    raise NotImplementedError()

  
  def get_file_filters(self):
    filters = []
    for ext in self.FILE_EXTENSIONS:
      flt = gtk.FileFilter()
      flt.set_name(ext.get("name", "") + " (%s)" % ext.get("ext", ""))
      flt.add_pattern("*"+ext.get("ext", ""))
      filters.append(flt)
    flt = gtk.FileFilter()
    flt.set_name(_("All files"))
    flt.add_pattern("*")
    filters.append(flt)
    return filters



class CorrespondenceOverviewExporterHTML(CorrespondenceOverviewExporter): 

  HTML_TEMPLATE = '''<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>%(title)s</title>
<style type="text/css">
td {padding: 0px 5px; }
div { border-bottom: solid 1px black;}
body { font-family:Arial, sans; font-size: 12pt;}
*.smaller { font-size: smaller; }
</style>
</head>
<body>
%(body)s
</body>
</html>
'''
  
  FILE_EXTENSIONS = [{"ext": ".html", "name": _("HTML")},
                     ]

  def generate_export(self):
    """return a HTML export representing this overview as one big string"""
    # header
    self.export_date = datetime.now()
    title = _("Correspondence overview")
    parts = ["<h1>%s</h1>" % title]
    _row_temp = "<tr><td>%s:</td><td>%s</td></tr>"
    _from_date = os_support.format_datetime(self.from_date.date())
    _from_row = _row_temp % (_("From date"), _from_date)
    _to_date = os_support.format_datetime(self.to_date.date())
    _to_row = _row_temp % (_("To date"), _to_date)
    _gen_row = _row_temp % (_("Generated"),
                            os_support.format_datetime(self.export_date))
    parts.append("<table>%s%s%s</table>"%(_from_row, _to_row, _gen_row))
    # all messages
    for subtitle, do_it, messages in \
          ((_("Sent"), self.export_sent, self.sent_messages),
           (_("Received"), self.export_received, self.received_messages)):
      if do_it:
        parts.append("<h2>%s</h2>" % subtitle)
        _table = "".join([self._format_message_html(m) for m in messages])
        if not _table:
          _table = "<p>%s</p>" % _("No messages in this period.")
        parts.append(_table)
    body = "\n".join(parts)
    html = self.HTML_TEMPLATE % locals()
    return html
      
  def _format_message_html(self, m):
    _d_time = os_support.format_datetime(m.dmDeliveryTime.time())
    _d_date = os_support.format_datetime(m.dmDeliveryTime.date())
    _base = '''<table>
<tr><td><b>%s</b></td></tr>
<tr><td class="smaller">%s</td></tr>
<tr><td class="smaller">%s</td></tr>
</table>''' % (m.dmID, _d_date, _d_time)
    ret = '<div><table><tr><td>%s</td><td><table>' % _base
    _row_temp = '<tr><td>%s:</td><td><i>%s</i></td></tr>'
    annot = m.dmAnnotation if m.dmAnnotation != None else _("Empty")
    sender = m.dmSender if m.dmSender != None else _("Empty")
    recipient = m.dmRecipient if m.dmRecipient != None else _("Empty")
    ret += _row_temp % (_("Subject"),"<b>"+annot+"</b>")
    ret += _row_temp % (_("Sender"), sender)
    ret += _row_temp % (_("Recipient"), recipient)
    #ret += _row_temp % (_("Sent"), m.dmDeliveryTime)
    ret += "</table></td></tr></table></div>"
    return ret


class CorrespondenceOverviewExporterCSV(CorrespondenceOverviewExporter): 

  CSV_fields = [('dmID', _("ID")),
                ("dmMessageStatus", _("Status")),
                ("_dmType", _("Message type")),
                ('dmDeliveryTime', _("Delivery time")),
                ('dmAcceptanceTime', _("Acceptance time")),
                ('dmAnnotation', _("Subject")),
                ('dmSender', _("Sender")),
                ('dmSenderAddress', _("Sender Address")),
                ('dmRecipient', _("Recipient")),
                ('dmRecipientAddress', _("Recipient Address")),
                ("dmSenderIdent", _("Our file mark")),
                ("dmSenderRefNumber", _("Our reference number")),
                ("dmRecipientIdent", _("Your file mark")),
                ("dmRecipientRefNumber", _("Your reference number")),
                ] 
  
  FILE_EXTENSIONS = [{"ext": ".txt", "name": _("Excel compatible extension")},
                     {"ext": ".csv", "name": _("CSV")},
                     ]

   
  def generate_export(self):
    """return a CSV export representing this overview as one big string"""
    import csv
    from cStringIO import StringIO
    output = StringIO()
    writer = csv.writer(output, dialect="excel")
    self.export_date = datetime.now()
    # header
    writer.writerow([title for name,title in self.CSV_fields])
    # all messages
    for subtitle, do_it, messages in \
          ((_("Sent"), self.export_sent, self.sent_messages),
           (_("Received"), self.export_received, self.received_messages)):
      if do_it:
        for m in messages:
          writer.writerow(self._format_message(m))
    result = output.getvalue()
    return result
      
  def _format_message(self, m):
    result = []
    for field_name, title in self.CSV_fields:
        result.append(getattr(m, field_name) or "")
    return result

